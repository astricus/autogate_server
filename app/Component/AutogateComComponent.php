<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Автогейт
 */
class AutogateComComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log'
    );

    public $controller;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $group_id
     * @return mixed|null
     */
    public function getGroupStatus($group_id){
        $gates = $this->getGatesByGateGroup($group_id);
        $gate_enter = 0;
        foreach ($gates as $gate){
            $gate_id = $gate["Autogate"]["id"];
            $gate_dir = $gate["Autogate"]["direction"];
            if($gate_dir=="Enter"){
                $gate_enter =  $gate_id;
            }
        }
        return $this->getAGStatus($gate_enter);

    }

    /**
     * @param $gate_id
     * @throws Exception
     */
    public function setDefaultGateListData($gate_id)
    {
        $gate_status_data = [
            'gate_id' => $gate_id,
            'gate_error' => null,
            'gate_status' => null,
            'mtime' => now_date(),
            'pass_id' => 0,
            'out_of_service' => false,
            'doors_open' => true
        ];
        $modelName = "Gate_Status";
        $this->Gate_Status = ClassRegistry::init($modelName);
        $this->Gate_Status->create();
        $this->Gate_Status->save($gate_status_data);
    }

    public function createGate()
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);

        $new_gate = array(
            'name' => "test_enter",
            'status' => true,
            'direction' => 'Enter',
            'location' => "BORDER",
            'gate_group' => 1,
            'mtime' => now_date(),
            'out_of_service' => false,
            'doors_open' => true
        );
        $this->Autogate->save($new_gate);
        $autogate_id = $this->Autogate->getLastInsertId();

        $modelName = "Gate_Status";
        $this->Gate_Status = ClassRegistry::init($modelName);

        $this->Gate_Status->setupGate($autogate_id);
    }

    public function getGatesByGateGroup($GateGroup)
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $gates = $this->Autogate->find('all',
            array(
                'conditions' => array(
                    'gate_group' => $GateGroup,
                ),
                'order' => array("id DESC")
            )
        );
        return $gates;
    }

    public function mainInfo()
    {
        $a_id = Configure::read('AUTOGATE_ID');
        return $this->autoGateInfo($a_id);
    }

    public function oncomingInfo()
    {
        $a_id = Configure::read('AUTOGATE_2_ID');
        return $this->autoGateInfo($a_id);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function autoGateInfo($id)
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $gates = $this->Autogate->find('first',
            array(
                'conditions' => array(
                    'id' => $id,
                ),
            )
        );
        return $gates['Autogate'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAGOutOfService($id)
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $gates = $this->Autogate->find('first',
            array(
                'conditions' => array(
                    'id' => $id,
                ),
            )
        );
        if (count($gates) > 0) {
            return $gates['Autogate']['out_of_service'];
        }
        return null;
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getAGStatus($id)
    {
        $modelName = "Gate_Status";
        $this->Gate_Status = ClassRegistry::init($modelName);
        $gates = $this->Gate_Status->find('first',
            array(
                'conditions' => array(
                    'gate_id' => $id,
                ),
            )
        );
        if (count($gates) > 0) {
            return $gates['Gate_Status']['gate_status'];
        }
        return null;
    }

    /**
     * @param $id
     * @return array
     */
    public function getAGEnvironment($id)
    {
        $modelName = "Environment";
        $this->Environment = ClassRegistry::init($modelName);
        $list = $this->Environment->find('all',
            array(
                'conditions' => array(
                    'gate_id' => $id,
                ),
            )
        );
        $list_arr = [];
        foreach ($list as $item) {
            $name = strtolower($item['Environment']['name']);
            $status = $item['Environment']['status'];
            $list_arr[] = ['name' => $name, 'status' => $status];
        }
        return $list_arr;
    }

    /**
     * @param $gate_id
     * @return mixed
     */
    public function getCurrentPassID($gate_id)
    {
        $modelName = "Gate_Status";
        $this->Gate_Status = ClassRegistry::init($modelName);
        $pass_data = $this->Gate_Status->find('first',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                ),
            )
        );
        if (key_exists('Gate_Status', $pass_data)) {
            return $pass_data['Gate_Status']['pass_id'];
        }
        return null;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return array|bool|mixed
     * @throws Exception
     */
    public function setCurrentPassID($gate_id, $pass_id)
    {
        $modelName = "Gate_Status";
        $this->Gate_Status = ClassRegistry::init($modelName);
        $pass_data = $this->Gate_Status->find('first',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                ),
            )
        );

        $this->Gate_Status->id = $pass_data['Gate_Status']['id'];
        return $this->Gate_Status->save(["pass_id" => $pass_id]);
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return array
     */
    public function getPassData($gate_id, $pass_id)
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_data = $this->Pass_List->find('all',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id
                ),
            )
        );
        $data = [];
        if(count($pass_data)>0) {
            $data['photo'] = $pass_data[0]['Pass_List']['photo'];
            $data['document_number'] = $pass_data[0]['Pass_List']['doc_number'];
            $data['person_id'] = $pass_data[0]['Pass_List']['person_id'];
            return $data;
        }
        return null;
    }

    /**
     * @param $pass_id
     * @return mixed|null
     */
    public function getAllPassData($pass_id)
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_data = $this->Pass_List->find('first',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id
                ),
            )
        );
        if(count($pass_data)>0) {
            return $pass_data['Pass_List'];
        }
        return null;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return array|int|null
     */
    public function getEventsByPassAndAG($gate_id, $pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_data = $this->Event->find('all',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                    'pass_id' => $pass_id,
                ),
                "order" => array("order ASC")
            )
        );

        return $pass_data;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @param $type
     * @return array|int|null
     */
    public function getEventsByPassAndAGAndType($gate_id, $pass_id, $type)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_data = $this->Event->find('all',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                    'pass_id' => $pass_id,
                    "event_type" => $type
                ),
                "order" => array("order ASC")
            )
        );
        return $pass_data;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @param $event_id
     * @return array|int|null
     */
    public function getLastEvents($gate_id, $pass_id, $event_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_data = $this->Event->find('all',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                    'pass_id' => $pass_id,
                    //"id >" => $event_id
                ),
                "order" => array("id ASC")
            )
        );
        return $pass_data;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return bool
     */
    public function isPassDone($gate_id, $pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_data = $this->Event->find('first',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                    'pass_id' => $pass_id,
                    "event_type" => "PassDone"
                ),
            )
        );
        if ($pass_data == null) {
            return false;
        }
        return true;
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @param $type
     * @param $data
     * @param $data_type
     * @param $status
     * @return array|bool|mixed
     * @throws Exception
     */
    public function addEventData($gate_id, $pass_id, $type, $data, $data_type, $status)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        if ($data_type == "text") {
            $field = "eventData";
        } else {
            $field = "object";
        }

        $new_event = array(
            'gate_id' => $gate_id,
            'pass_id' => $pass_id,
            $field => $data,
            "event_result" => $status,
            "event_type" => $type,
            "event_time" => now_date(),
        );
        $this->Event->create();
        return $this->Event->save($new_event);
    }

    /**
     * @param $gate_id
     * @param $pass_id
     */
    public function clearEventData($gate_id, $pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $list = $this->getLastEvents($gate_id, $pass_id, 0);
        foreach ($list as $item) {
            $e_id = $item["Event"]['id'];
            $this->Event->id = $e_id;
            $this->Event->delete();
        }
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @param $type
     * @return mixed
     */
    private function getEventIdByPassGateAndType($gate_id, $pass_id, $type)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_data = $this->Event->find('first',
            array(
                'conditions' => array(
                    'gate_id' => $gate_id,
                    'pass_id' => $pass_id,
                    "event_type" => $type
                ),
                "order" => array("id ASC")
            )
        );
        return $pass_data['Event']['id'];
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return bool
     * @throws Exception
     */
    public function setPassDone($gate_id, $pass_id)
    {
        $this->addEventData($gate_id, $pass_id, 'PassDone', null, 'text', 'true');
        // reset current pass to null
        $this->setCurrentPassID($gate_id, 0);
        return true;
    }

    /**
     * @param $gate_id
     * @param $out_of_service
     * @param $doors_status
     * @return bool
     * @throws Exception
     */
    public function getGatesDoorsStatus($gate_id, $out_of_service, $doors_status)
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $gates = $this->getGatesByGateGroup($gate_id);
        foreach ($gates as $gate){
            $gate__id = $gate['Autogate']['id'];
            $gate_status_data = [
                "out_of_service" => $out_of_service,
                "doors_open" => $doors_status,
            ];
            $this->Autogate->id = $gate__id;
            $this->Autogate->save($gate_status_data);
        }
        return true;
    }

    /**
     * @param $gate_id
     * @return bool
     */
    private function checkAutoGateExist($gate_id)
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $gate = $this->Autogate->find('count',
            array(
                'conditions' => array(
                    'id' => $gate_id,
                ),
            )
        );
        if ($gate == 0) return false;
        return true;
    }

    /**
     * @param $gate_id
     * @param $amount
     * @return mixed
     */
    public function getLastPasses($gate_id, $amount){
        return $this->getGatePassListByDate($gate_id, date('Y-m-d'), "pass_time", 'DESC', $amount);
    }

    /**
     * @param $gate_id
     * @param $date
     * @param $filters
     * @param $sort
     * @param $dir
     * @param int $page
     * @param false $fast_mode
     * @param int $show_count
     * @return array|int|null
     */
    public function getGatePassListByDate($gate_id, $date, $filters, $sort, $dir, $page = 0, $fast_mode = false, $show_count = 20)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);
        if ($dir == null) {
            $dir = "DESC";
        }
        if ($sort == null) {
            $sort = "id";
        }
        $gate_arr = [];
        if($gate_id!=null){
            $gate_arr = ["gate_id" => $gate_id];
        }

        if($fast_mode==true) {
            $fast_mode_sql = 'Pass_List.photo';
        } else {
            $fast_mode_sql = "";
        }

        if($date == null){
            $date_str = [];
        }
        else {
            $date_str = ["DATE(pass_time) - DATE('" . $date . " 00:00:00') >=" => 0];
        }

        $person_str = [];
        $organization_str = [];
        $doc_number_str = [];
        if(isset($filters["person"])){
            $person_str =  $filters['person'];
            $person_str = ['or' => array(
                array(
                    'firstname ILIKE ' => "%$person_str%",
                ),
                array(
                    'lastname ILIKE ' => "%$person_str%",
                ),
                array(
                    'organization ILIKE ' => "%$person_str%",
                ),
            )];
        }

        if(isset($filters["organization"])){
            $organization_str =  $filters['organization'];
            $organization_str = [
                    'organization ILIKE ' => "%$organization_str%"
            ];
        }

        if(isset($filters["doc_number"])){
            $doc_number_str =  $filters['doc_number'];
            $doc_number_str = [
                'Pass_List.doc_number ILIKE ' => "%$doc_number_str%"
            ];
        }

        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);


        $list = $this->Pass_List->find('all',
            array(
                'conditions' => array(
                    $gate_arr,
                    $date_str,
                    $person_str,
                    $doc_number_str,
                    $organization_str
                ),
                'joins' => array(
                    array(
                        'table' => 'persons',
                        'alias' => 'Person',
                        'type' => 'Left',
                        'conditions' => array(
                            'Person.id = Pass_List.person_id'
                        )
                    ),
                    array(
                        'table' => 'control_lists',
                        'alias' => 'Control_List',
                        'type' => 'Left',
                        'conditions' => array(
                            'Pass_List.control_list = Control_List.id'
                        )
                    ),

                ),
                'fields' => array(
                    'Person.id',
                    'Person.firstname',
                    'Person.lastname',
                    'Person.middlename',
                    'Person.organization',
                    'Pass_List.id',
                    'Pass_List.person_id',
                    'Pass_List.pass_time',
                    'Pass_List.doc_number',
                    'Pass_List.doc_type',
                    'Pass_List.fingerprint_verification',
                    'Pass_List.face_verification',
                    'Pass_List.pass_direction',
                    'Pass_List.control_list',
                    $fast_mode_sql,
                    'Pass_List.pass_allowed',
                    'Pass_List.pass_done',
                    'Pass_List.pass_id',
                    'Control_List.list_name',
                    'Control_List.id'
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("Pass_List.$sort $dir")
            )
        );
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        foreach ($list as &$list_item){
            $pass_id = $list_item['Pass_List']['pass_id'];
            $list_item['has_event'] = $this->Event->find("count",
                array('conditions' =>
                    array(
                        'pass_id' => $pass_id,
                    ),
                )
            );
        }
        return $list;
    }

    /**
     * @param $gate_id
     * @param $date
     * @param $filters
     * @return array|int|null
     */
    public function getGatePassListCountByDate($gate_id, $date, $filters)
    {
        $gate_arr = [];
        if($gate_id!=null){
            $gate_arr = ["gate_id" => $gate_id];
        }
        if($date == null){
            $date_str = [];
        }
        else {
            $date_str = ["DATE(pass_time) - DATE('" . $date . " 00:00:00') >=" => 0];
        }

        $person_str = [];
        $organization_str = [];
        $doc_number_str = [];
        if(isset($filters["person"])){
            $person_str =  $filters['person'];
            $person_str = ['or' => array(
                array(
                    'firstname ILIKE ' => "%$person_str%",
                ),
                array(
                    'lastname ILIKE ' => "%$person_str%",
                ),
                array(
                    'organization ILIKE ' => "%$person_str%",
                ),
            )];
        }

        if(isset($filters["organization"])){
            $organization_str =  $filters['organization'];
            $organization_str = [
                'organization ILIKE ' => "%$organization_str%"
            ];
        }

        if(isset($filters["doc_number"])){
            $doc_number_str =  $filters['doc_number'];
            $doc_number_str = [
                'Pass_List.doc_number ILIKE ' => "%$doc_number_str%"
            ];
        }


        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);

        return $this->Pass_List->find('count',
            array(
                'conditions' => array(
                    $gate_arr,
                    $date_str,
                    $person_str,
                    $doc_number_str,
                    $organization_str
                ),
                'joins' => array(
                    array(
                        'table' => 'persons',
                        'alias' => 'Person',
                        'type' => 'Left',
                        'conditions' => array(
                            'Person.id = Pass_List.person_id'
                        )
                    ),
                ),

            )
        );
    }

    public function getPassEvents($pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_events = $this->Event->find('all',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id,
                ),
                'order' => array("id DESC")
            )
        );
        return $pass_events;
    }

    /**
     * @param $pass_id
     * @return mixed|null
     */
    public function getControlListByPass($pass_id){
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_events = $this->Event->find('first',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id,
                    'event_type' => 'ControlList'
                ),
            )
        );
        if(count($pass_events)==0) return null;
        return $pass_events['Event']['object'];
    }

}