<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Control RuleList
 */
class ControlRuleComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log',
        'PersonCom'
    );

    public $errors = [
        'empty_list_name' => 'пустое имя списка',
        'empty_list_description' => 'пустое описание списка',
        'empty_action_logic' => 'не выбрана логика действия правила или логика не определена',
        'undefined_direction' => 'не установлено либо некорректное направление для списка',
        'empty_owner' => 'пустой владелец списка',
        'rule_exists' => 'Правило уже существует для данного списка',
        'rule_id_is_not_set' => 'идентификатор правила отсутствует',
        'list_name_exists' => 'контрольный список с таким именем уже существует',
        'list_not_found' => 'контрольный список не найден',
        'cant_remove_list_on_cause_passes' => 'нельзя удалить контрольный список по причине того, что по нему уже были проходы',
        'cant_remove_rule_on_cause_passes' => 'нельзя удалить правило контрольного списка по причине того, что по нему уже были проходы',
    ];

    public $logic_action_list = [
        "deny_notify" => "Запретить и уведомить",
        "granted_notify" => "Разрешить и уведомить"
    ];

    public $direction_list = [
        "Enter" => "Вход",
        "Exit" => "Выход",
        "All" => "Все направления"
    ];

    public $error;

    public $controller;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public $valid_list_direction = ["Enter", "Exit", "All"];

    public function controlListAll()
    {
        $this->Control_List = ClassRegistry::init("Control_List");
        $matches = $this->Control_List->find('all',
            array(
                'conditions' => array(//$conditions,
                ),
                'order' => array("id DESC")
            )
        );
        return $matches;
    }


    /**
     * @param $name
     * @param $description
     * @param $owner
     * @param $direction
     * @param $action_logic
     * @return bool
     */
    private function validList($name, $description, $owner, $direction, $action_logic)
    {
        $this->error = [];
        if (empty($name)) {
            $this->error[] = $this->errors['empty_list_name'];
            return false;
        }
        if (empty($description)) {
            $this->error[] = $this->errors['empty_list_description'];
            return false;
        }
        if (empty($owner)) {
            $this->error[] = $this->errors['empty_owner'];
            return false;
        }
        if (!in_array($direction, $this->valid_list_direction)) {
            $this->error[] = $this->errors['undefined_direction'];
            return false;
        }
        if (empty($action_logic)) {
            $this->error[] = $this->errors['empty_action_logic'];
            return false;
        }
        return true;

    }

    /**
     * @param $name
     * @param $description
     * @param $owner
     * @param $direction
     * @param $action_logic
     * @return int|mixed|string|null
     * @throws Exception
     */
    public function createList($name, $description, $owner, $direction, $action_logic)
    {
        if (!$this->validList($name, $description, $owner, $direction, $action_logic)) {
            return false;
        }
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);

        if ($this->checkListExists($name)) {
            return false;
        }

        $new_gate = array(
            'list_name' => $name,
            'action_logic' => $action_logic,
            'enabled' => false,
            'direction' => $direction,
            'list_owner' => $owner,
            'description' => $description,
            'updated' => now_date(),
        );
        $this->Control_List->create();
        $this->Control_List->save($new_gate);
        return $this->Control_List->getLastInsertId();
    }

    /**
     * @param $name
     * @return bool
     */
    private function checkListExists($name)
    {
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);
        $conditions[] = ["list_name" => $name];
        $matches = $this->Control_List->find('count',
            array(
                'conditions' => array(
                    $conditions,
                ),
            )
        );
        if ($matches > 0) {
            $this->error[] = $this->errors['list_name_exists'];
            return true;
        }
        return false;
    }

    /**
     * @param $list_id
     * @return bool
     */
    private function checkListById($list_id)
    {
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);
        $conditions = ["id" => $list_id];
        $matches = $this->Control_List->find('count',
            array(
                'conditions' => array(
                    $conditions,
                ),
            )
        );
        if ($matches > 0) {
            return true;
        }
        $this->error[] = $this->errors['list_name_exists'];
        return false;
    }

    /**
     * @param $list_id
     * @return array|int|null
     */
    public function getListById($list_id)
    {
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);
        $conditions = ["id" => $list_id];
        return $this->Control_List->find('first',
            array(
                'conditions' => array(
                    $conditions,
                ),
            )
        );
    }


    //проверка на то, что данные соответствуют правилам

    /**
     * @param $firstname
     * @param $lastname
     * @param $document_number
     * @param $person_id
     * @return array|null
     */
    public function checkRuleMatch($firstname, $lastname, $document_number, $person_id)
    {
        $firstname = mb_strtolower($firstname);
        $lastname = mb_strtolower($lastname);
        $document_number = mb_strtolower($document_number);
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        if ($person_id > 0) {
            return $this->checkRulesByPersonId($person_id);
        }

        $active_rules = $this->getAllCheckRules();

        $rules = [];
        foreach ($active_rules as $rule) {
            $match_found = 0;
            $count_matches = 0;

            $rule_id = $rule['Control_Rule']['id'];
            $firstname_mask = mb_strtolower($rule['Control_Rule']['firstname_mask']);
            $lastname_mask= mb_strtolower($rule['Control_Rule']['lastname_mask']);
            $document_mask = mb_strtolower($rule['Control_Rule']['document_mask']);
            if ($firstname_mask != ""){
                $count_matches++;
                if(mb_substr_count($firstname, $firstname_mask)>0) {
                    $match_found++;
                }
            }
            if ($lastname_mask != ""){
                $count_matches++;
                if(mb_substr_count($lastname, $lastname_mask)>0) {
                    $match_found++;
                }
            }
            if ($document_mask != ""){
                $count_matches++;
                if(mb_substr_count($document_number, $document_mask)>0) {
                    $match_found++;
                }
            }

            if($match_found>0 && $count_matches==$match_found){
                $rules[] = $rule_id;
            }
        }
        if(count($rules)>0) return $rules;
        return null;
    }

    /**
     * @return array
     */
    public function getListNames()
    {
        $this->Control_List = ClassRegistry::init("Control_List");
        $lists = $this->Control_List->find('all',
            array(
                'conditions' => array(
                    //'enabled' => true
                ),
                'order' => array("id DESC")
            )
        );
        $list_ar = [];
        if (count($lists) == 0) return [];
        foreach ($lists as $list) {
            $list_ar[$list['Control_List']['id']] = $list['Control_List']['list_name'];
        }

        return $list_ar;
    }

    public function getActiveList()
    {
        $this->Control_List = ClassRegistry::init("Control_List");
        $lists = $this->Control_List->find('all',
            array(
                'conditions' => array(
                    'enabled' => true
                ),
                'order' => array("id DESC")
            )
        );
        $list_ar = [];
        if (count($lists) == 0) return [];
        foreach ($lists as $list) {
            $list_ar[] = $list['Control_List']['id'];
        }

        return $list_ar;
    }

    public function getAllCheckRules()
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $rules_arr = [];
        foreach ($this->getActiveList() as $list) {
            $rules = $this->Control_Rule->find('all',
                array(
                    'conditions' => array(
                        'control_list_id' => $list
                    ),
                    'order' => array("id DESC")
                )
            );
            foreach ($rules as $rule){
                $rules_arr[] = $rule;
            }
        }
        return $rules_arr;

    }

    /**
     * @param $person_id
     * @return array|null
     */
    private function checkRulesByPersonId($person_id)
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $rules = $this->Control_Rule->find('all',
            array(
                'conditions' => array(
                    'person_id' => $person_id,
                    'enabled' => true
                ),
                'order' => array("id DESC")
            )
        );
        $ids = [];
        if (count($rules) > 0) {
            foreach ($rules as $rule) {
                $ids[] = $rule['Control_Rule']['id'];
            }
        }
        if (count($ids) > 0) {
            return $ids;
        }
        return null;
    }

    /**
     * @param $list_id
     * @param $firstname
     * @param $lastname
     * @param $document_number
     * @param $person_id
     * @return array|bool|int|mixed|string|null
     * @throws Exception
     */
    public function createRule($list_id, $firstname, $lastname, $document_number, $person_id)
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $rule = [
            "control_list_id" => $list_id,
            "enabled" => false,
            "firstname_mask" => $firstname,
            "lastname_mask" => $lastname,
            "document_mask" => $document_number,
            "person_id" => intval($person_id) > 0 ? intval($person_id) : 0,
        ];
        $this->Control_Rule->create();
        if ($this->Control_Rule->save($rule)) {
            return $this->Control_Rule->id;
        } else {
            return null;
        }

    }

    /**
     * @param $list_id
     * @param $firstname
     * @param $lastname
     * @param $document_number
     * @param $person_id
     * @return bool
     */
    private function checkRuleExists($list_id, $firstname, $lastname, $document_number, $person_id)
    {
        if ($list_id == 0) {
            $this->errors[] = $this->errors['rule_id_is_not_set'];
            return true;
        }
        if ($firstname != "") {
            $conditions[] = ["firstname_mask" => $firstname];
        }
        if ($lastname != "") {
            $conditions[] = ["lastname_mask" => $lastname];
        }
        if ($lastname != "") {
            $conditions[] = ["document_mask" => $document_number];
        }
        if ($person_id != "") {
            $conditions[] = ["person_id" => $person_id];
        }
        $matches = $this->Control_Rule->find('count',
            array(
                'conditions' => array(
                    $conditions,
                ),
            )
        );
        if ($matches > 0) {
            $this->errors[] = $this->errors['rule_exists'];
            return true;
        }
        return false;
    }

    /**
     * @param $rule_id
     * @param $firstname
     * @param $lastname
     * @param $document_number
     * @param $person_id
     * @return bool|int
     * @throws Exception
     */
    public function updateRule($rule_id, $firstname, $lastname, $document_number, $person_id)
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        if (!$this->getRuleById($rule_id)) {
            return false;
        }
        $rule = [
            "firstname_mask" => $firstname,
            "lastname_mask" => $lastname,
            "document_mask" => $document_number,
            "person_id" => intval($person_id) > 0 ? intval($person_id) : 0,
        ];
        $this->Control_Rule->id = $rule_id;
        $this->Control_Rule->save($rule);
        return true;

    }

    /**
     * @param $list_id
     * @param $name
     * @param $description
     * @param $owner
     * @param $direction
     * @param $action_logic
     * @return bool
     * @throws Exception
     */
    public function updateList($list_id, $name, $description, $owner, $direction, $action_logic)
    {
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);
        $list = [
            "list_name" => $name,
            "description" => $description,
            "list_owner" => $owner,
            "direction" => $direction,
            "action_logic" => $action_logic
        ];
        $this->Control_List->id = $list_id;
        $this->Control_List->save($list);
        return true;

    }

    /**
     * @param $list_id
     * @return array|null
     */
    public function getRulesByListId($list_id)
    {
        $this->Control_Rule = ClassRegistry::init("Control_Rule");
        $matches = $this->Control_Rule->find('all',
            array(
                'conditions' => array(
                    'control_list_id' => $list_id,
                ),
            )
        );
        if (count($matches) == 0) {
            return null;
        }
        $ids = [];
        foreach ($matches as $match) {
            $ids[] = $match['Control_Rule']['id'];
        }
        return $ids;
    }

    public function getRuleById($rule_id)
    {
        $this->Control_Rule = ClassRegistry::init("Control_Rule");
        return $this->Control_Rule->find('all',
            array(
                'conditions' => array(
                    'id' => $rule_id,
                ),
            )
        );
    }

    /**
     * @param $list_id
     * @return array|int|null
     */
    public function getRulesDataByListId($list_id)
    {
        $this->Control_Rule = ClassRegistry::init("Control_Rule");
        $result_rules = $this->Control_Rule->find('all',
            array(
                'conditions' => array(
                    'control_list_id' => $list_id,
                ),
            )
        );
        foreach ($result_rules as &$result_rule){
            $person_id = $result_rule['Control_Rule']['person_id'];
            if($person_id>0){
                $person_name = $this->PersonCom->getPersonNameById($person_id);
                $result_rule['person_name'] = $person_name;
            } else {
                $result_rule['person_name'] = "";
            }
        }
        return $result_rules;
    }


    /**
     * @param $list_id
     * @return bool
     */
    public function deleteList($list_id)
    {
        $this->Control_List = ClassRegistry::init("Control_List");
        if (!$this->checkListById($list_id)) {
            $this->error[] = $this->errors['list_not_found'];
            return false;
        }
        // проверка, что по чеклисту не было проходов, иначе - удалять нельзя
        if($this->checkPassesByControlList($list_id)>0){
            $this->error[] = $this->errors['cant_remove_list_on_cause_passes'];
            return false;
        }
        $rules = $this->getRulesByListId($list_id);
        if ($rules !== null) {
            foreach ($rules as $rule) {
                $this->deleteRule($rule);
            }
        }
        $this->Control_List->id = $list_id;
        $this->Control_List->delete();
        return true;
    }

    /**
     * @param $rule_id
     * @return bool
     */
    public function deleteRule($rule_id)
    {
        $this->Control_Rule = ClassRegistry::init("Control_Rule");
        $matches = $this->Control_Rule->find('count',
            array(
                'conditions' => array(
                    'id' => $rule_id,
                ),
            )
        );
        if ($matches == 0) {
            return false;
        }
        $list_rule = $this->getRuleById($rule_id);
        $list_id = $list_rule[0]['Control_Rule']['control_list_id'];
        if($this->checkPassesByControlList($list_id)>0){
            $this->error[] = $this->errors['cant_remove_rule_on_cause_passes'];
            return false;
        }
        $this->Control_Rule->id = $rule_id;
        $this->Control_Rule->delete();
        return true;
    }

    /**
     * @param $rule_id
     * @return bool
     * @throws Exception
     */
    public function setOffRule($rule_id)
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $rule = [
            "enabled" => false,
        ];
        $this->Control_Rule->id = $rule_id;
        if ($this->Control_Rule->save($rule)) {
            return true;
        }
        return false;
    }

    /**
     * @param $rule_id
     * @return bool
     * @throws Exception
     */
    public function setOnRule($rule_id)
    {
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $rule = [
            "enabled" => true,
        ];
        $this->Control_Rule->id = $rule_id;
        if ($this->Control_Rule->save($rule)) {
            return true;
        }
        return false;
    }

    /**
     * @param $list_id
     * @return bool
     * @throws Exception
     */
    public function setOffList($list_id)
    {
        $this->Control_List = ClassRegistry::init("Control_List");
        $rule = [
            "enabled" => false,
        ];
        $this->Control_List->id = $list_id;
        if ($this->Control_List->save($rule)) {
            return true;
        }
        return false;
    }

    /**
     * @param $rule_id
     * @return bool
     * @throws Exception
     */
    public function setOnList($list_id)
    {
        $modelName = "Control_List";
        $this->Control_List = ClassRegistry::init($modelName);
        $rule = [
            "enabled" => true,
        ];
        $this->Control_List->id = $list_id;
        if ($this->Control_List->save($rule)) {
            return true;
        }
        return false;
    }

    /**
     * @param $list_id
     * @return array|int|null
     */
    public function checkPassesByControlList($list_id){
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        return $this->Pass_List->find("count",
            array('conditions' =>
                array(
                    "control_list" => $list_id,
                ),
            )
        );
    }

}