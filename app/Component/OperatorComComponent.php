<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Operator
 */
class OperatorComComponent extends Component
{
    public $components = array(
        'Log',
        'Session',
        'Validator',
    );

    public $real_password;

    public $min_pass_length = 8;

    public $max_pass_length = 20;

    public $min_unique_symbols = 3;

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public function setup()
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
    }

    public $controller;

    public $operator_roles = [
        "admin", "security"
    ];

    public $error = [];

    public $errors = [
        'operator_is_not_exists' => 'Оператор не найден или не существует',
        'login_already_exists' => 'Логин занят',
        'firstname_cannot_be_empty' => 'Имя не может быть пустым',
        'lastname_cannot_be_empty' => 'Фамилия не может быть пустой',
        'login_cannot_be_empty' => 'Логин не может быть пустым',
        'field_is_too_long' => 'Поле слишком большое (ограничение в 30 символов максимум)',
        'undefined_role' => 'Роль не определена',
        'password_is_too_short' => 'Пароль слишком короткий (не менее 8 символов)',
        'password_is_too_long' => 'Пароль слишком длинный (не менее 20 символов)',
        'password_is_too_simple' => 'Пароль слишком прост, придумайте более сложный пароль'
    ];

    public function saveOperatorAction($message, $type)
    {
        //return $this->_addMessage($message, $type);
    }

    private function _addMessage($message, $type)
    {
        /*
        $this->log_file = dirname(__DIR__) . DS . 'webroot' . DS . 'logs' . DS . 'events.log';
        if (!file_exists($this->log_file) or !is_readable($this->log_file)) {
            echo 'LOG file is not exist or unreadable,  create file ' . $this->log_file;
            exit;
        }
        $message = PHP_EOL . " " . now_date() . " > " . $type . " > " . $message;
        file_put_contents($this->log_file, $message, FILE_APPEND);
        return true;
        */
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function checkPassIsNull($id){
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $op = $this->Operator->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $id
                    ),
            )
        );
        if(empty($op['Operator']['password'])){
            return true;
        }
        return false;
    }

    /**
     * @param $login
     * @return int
     */
    public function checkLoginExists($login)
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        return $this->Operator->find("count",
            array(
                'conditions' =>
                    array(
                        'login' => $login
                    ),
            )
        );
    }

    /**
     * @param $length
     * @param string $keyspace
     * @return string
     * @throws Exception
     */
    public function generateRandomPass($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$_')
    {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    public function is_auth()
    {
        if ($this->Session->check('operator_id')) {
            return true;
        } else {
            return false;
        }
    }

    public function operator_id()
    {
        return $this->Session->read('operator_id');
    }

    public function auth_requires()
    {
        if (!$this->is_auth()) {
            response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
            exit;
        }
    }

    /**
     * @param $operator_id
     * @return mixed
     */
    public function getOperatorData($operator_id)
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $operator = $this->Operator->find("first",
            array('conditions' =>
                array(
                    'id' => $operator_id
                )
            )
        );
        return $operator;
    }

    public function getActiveAutoGates()
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $autogates = $this->Autogate->find("all",
            array('conditions' =>
                array(//'id' => $operator_id
                ),
                'order' => array('id DESC')
            )
        );
        return $autogates;
    }

    public function getActiveAutoGateGroups()
    {
        $modelName = "Autogate";
        $this->Autogate = ClassRegistry::init($modelName);
        $autogates = $this->Autogate->find("all",
            array('conditions' =>
                array(//'id' => $operator_id
                ),
                'order' => array('Autogate.id DESC'),
                // 'group' => array('Autogate.GateGroup'),
                // 'fields' => array('Autogate.Location, Autogate.Name, DISTINCT Autogate.GateGroup'),
            )
        );
        return $autogates;
    }

    /**
     * @param $token
     */
    public function logout($token)
    {
        $this->Token = ClassRegistry::init("Token");
        if ($this->checkToken($token)) {
            $token_id = $this->getTokenIdByToken();
            $this->Token->id = $token_id;
            $this->Token->delete();
            $this->Session->write('token', null);
            $this->Cookie->delete('token');
        }
    }

    public function getUserId()
    {
        $this->Token = ClassRegistry::init("Token");
        $token = $this->getUserToken();

        $user = $this->Token->find("first",
            array('conditions' =>
                array(
                    'token' => $token
                )
            )
        );
        if (count($user) > 0) {
            return $user['Token']['user_id'];
        }
        return null;
    }

    public function user_data()
    {
        if (!$this->is_auth()) {
            return false;
        } else {
            if (empty($this->user_data)) {
                $this->user_data = $this->getOperatorData($this->operator_id());
            }
            return $this->user_data;
        }
    }

    public function getOperatorList()
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $operators = $this->Operator->find("all",
            array('conditions' =>
                array(//'id' => $operator_id
                ),
                "order" => ("id ASC")
            )
        );
        return $operators;
    }

    public function add_operator($data)
    {
        $this->Operator = ClassRegistry::init("Operator");
        $this->error = [];


        $data['firstname'] = trim($data['firstname']);
        $data['lastname'] = trim($data['lastname']);
        $data['middlename'] = trim($data['middlename']);
        $data['firstname'] = mb_ucfirst(mb_strtolower($data['firstname']));
        $data['lastname'] = mb_ucfirst(mb_strtolower($data['lastname']));
        $data['middlename'] = mb_ucfirst(mb_strtolower($data['middlename']));

        $user_data["firstname"] = $data['firstname'];
        $user_data['lastname'] = $data['lastname'];
        $user_data['middlename'] = $data['middlename'];
        $user_data['login'] = $data['login'];
        $user_data['role'] = $data['role'];
        $user_data['enabled'] = true;

        if (empty($user_data['login'])) {
            $this->error[] = $this->errors["login_cannot_be_empty"];
        }

        if (!in_array($user_data['role'], $this->operator_roles)) {
            $this->error[] = $this->errors['undefined_role'] . " " . $user_data['role'];
        }

        if (!$this->Validator->valid_firstname($data['firstname'])) {
            $this->error[] = "Некорректное поле: имя";
        }

        if (!$this->Validator->valid_lastname($data['lastname'])) {
            $this->error[] = "Некорректное поле: фамилия";
        }

        if ($this->checkLoginExists($data['login'])) {
            $this->error[] = $this->error["login_already_exists"];
        }
        if (count($this->error) > 0) {
            return false;
        }

        $user_data["password"] = "";
        $this->Operator->create();
        $this->Operator->save($user_data);
        return $this->Operator->getLastInsertId();
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     * @throws Exception
     */
    public function updateStatus($id, $status)
    {
        $this->Operator = ClassRegistry::init("Operator");
        $this->Operator->id = $id;

        if ($id == null or $id < 0) {
            die("id is not integer!");
        }
        if ($status != "true" and $status != "false") {
            die("status is not boolean!");
        }
        $update_status = [
            'enabled' => $status
        ];

        $this->Operator->save($update_status);
        return true;
    }

    /**
     * @param $string
     * @return bool
     */
    private function checkFieldLength($string)
    {
        if (mb_strlen($string) > 30) {
            return false;
        }
        return true;
    }

    /**
     * @param $id
     * @param $lastname
     * @param $firstname
     * @param $middlename
     * @param $login
     * @return bool
     * @throws Exception
     */
    public function editOperator($id, $lastname, $firstname, $middlename, $login, $role)
    {

        if (empty($lastname)) {
            $this->error[] = $this->errors['lastname_cannot_be_empty'];
        }
        if (empty($firstname)) {
            $this->error[] = $this->errors['firstname_cannot_be_empty'];
        }
        if (empty($login)) {
            $this->error[] = $this->errors['login_cannot_be_empty'];
        }
        if (!$this->checkFieldLength($firstname)) {
            $this->error[] = $this->errors['field_is_too_long'] . " имя";
        }
        if (!$this->checkFieldLength($middlename)) {
            $this->error[] = $this->errors['field_is_too_long'] . " отчество";
        }
        if (!$this->checkFieldLength($lastname)) {
            $this->error[] = $this->errors['field_is_too_long'] . " фамилия";
        }
        if (!in_array($role, $this->operator_roles)) {
            $this->error[] = $this->errors['undefined_role'] . " " . $role;
        }

        $checkLogin = $this->Operator->find("count",
            array(
                'conditions' =>
                    array(
                        'id !=' => $id,
                        'login' => $login
                    ),
            )
        );
        if ($checkLogin) {
            $this->error[] = $this->errors['login_already_exists'];
        }
        $checkOperatorExists = $this->Operator->find("count",
            array(
                'conditions' =>
                    array(
                        'id' => $id,
                    ),
            )
        );
        if ($checkOperatorExists == 0) {
            $this->error[] = $this->errors['operator_is_not_exists'];
        }
        if (count($this->error) > 0) {
            return false;
        }
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $operator = [
            'lastname' => $lastname,
            'firstname' => $firstname,
            'middlename' => $middlename,
            'login' => $login,
            'role' => $role
        ];
        $this->Operator->id = $id;


        $this->Operator->save($operator);
        return true;
    }

    /*
     * Доработки:
     * 1. Не подгрузились данные в дашборд
     * 2. Не добавился документ при добавлении персоны
     * 3. Заменить бюст
     * 4. Добавить иконку лайтера
     * 5. Добавить закрытие после окончания pass
     * */

    /**
     * @param $id
     * @return array|int|null
     */
    public function getOperatorById($id)
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        return $this->Operator->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $id
                    ),
            )
        );
    }

    /**
     * @param $operator_id
     * @return mixed
     * @throws Exception
     */
    public function resetPassword($operator_id)
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $this->Operator->id = $operator_id;
        $this->Operator->save(['password' => ""]);
        return true;
    }

    /**
     * @param $operator_id
     * @param $password
     * @return bool
     */
    public function setupPassword($operator_id, $password)
    {
        if(!$this->validatePassword($password)){
            return false;
        }
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);
        $password = get_hash(Configure::read('USER_AUTH_SALT'), $password);
        $this->Operator->id = $operator_id;
        $this->Operator->save(['password' => $password]);
        return true;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        if (mb_strlen($password) < $this->min_pass_length) {
            $this->error[] = $this->errors['password_is_too_short'];
            return false;
        }
        if (mb_strlen($password) > $this->max_pass_length) {
            $this->error[] = $this->errors['password_is_too_long'];
            return false;
        }
        $unique = [];
        foreach (str_split($password) as $symbol){
            if(!in_array($symbol, $unique)){
                $unique[]=$symbol;
            }
        }
        if(count($unique)<$this->min_unique_symbols){
            $this->error[] = $this->errors['password_is_too_simple'];
            return false;
        }
        return true;
    }

}