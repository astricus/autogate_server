<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Person
 */
class PersonComComponent extends Component
{
    public $components = array(
        'Log',
        'Session',
        'AutogateCom',
        'Validator',
        'OperatorCom'
    );

    public function setup()
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
    }

    public $error = [];

    public $person_has_document;

    public $errors = [
        'person_is_not_exists' => 'Персона не найдена или не существует',
        'person_records_exists' => 'Нельзя удалить персону, так как уже существуют записи для нее',
        'person_firstname_is_incorrect' => 'Некорректное имя персоны',
        'person_lastname_is_incorrect' => 'Некорректная фамилия персоны',
        'person_middle_is_incorrect' => 'Некорректное отчество персоны',
        'document_is_busy' => 'Документ с такими данными уже есть в системе и используется',
    ];

    public $pass_events = [
        'DocumentImage' => 'Изображение документа',
        'ChipPhoto' => 'Изображение персоны из чипа',
        'ReadDocument' => 'Чтение документа со сканнера',
        'ReadFromDB' => 'Получение данных из базы данных',
        'FingerprintVerification' => 'Верификация отпечатка пальца',
        'FaceVerification' => 'Верификация по лицу',
        'ReadACMSCard' => 'Чтение пропуска',
        'Required' => 'Требуемые совпадения для верификации',
        'ControlList' => 'Наличие в контрольном списке',
        'Decision' => 'Принятие решения',
        'FatalError' => 'Фатальная ошибка',
        'InteractiveForm' => 'Форма взаимодействия с персоной'
    ];

    public $controller;

    public $verification_status = [
        'Unknown' => 'Нет данных',
        'Required' => 'Требуется',
        'NotRequired' => 'Не требуется',
        'Successfully' => 'Успешно',
        'Failed' => 'Не удалось',
    ];

    const UNKNOWN_DOCK_TYPE = "Неопределенный документ";

    public $doc_type = [
        "PN_RUS" => "Внутренний паспорт",
        "P_RUS" => "Загранпаспорт",
        "ID_internal" => "Пропуск",
        "PD_LAO" => "Паспорт Лаос",
        'FACE' => 'Идентификация по лицу'
    ];

    public $verification_type = [
        "Face" => "По лицу",
        "All" => "Оба",
        "Fingerprint" => "Отпечатки пальцев",
        "No" => 'Нет'
    ];

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $person_id
     * @return array|int|null
     */
    public function checkAcceptActivateFaceVerifyDocument($person_id)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $has_document = $this->Document->find("count",
            array('conditions' =>
                array(
                    'doc_type' => 'FACE',
                    'enabled' => true,
                    'person_id' => $person_id
                )
            )
        );
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        $can_accept = $this->Biometric->find("count",
            array('conditions' =>
                array(
                    //'modality' => "Face",
                    'template' => true,
                    'processed' => true,
                    'deleted' => false,
                    'person_id' => $person_id
                )
            )
        );
        if($has_document==0 AND $can_accept>0){
            return true;
        }
        return false;
    }

    /**
     * @param $person_id
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function addFaceVerificationDocument($person_id)
    {
        $new_document = [
            'doc_number' => '',
            'doc_type' => 'FACE',
            'person_id' => $person_id,
            'scheduler_id' => null,
            'verification' => 'Face',
            'enabled' => true,
            'known_security_elements' => 0,
            'security_elements_found' => 0,
        ];
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $this->Document->create();
        $this->Document->save($new_document);
        return $this->Document->id;
    }


    /**
     * @param $person_id
     * @return mixed
     */
    public function getPersonData($person_id)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $person = $this->Person->find("first",
            array('conditions' =>
                array(
                    'id' => $person_id
                )
            )
        );
        return $person;
    }

    /**
     * @param $person
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function addPerson($person)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        if (!key_exists("request_id", $person)) {
            $person["request_id"] = 0;
        }
        if (!key_exists("blocked", $person)) {
            $person["blocked"] = false;
        }
        $this->Person->create();
        $this->Person->save($person);
        return $this->Person->id;
    }

    /**
     * @param $type
     * @return bool
     */
    public function validateVerificationType($type)
    {
        if (!in_array($type, array_keys($this->verification_type))) {
            return false;
        }
        return true;
    }

    /**
     * @param $doc_id
     * @return array|int|null
     */
    public function checkDocumentInPass($doc_id)
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        return $this->Pass_List->find("count",
            array('conditions' =>
                array(
                    'doc_id' => $doc_id,

                ),
            )
        );
    }

    /**
     * @param $doc_number
     * @param $doc_type
     * @return bool
     */
    public function checkDocumentIsFree($doc_number, $doc_type): bool
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $document = $this->Document->find("first",
            array('conditions' =>
                array(
                    'doc_type' => $doc_type,
                    'doc_number' => $doc_number,
                    'enabled' => true

                ),
            )
        );
        if (count($document)> 0) {
            $this->person_has_document = $this->getPersonNameById($document['Document']['person_id']);
            return false;
        }
        return true;
    }

    /**
     * @param $doc_id
     * @return bool
     */
    public function deleteDocument($doc_id)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        if ($this->checkDocumentInPass($doc_id) > 0) {
            return false;
        }
        $this->Document->id = $doc_id;
        $this->Document->delete();
        return true;
    }

    /**
     * @param $doc_id
     * @return bool
     * @throws Exception
     */
    public function resetDocument($doc_id)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $document = [
            'known_security_elements' => 0,
        ];

        $message = "reset person document with id $doc_id";
        $this->OperatorCom->saveOperatorAction($message, "operator_action");

        $this->Document = ClassRegistry::init("Document");
        $this->Document->id = $doc_id;
        $this->Document->save($document);
        return true;
    }

    /**
     * @param $doc_number
     * @param $doc_type
     * @param $person_id
     * @param $verification_type
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function addPersonDocument($doc_number, $doc_type, $person_id, $verification_type)
    {
        // заглушки - добавить после верификацию
        if ($person_id == null) {
            die("person_id is null!");
        }
        $new_document = [
            'doc_number' => $doc_number,
            'doc_type' => $doc_type,
            'person_id' => $person_id,
            'scheduler_id' => null,
            'verification' => $verification_type,
            'enabled' => true,
            'known_security_elements' => 0,
            'security_elements_found' => 0,
        ];
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $this->Document->create();
        $this->Document->save($new_document);
        return $this->Document->id;
    }

    /**
     * @param $id
     * @param $doc_type
     * @param $doc_number
     * @param $verification
     * @return bool
     * @throws Exception
     */
    public function editDocument($id, $doc_type, $doc_number, $verification)
    {
        if ($id == null) {
            die("person_id is null!");
        }
        if (!$this->validateDocument($doc_number, $doc_type)) {
            $this->error[] = "Документ не валидный";
            return false;
        }
        if (!$this->validateVerificationType($verification)) {
            $this->error[] = "Некорректная верификация документа!";
            return false;
        }
        $document = [
            'doc_number' => $doc_number,
            'doc_type' => $doc_type,
            'verification' => $verification,
        ];

        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $this->Document->id = $id;
        $this->Document->save($document);
        return true;
    }

    /**
     * @param $person_id
     * @param $lastname
     * @param $firstname
     * @param $middlename
     * @param $organization
     * @param $division
     * @param $subdivision
     * @param $position
     * @return bool
     * @throws Exception
     */
    public function editPerson($person_id, $lastname, $firstname, $middlename, $organization, $division, $subdivision, $position)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);

        if (!$this->validatePerson($lastname, $firstname, $middlename)) {
            return false;
        }

        $person = [
            'lastname' => $lastname,
            'firstname' => $firstname,
            'middlename' => $middlename,
            'organization' => $organization,
            'division' => $division,
            'subdivision' => $subdivision,
            'position' => $position,
            'created' => now_date()
        ];
        $this->Person->id = $person_id;
        $this->Person->save($person);
        return true;
    }

    /**
     * @param $page
     * @param $count
     * @param $filter
     * @param $organization
     * @return array|int|null
     */
    public function getPersonList($page, $count, $filter, $organization, $person_status)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $count * ($page - 1);

        $modelName = "Person";
        $filter_request = [];
        if ($filter != null) {
            $filter_request = [
                "LEFT(lastname, 1)" => $filter
            ];
        }

        $org_request = [];
        if ($organization != null) {
            $org_request = [
                "organization" => $organization
            ];
        }
        if ($person_status == "on") {
            $person_status_filter = [
                //"blocked" => "false"
            ];
        } else {
            $person_status_filter = [
                "blocked" => "false"
            ];
        }
        $this->Person = ClassRegistry::init($modelName);
        $persons = $this->Person->find("all",
            array('conditions' =>
                array(
                    $filter_request,
                    $org_request,
                    $person_status_filter
                ),
                'limit' => $count,
                'offset' => $limit_page,
                'order' => array('lastname ASC')
            )
        );
        foreach ($persons as &$person) {
            $person_id = $person['Person']['id'];
            $documents = $this->getDocumentByPersonId($person_id);
            $person["docs"] = $documents;
        }
        return $persons;
    }

    /**
     * @return array
     */
    public function getPersonLastnameIndexes($organization, $person_status)
    {
        $org_arr =[];
        if($organization!=null){
            $org_arr = ['organization ILIKE '=> "%$organization%"];
        }

        if ($person_status == "on") {
            $person_status_filter = [

            ];
        } else {
            $person_status_filter = [
                "blocked"=> "false"
            ];
        }

        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $persons = $this->Person->find("all",
            array('conditions' =>
                array(
                    $org_arr,
                    $person_status_filter
                ),
                'order' => array('lastname ASC')
            )
        );
        $indexes = [];
        foreach ($persons as $person) {
            $person_index = mb_strtoupper(mb_substr($person['Person']['lastname'], 0, 1));
            if (!in_array($person_index, $indexes)) {
                $indexes[] = $person_index;
            }
        }
        return $indexes;
    }

    /**
     * @return array
     */
    public function getPersonOrganizations()
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $persons = $this->Person->find("all",
            array('conditions' =>
                array(),
                'order' => array('organization ASC')
            )
        );
        $indexes = [];
        foreach ($persons as $person) {
            $org = $person['Person']['organization'];
            if (empty($org)) {
                $org = "---не указана---";
            }
            if (!in_array($org, $indexes)) {
                $indexes[] = $org;
            }
        }
        return $indexes;
    }

    /**
     * @param $pass_id
     * @return array|null
     */
    public function detectDocumentByEvent($pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $document = $this->Event->find("first",
            array('conditions' =>
                array(
                    'pass_id' => $pass_id,
                    'or' => array(
                        array('event_type' => 'ReadDocument'),
                        array('event_type' => 'ReadACMSCard'),
                    ),
                ),
                'order' => array('id DESC')
            )
        );
        if (count($document) == 0) {
            return null;
        }
        $document = $document['Event']['event_result'];
        if (substr_count($document, "Document: ") > 0) {
            $doc_string = str_replace("Document: ", "", $document);
            $doc_arr = explode(" ", $doc_string);
            return ["doc_type" => $doc_arr[0], "doc_number" => $doc_arr[1]];
        }
        if (substr_count($document, "CARD N ") > 0) {
            $doc_string = str_replace("CARD N ", "", $document);
            return ["doc_type" => "ID_internal", "doc_number" => $doc_string];
        }
        return null;
    }

    /**
     * @param $doc_type
     * @param $doc_number
     * @return mixed|null
     */
    public function findPersonIdByDoc($doc_type, $doc_number)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $document = $this->Document->find("first",
            array('conditions' =>
                array(
                    'doc_type' => $doc_type,
                    'doc_number' => $doc_number

                ),
                'order' => array('id DESC')
            )
        );
        if (count($document) == 0) return null;
        return $document['Document']['person_id'];
    }

    /**
     * @param $person_id
     * @return string|null
     */
    public function getFaceFromBiometric($person_id)
    {
        $this->Biometric = ClassRegistry::init("Biometric");
        $biometric = $this->Biometric->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'modality' => 'Face',
                    'deleted' => false
                ),
                'order' => array('id DESC')
            )
        );
        if (count($biometric) == 0) {
            return null;
        }
        if (empty($biometric['Biometric']['object'])) {
            return null;
        }
        return $this->generateImageFromBinary($biometric['Biometric']['object']);
    }

    /**
     * @param $person_id
     * @return mixed|null
     */
    public function getFaceIdFromBiometric($person_id)
    {
        $this->Biometric = ClassRegistry::init("Biometric");
        $biometric = $this->Biometric->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'modality' => 'Face',
                    'deleted' => false,
                    'template' => false
                ),
                'order' => array('id DESC')
            )
        );
        if (count($biometric) == 0) {
            return null;
        }
        if (empty($biometric['Biometric']['object'])) {
            return null;
        }
        return $biometric['Biometric']['id'];
    }

    /**
     * @param $person_id
     * @return mixed|null
     */
    public function getImageParamFromBiometric($person_id)
    {
        $this->Biometric = ClassRegistry::init("Biometric");
        $biometric = $this->Biometric->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'modality' => 'Face',
                    'deleted' => false,
                    'template' => false
                ),
            )
        );
        if (count($biometric) == 0) {
            return null;
        }
        return $biometric['Biometric'];
    }

    /**
     * @param $person_id
     * @return string|null
     */
    public function getImageUrlFromBiometric($person_id)
    {
        $this->Biometric = ClassRegistry::init("Biometric");
        $biometric = $this->Biometric->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'modality' => 'Face',
                    'deleted' => false,
                    'template' => false
                ),
                'order' => array('id DESC')
            )
        );
        if (count($biometric) == 0) {
            return null;
        }
        if (empty($biometric['Biometric']['object'])) {
            return null;
        }
        return "/image/" . $biometric['Biometric']['id'];
    }

    /**
     * @param $gate_id
     * @param $pass_id
     * @return array
     */
    public function getPersonPhotosByPassId($gate_id, $pass_id)
    {
        //  изначально фотографии забирались из таблицы проходов, но так как она заполняется в последнюю очередь
        //в целях отмены задержки подгрузки фото в окно прохода решено забирать фотографии из таблицы событий в момент их появления там
        /*
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_data = $this->Pass_List->find("first",
            array('conditions' =>
                array(
                    'pass_id' => $pass_id,
                    //'gate_id' => $gate_id,

                ),
                'order' => array('id DESC')
            )
        );
        if (count($pass_data) == 0) {
            return null;
        }
        $person_id = $pass_data["Pass_List"]["person_id"];
        $real_image = $pass_data["Pass_List"]["photo"];
        if(!empty($real_image)){
            $real_image = $this->generateImageFromBinary($real_image);
        }
        $db_image = null;
        if ($person_id !== null) {
            $modelName = "Biometric";
            $this->Biometric = ClassRegistry::init($modelName);
            $photo_data = $this->Biometric->find("first",
                array('conditions' =>
                    array(
                        'person_id' => $person_id,
                    ),
                    'order' => array('id DESC')
                )
            );
            $db_image = $photo_data['Biometric']['object'];
            if(!empty($db_image)) {
                $image_data_base64 = $this->generateImageFromBinary($db_image);
            }
        }*/

        $document = $this->detectDocumentByEvent($pass_id);
        $person_id = $this->findPersonIdByDoc($document['doc_type'], $document['doc_number']);

        // get face verification image from event
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $event = $this->Event->find("first",
            array('conditions' =>
                array(
                    'pass_id' => $pass_id,
                    'event_type' => 'FaceVerification'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($event) > 0) {
            $real_image = $event["Event"]["object"];
            if (!empty($real_image)) {
                $real_image = $this->generateImageFromBinary($real_image);
            }
        }
        $real_image_final = $real_image ?? null;
        $db_face_image = null;
        if ($person_id != null) {
            $db_face_image = $this->getFaceFromBiometric($person_id);
        }
        return ["real_image" => $real_image_final, "db_image" => $db_face_image];
    }

    /**
     * @param $image_string
     * @return string
     */
    public function generateImageFromBinary($image_string)
    {
        $im = imagecreatefromstring($image_string);
        //header('Content-Type: image/jpeg');
        ob_start();
        imagejpeg($im);
        $image_data = ob_get_contents();
        ob_end_clean();
        return base64_encode($image_data);
    }

    /**
     * @param $table
     * @param $id
     */
    public function get_image($table, $id)
    {
        if ($id == null) {
            header("HTTP/1.1 404 Not Found");
            exit;
        }
        $this->$table = ClassRegistry::init($table);
        $image = $this->$table->find("first",
            array('conditions' =>
                array(
                    'id' => $id,
                ),
            )
        );
        if (count($image) == 0) {
            header("HTTP/1.1 404 Not Found");
            exit;
        }

        if ($table == "Biometric") {
            $field = "object";
        } else if ($table == "Pass_List") {
            $field = "photo";
        } else if ($table == "Event") {
            $field = "object";
        }

        if ($image[$table][$field] != null) {
            $this->layout = false;
            $im = imagecreatefromstring($image[$table][$field]);
            header('Content-Type: image/jpeg');
            imagejpeg($im);
        } else {
            header("HTTP/1.1 404 Not Found");
        }
        exit;
    }

    /**
     * @param $person_id
     * @return array|int|null
     */
    public function getPassListByPersonId($person_id)
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_list = $this->Pass_List->find("all",
            array('conditions' =>
                array(
                    'person_id' => $person_id
                ),
                'limit' => 100,
                'order' => array('id DESC')
            )
        );
        foreach ($pass_list as &$pass) {
            if (!empty($pass['Pass_List']['photo'])) {
                $pass['Pass_List']['photo_image_link'] = "/image_pass/" . $pass['Pass_List']['id'];//"data:image/jpeg;base64," . $this->generateImageFromBinary($pass['Pass_List']['photo']);
            } else {
                $pass['Pass_List']['photo_image_link'] = null;
            }
            $pass['Pass_List']['fingerprint_verification'] = $pass['Pass_List']['fingerprint_verification'] ?? 'Unknown';
            $pass['Pass_List']['face_verification'] = $pass['Pass_List']['face_verification'] ?? 'Unknown';
            $pass['Pass_List']['fp_verification_status_name'] = $this->verification_status[$pass['Pass_List']['fingerprint_verification']];
            $pass['Pass_List']['face_verification_status_name'] = $this->verification_status[$pass['Pass_List']['face_verification']];
        }
        return $pass_list;
    }

    /**
     * @param $person_id
     * @return array|int|null
     */
    public function getPersonDataById($person_id)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $person_info = $this->Person->find("all",
            array('conditions' =>
                array(
                    'id' => $person_id
                ),
            )
        );
        if ($person_info == null) {
            return null;
        }
        $documents = $this->getDocumentByPersonId($person_id);
        $pass_lists = $this->getPassListByPersonId($person_id);
        foreach ($pass_lists as &$pass_list){
            if($pass_list['Pass_List']['doc_type']=="Face"){
                $pass_list['Pass_List']['doc_type'] = "Идентификация по лицу";
            }
            else if (!in_array($pass_list['Pass_List']['doc_type'], array_keys($this->doc_type))) {
                $pass_list['Pass_List']['doc_type'] = "UNKNOWN";
            } else {
                $pass_list['Pass_List']['doc_type'] = $this->doc_type[$pass_list['Pass_List']['doc_type']];
            }
        }
        //$person_photo = $this->getFaceFromBiometric($person_id);

        return [
            "docs" => $documents,
            "info" => $person_info,
            'pass_list' => $pass_lists,
            //"person_image" => "data:image/jpeg;base64," . $person_photo,
            "person_image_link" => $this->getImageUrlFromBiometric($person_id)
        ];

    }

    /**
     * @param $person_id
     * @return array|int|null
     */
    public function getDocumentByPersonId($person_id)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        return $this->Document->find("all",
            array('conditions' =>
                array(
                    'person_id' => $person_id
                ),
                'order' => array('id ASC')
            )
        );
    }

    /**
     * @param $id
     * @return array|int|null
     */
    public function getDocumentById($id)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        return $this->Document->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                ),
            )
        );
    }


    /**
     * @param $person_id
     * @param $person_status
     * @return bool
     * @throws Exception
     */
    public function updatePersonStatus($person_id, $person_status)
    {
        $this->Person = ClassRegistry::init("Person");
        $this->Person->id = $person_id;

        if ($person_id == null or $person_id < 0) {
            die("person_id is not integer!");
        }
        if ($person_status != "true" and $person_status != "false") {
            die("person_status is not boolean!");
        }
        $update_status = [
            'blocked' => $person_status
        ];

        $this->Person->save($update_status);
        return true;

    }

    /**
     * @param $firstname
     * @param $lastname
     * @param $middlename
     * @return bool
     */
    private function validatePerson($lastname, $firstname, $middlename)
    {
        if (!$this->Validator->valid_lastname($lastname)) {
            $this->error[] = $this->errors['person_lastname_is_incorrect'];
        }
        if (!$this->Validator->valid_firstname($firstname)) {
            $this->error[] = $this->errors['person_firstname_is_incorrect'];
        }
        if (mb_strlen($middlename) > 0) {
            if (!$this->Validator->valid_middlename($middlename)) {
                $this->error[] = $this->errors['person_middle_is_incorrect'];
            }
        }
        if (count($this->error) > 0) return false;
        return true;
    }

    /**
     * @param $doc_number
     * @param $doc_type
     * @return bool
     */
    public function validateDocument($doc_number, $doc_type)
    {
        if (empty($doc_number) && $doc_type!="FACE") {
            return false;
        }
        if (empty($doc_type) or !in_array($doc_type, array_keys($this->doc_type))) {
            return false;
        }
        return true;
    }

    /**
     * @param $person_id
     * @param $photo
     * @return bool
     * @throws Exception
     */
    public function addPersonPhoto($person_id, $photo)
    {
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        $this->Biometric->create();

        if ($person_id == null or $person_id < 0) {
            die("person_id is not integer!");
        }
        if (empty($photo)) {
            die("person photo file is empty");
        }
        $new_data = [
            'modality' => 'Face',
            'personID' => $person_id,
            'fingerprint_index' => 0,
            'enrollment_id' => 0,
            'object' => $photo,
            'deleted' => false

        ];
        $this->Biometric->save($new_data);
        return true;
    }

    /**
     * @param $person_id
     * @return bool
     */
    private function forceDeletePerson($person_id)
    {
        $this->Person = ClassRegistry::init("Person");
        $this->Person->id = $person_id;
        $this->Person->delete();

        /*
        $biometrics = $this->getBiometricByPersonId($person_id);
        if (count($biometrics) > 0) {
            $this->Biometric = ClassRegistry::init("Biometric");
            foreach ($biometrics as $biometric) {
                $id = $biometric['Biometric']['id'];
                $this->Biometric->id = $id;
                $this->Biometric->delete($id);
            }
        }*/
        $documents = $this->getDocumentByPersonId($person_id);
        if (count($documents) > 0) {
            $this->Document = ClassRegistry::init("Document");
            foreach ($documents as $document) {
                $id = $document['Document']['id'];
                $this->Document->id = $id;
                $this->Document->delete();
            }
        }
        return true;
    }

    /**
     * @param $person_id
     * @return bool
     */
    private function checkRecordsByPersonId($person_id)
    {
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        $biometric = $this->Biometric->find("count",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                ),
            )
        );
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_list = $this->Pass_List->find("count",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                ),
            )
        );
        $modelName = "Control_Rule";
        $this->Control_Rule = ClassRegistry::init($modelName);
        $list_rules = $this->Control_Rule->find("count",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                ),
            )
        );

        if ($pass_list > 0 or $list_rules > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $person_id
     * @return false
     */
    public function deletePerson($person_id)
    {
        if (!$this->getPersonData($person_id)) {
            $this->errors[] = $this->errors['person_is_not_exists'];
            return false;
        }
        if ($this->checkRecordsByPersonId($person_id)) {
            $this->errors[] = $this->errors['person_records_exists'];
            return false;
        }
        return $this->forceDeletePerson($person_id);
    }

    /**
     * @param $person_id
     * @return array|int|null
     */
    private function getBiometricByPersonId($person_id)
    {
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        return $this->Biometric->find("all",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                ),
            )
        );
    }

    /**
     * @param $pass_id
     * @return array|null
     */
    public function getPersonDocumentScanParams($pass_id)
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);
        $pass_data = $this->Pass_List->find("first",
            array('conditions' =>
                array(
                    'pass_id' => $pass_id,
                ),
                'order' => array('id DESC')
            )
        );
        if (count($pass_data) == 0) {
            return null;
        }
        $person_id = $pass_data["Pass_List"]["person_id"];
        $doc_number = $pass_data["Pass_List"]["doc_number"];
        $doc_type = $pass_data["Pass_List"]["doc_type"];
        return $this->getDocumentParams($person_id, $doc_number, $doc_type);
    }

    /**
     * @param $person_id
     * @param $doc_number
     * @param $doc_type
     * @return array
     */
    private function getDocumentParams($person_id, $doc_number, $doc_type)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $pass_data = $this->Document->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'doc_number' => $doc_number,
                    'doc_type' => $doc_type,
                ),
            )
        );
        if (count($pass_data) > 0) {
            return [
                'known' => $pass_data["Document"]["known_security_elements"],
                "found" => $pass_data["Document"]["security_elements_found"]
            ];
        }
        return null;
    }

    /**
     * @param $pass_id
     * @return array|false
     */
    public function checkDoubtfulDocument($pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_check = $this->Event->find('first',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id,
                    'event_type' => 'ReadDocument',
                    'event_result' => 'DoubtfulDocument'
                ),
                "order" => array("id DESC")
            )
        );
        if (count($pass_check) == 0) return false;
        $data = unserialize($pass_check['Event']['event_data']);
        return ["found" => $data["security_elements_found"], "known" => $data["known_security_elements"]];
    }

    /**
     * @param $pass_id
     * @return bool
     * @throws Exception
     */
    public function acceptDocument($pass_id)
    {
        $params = $this->checkDoubtfulDocument($pass_id);
        if ($params == null) {
            return false;
        }
        $pass_data = $this->getPersonDocumentScanParams($pass_id);
        if ($pass_data == null) {
            return false;
        }
        $all_pass_data = $this->AutogateCom->getAllPassData($pass_id);
        if ($all_pass_data == null) {
            return false;
        }
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $get_document = $this->Document->find('first',
            array(
                'conditions' => array(
                    'person_id' => $all_pass_data['person_id'],
                    'doc_type' => $all_pass_data['doc_type'],
                    'doc_number' => $all_pass_data['doc_number']
                ),
            )
        );
        if (count($get_document) == 0) return false;
        $doc_id = $get_document['Document']['id'];
        $this->Document->id = $doc_id;
        $update_data = [
            "security_elements_found" => $params['found'],
            "known_security_elements" => $params['known'],
        ];
        $this->Document->save($update_data);
        return true;
    }

    /**
     * @param $pass_id
     * @return array|false
     */
    public function getDocumentImagesFromEvents($pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $pass_doc_images = $this->Event->find('all',
            array(
                'conditions' => array(
                    'pass_id' => $pass_id,
                    'event_type' => 'DocumentImage',
                ),
                "order" => array("id DESC")
            )
        );
        if (count($pass_doc_images) == 0) return false;
        $images = [];
        foreach ($pass_doc_images as $pass_doc_image) {
            if (!empty($pass_doc_image['Event']['object'])) {
                $images[] = $this->generateImageFromBinary($pass_doc_image['Event']['object']);
            }
        }
        if (count($images) == 0) return false;
        return $images;
    }

    public function getPersonInside()
    {
        $modelName = "Pass_List";
        $this->Pass_List = ClassRegistry::init($modelName);

        $all_persons_by_date = $this->Pass_List->find("all",
            array('conditions' =>
                array(
                    "DATE(Pass_List.pass_time) - DATE(NOW())" => 0,
                ),
                'fields' => array(
                    'DISTINCT person_id',
                ),
            )
        );
        $persons = [];
        foreach ($all_persons_by_date as $person) {
            $pid = $person['Pass_List']['person_id'];
            $pass_data = $this->Pass_List->find("first",
                array('conditions' =>
                    array(
                        "DATE(Pass_List.pass_time) - DATE(NOW())" => 0,
                        'person_id' => $pid
                    ),
                    'fields' => array(
                        'person_id',
                        'pass_direction',
                        'pass_time'
                    ),
                    'order' => array('pass_time DESC')
                )
            );
            $pass_direction = $pass_data['Pass_List']['pass_direction'];
            if ($pass_direction == "Enter") {
                $persons[] = ['id' => $pid, 'time' => $pass_data['Pass_List']['pass_time']];
            }
        }
        if (count($persons) > 0) {
            foreach ($persons as &$person) {
                if ($person['id'] > 0) {
                    $modelName = "Person";
                    $this->Person = ClassRegistry::init($modelName);
                    $person_data = $this->Person->find("first",
                        array('conditions' =>
                            array(
                                'id' => $person['id'],
                            ),
                        )
                    );
                    $name = prepare_fio($person_data['Person']['lastname'], $person_data['Person']['firstname'], $person_data['Person']['middlename']);
                    $person['name'] = $name;
                } else {
                    $name = "UNKNOWN";
                    $person['name'] = $name;
                }
            }
        }
        return $persons;
    }

    /**
     * @param $pass_id
     * @return mixed|null
     */
    public function getRequiredVerification($pass_id)
    {
        $modelName = "Event";
        $this->Event = ClassRegistry::init($modelName);
        $verify = $this->Event->find("first",
            array('conditions' =>
                array(
                    'pass_id' => $pass_id,
                    'event_type' => 'Required',
                ),
            )
        );
        if (count($verify) == 0) {
            return null;
        }
        $types = unserialize($verify['Event']['event_data']);
        return array_keys($types);
    }

    /**
     * @param $person_id
     * @return mixed|null
     */
    public function findPersonFace($person_id)
    {
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        $face = $this->Biometric->find("first",
            array('conditions' =>
                array(
                    'person_id' => $person_id,
                    'modality' => 'Face',
                ),
            )
        );
        if (count($face) == 0) {
            return null;
        }
        return $face['Biometric']['id'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletePersonFace($id)
    {
        $modelName = "Biometric";
        $this->Biometric = ClassRegistry::init($modelName);
        $this->Biometric->id = $id;
        $this->Biometric->delete();
        return true;
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     * @throws Exception
     */
    public function set_document($id, $status)
    {
        $modelName = "Document";
        $this->Document = ClassRegistry::init($modelName);
        $get_document = $this->Document->find('first',
            array(
                'conditions' => array(
                    'id' => $id,
                ),
            )
        );
        if (count($get_document) == 0) return false;
        $this->Document->id = $id;
        $update_data = [
            "enabled" => $status,
        ];
        $this->Document->save($update_data);
        return true;
    }

    /**
     * @param $person_string
     * @param $count
     * @return array|int|null
     */
    public function getPersonByString($person_string, $count)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $person = $this->Person->find("all",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'firstname ILIKE ' => "%$person_string%",
                        ),
                        array(
                            'lastname ILIKE ' => "%$person_string%",
                        ),
                        array(
                            'organization ILIKE ' => "%$person_string%",
                        ),
                    )
                ),
                "limit" => $count
            )
        );
        return $person;
    }

    /**
     * @param $person_id
     * @return string
     */
    public function getPersonNameById($person_id)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $person = $this->Person->find("first",
            array('conditions' =>
                array(
                    'id' => $person_id
                )
            )
        );
        return prepare_fio($person['Person']['lastname'], $person['Person']['firstname'], $person['Person']['middlename']);
    }

    public function getPersonsCount($filter, $organization, $person_status)
    {
        $modelName = "Person";
        $this->Person = ClassRegistry::init($modelName);
        $filter_request = [];
        if ($filter != null) {
            $filter_request = [
                "LEFT(lastname, 1)" => $filter
            ];
        }

        $org_request = [];
        if ($organization != null) {
            $org_request = [
                "organization" => $organization
            ];
        }
        if ($person_status == "on") {
            $person_status_filter = [

            ];
        } else {
            $person_status_filter = [
                "blocked" => "false"
            ];
        }
        return $this->Person->find("count",
            array('conditions' =>
                array(
                    $filter_request,
                    $org_request,
                    $person_status_filter
                ),
            )
        );
    }
}