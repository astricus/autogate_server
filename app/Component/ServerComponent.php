<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Сервер (Связь с сервером)
 */
class ServerComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    public $log_file;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $this->api_server = Configure::read('API_SERVER_IP');
        $this->api_uri = Configure::read('API_SERVER_URI');
        if(!file_exists($this->log_file) OR !is_readable($this->log_file)){
            echo 'LKP LOG file is not exist or unreadable,  create file ' . $this->log_file;
            exit;
        }
    }

    /**
     * @param $message
     * @param $sender
     * @param $message_type
     * @param $message_data
     * @return bool|void
     */
    public function sendMessage($message, $sender, $message_type, $message_data)
    {
        $this->setup();
        if($sender==null) $sender = 'AUTOGATE';
        $timestamp = time();
        $message_hash = $this->messageHash($message, $sender, $message_type, $message_data, $timestamp);
        $send_data = [];
        $send_data['message'] = $message;
        $send_data['sender'] = $sender;
        $send_data['message_type'] = $message_type;
        $send_data['message_data'] = $message_data;
        $send_data['timestamp'] = $timestamp;
        $send_data['hash'] = $message_hash;
        $api_server_message_url = $this->api_server  . $this->api_uri .

        /*
        $api_server_message_url = $this->api_server  . $this->api_uri .
            '?message=' . urlencode($message)
            . '&sender=' . urlencode($sender)
            . '&message_type=' . urlencode($message_type)
            . '&message_data=' . urlencode($message_data)
            . '&hash=' . $message_hash;
        $api_server_message_sender = file_get_contents($api_server_message_url);*/
        $send_json = json_decode($send_data);

        return $this->_send($api_server_message_url, $send_json);
    }

    //private message hash
    /**
     * @param $message
     * @param $sender
     * @param $message_type
     * @param $message_data
     * @param $timestamp
     * @return string
     */
    private function messageHash($message, $sender, $message_type, $message_data, $timestamp){
        $this->api_salt = Configure::read('API_SALT');
        return hash('sha256', $message . $sender . $message_type . $message_data . $timestamp . $this->api_salt);
    }

    /**
     * @param $server
     * @param $data_json
     */
    private function _send($server, $data_json)
    {
        $this->setup();
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $server);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data_json));
            $response = curl_exec($curl);
            curl_close($curl);
        }
    }

}