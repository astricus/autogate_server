<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Валидатор
 */
class ValidatorComponent extends Component
{
    public $components = array(
        'Log'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public function valid_hash($hash)
    {
        if (strlen(trim($hash)) != 32) {
            return false;
        } else return true;
    }

    public function valid_password($pass)
    {
        if (preg_match("/^[a-zA-Z0-9_!@#$%^&*]{8,32}$/i", $pass)) {
            return true;
        } else return false;
    }

    public function valid_mail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else return false;
    }

    public function valid_phone_number($number)
    {
        if (preg_match("/^[0-9]{11}$/", $number)) {
            return true;
        }
        return false;
    }

    public function valid_minute($int)
    {
        if ($int >= 0 AND $int <= 59) {
            return true;
        } else return false;
    }

    public function valid_hour($int)
    {
        if ($int >= 0 AND $int <= 23) {
            return true;
        } else return false;
    }

    public function valid_date($date)
    {
        return (bool)strtotime($date);
    }

    private function is_valid_name($string, $min, $max)
    {
        if ((!empty($string)) AND (mb_strlen($string) <= $max) AND (mb_strlen($string) >= $min)
            AND (preg_match('/^[a-zA-ZабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\s\- ]+$/u', $string))
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function valid_firstname($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_lastname($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_middlename($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_sex($string)
    {
        if($string == 'male' or $string == 'female'){
            return true;
        }
        else{
            return false;
        }
    }

    public function valid_building($string)
    {
        if ((!empty($string)) AND (mb_strlen($string) <= 1) AND (mb_strlen($string) >= 32)
            AND (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\. ]+$/u', $string))
        ) {

            return true;
        } else {
            return false;
        }
    }

    public function valid_house($string)
    {
        if ((!empty($string)) AND (mb_strlen($string) <= 1) AND (mb_strlen($string) >= 32)
            AND (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\. ]+$/u', $string))
        ) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $int
     * @return bool
     */
    public function valid_int($int){
        if(preg_match('/^\d+$/',$int)) {
            return true;
        } return false;
    }
}