<?php
/**
 * This is email configuration file.
 *
 * Use it to configure email transports of CakePHP.
 *
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *  Mail - Send using PHP mail function
 *  Smtp - Send using SMTP
 *  Debug - Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email. Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
class EmailConfig
{
//	public $default = array(
//		'from' => 'noreplay@terpikka.ru',
//		'charset' => 'utf-8',
//		'headerCharset' => 'utf-8',
//        'transport' => 'Smtp',
//        //'from' => array('email@gmail.com' => 'My Site'),
//        'host' => 'ssl://smtp.yandex.ru',
//        'port' => 465,
//        'timeout' => 30,
//        'username' => 'armageddance',
//        'password' => 'cosmos1234',
//        'client' => null,
//        'log' => true,
//        'auth' => true,
//	);

    public $yandex = array(
        'from' => 'terpikka-mail@yandex.ru',
        'charset' => 'utf-8',
        'headerCharset' => 'utf-8',
        'transport' => 'Smtp',
        //'from' => array('email@gmail.com' => 'My Site'),
        'host' => 'ssl://smtp.yandex.ru',
        'port' => 465,
        'timeout' => 30,
        'username' => 'terpikka-mail',
        'password' => "terpikka",
        'client' => null,
        'log' => true,
        'auth' => true,
        'tls' => false
    );

	public $smtp_gmail = array(
		'transport' => 'Smtp',
        'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'timeout' => 30,
		'username' => 'noreplay.terpikka@gmail.com',
		'password' => 'terpikka1234',
		'client' => null,
		'log' => true,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);

	public $fast = array(
		'from' => 'you@localhost',
		'sender' => null,
		'to' => null,
		'cc' => null,
		'bcc' => null,
		'replyTo' => null,
		'readReceipt' => null,
		'returnPath' => null,
		'messageId' => true,
		'subject' => null,
		'message' => null,
		'headers' => null,
		'viewRender' => null,
		'template' => false,
		'layout' => false,
		'viewVars' => null,
		'attachments' => null,
		'emailFormat' => null,
		'transport' => 'Smtp',
		'host' => 'localhost',
		'port' => 25,
		'timeout' => 30,
		'username' => 'user',
		'password' => 'secret',
		'client' => null,
		'log' => true,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);
}
