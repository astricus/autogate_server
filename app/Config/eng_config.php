<?php

$config = array(

	'MAIN_PAGE' => 'Home Page',

    'NEW_PAGE' => 'News Page',

    'YOU_JUST_REGISTERED' => 'You just registered now!',

    'WRONG_LOGIN_OR_PASSWORD' => "wrong login or password",

    'ADMIN_ACCESS_DENIED' => "Access denied for you, please contact with youor administrator",

);