<?php

$config = array(
	'ERROR_1' => array(
		'TEXT' => 'Вызвана неопознанная ошибка',
		'TYPE' => 'NCR',
		'RESPONSE' => 'DIE',
		'RESPONSE_CONTENT' => 'Вызвана неопознанная ошибка',
		'Product' => 'YES',
		'ID' => 1
	),

	'ERROR_101' => array(
		'TEXT' => 'Временная директория для записи изображений недоступна',
		'TYPE' => 'NCR',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Временная директория для записи изображений недоступна',
		'Product' => 'YES',
		'ID' => 101
	),

	'ERROR_102' => array(
		'TEXT' => 'Указан некорректный конфиг загрузки файла',
		'TYPE' => 'NCR',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Указан некорректный конфиг загрузки файла',
		'Product' => '',
		'ID' => 102
	),

	'ERROR_103' => array(
		'TEXT' => 'Файл не существует',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Файл не существует',
		'Product' => '',
		'ID' => 103
	),

	'ERROR_110' => array(
		'TEXT' => 'Размер загружаемого изображения превышаем допустимый',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Размер загружаемого изображения превышаем допустимый',
		'Product' => '',
		'ID' => 110
	),

	'ERROR_111' => array(
		'TEXT' => 'Объем загружаемого изображения превышаем допустимый',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Объем загружаемого изображения превышаем допустимый',
		'Product' => '',
		'ID' => 111
	),

	'ERROR_112' => array(
		'TEXT' => 'Расширение загружаемого изображения не соответствует списку допустимых',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Расширение загружаемого изображения не соответствует списку допустимых',
		'Product' => '',
		'ID' => 112
	),

	'ERROR_113' => array(
		'TEXT' => 'Не удалось загрузить файл в указанную директорию',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не удалось загрузить файл в указанную директорию',
		'Product' => '',
		'ID' => 113
	),

	'ERROR_114' => array(
		'TEXT' => 'Не удалось создать директорию',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не удалось создать директорию',
		'Product' => '',
		'ID' => 114
	),


	'ERROR_201' => array(
		'TEXT' => 'Не найдена указанная услуга',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не найдена указанная услуга',
		'Product' => '',
		'ID' => 201
	),

	'ERROR_231' => array(
		'TEXT' => 'Должно быть заполнено название города хотя бы одном языке',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не найдена указанная услуга',
		'Product' => '',
		'ID' => 231
	),

	'ERROR_301' => array(
		'TEXT' => 'Должно быть заполнено поле название сообщения',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не удалось отправить сообщение пользователю',
		'Product' => '',
		'ID' => 301
	),

	'ERROR_302' => array(
		'TEXT' => 'Должно быть заполнено поле текст сообщения',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не удалось отправить сообщение пользователю',
		'Product' => '',
		'ID' => 302
	),

	'ERROR_303' => array(
		'TEXT' => 'Указан некорректный идентификатор пользователя',
		'TYPE' => 'WARNING',
		'RESPONSE' => 'AJAX',
		'RESPONSE_CONTENT' => 'Не удалось отправить сообщение пользователю',
		'Product' => '',
		'ID' => 303
	)

);