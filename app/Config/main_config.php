<?php

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('DEF_MAIL')) {
	define('DEF_MAIL', 'homoastricus2011@gmail.com');
}

$config = array(

    'ACCEPT_FACE_VERIFICATION' => true,

    'JSON_CACHE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'api_cache',

    'DEF_LOCALE' => 'eng',

    'VALID_LANGS' => array('RU', 'EN'),

    'VALID_LANG_LOCALES' => array('rus', 'eng'),

	'API_SERVER_IP' => "127.0.0.1",

    'API_SERVER_URI' => "/autogate/api",

	//общие
	'SITENAME' => 'autogate system',

    'CONTENT_SERVER' => "http://content.server",

	'SITE_URL' => 'http://autogate.spo/',

	'SITEPHONE' => '',

	'DEF_LANG' => 'RU',

	//записывается ли лог ошибок
	'ERROR_LOG_ACTIVE' => true,

	'IS_CLOUDFLARE' => "N",

	//почта сайта
	'SITE_MAIL' => DEF_MAIL,

	'CAN_SENT_EMAIL' => false,

	//Режим работы сайта
	'SITE_STATUS' => 'ACTIVE',

	//security
    'API_SALT' => md5('my api salt=> change it for other app instances!'),

	'USER_AUTH_SALT' => md5('abrakadabra123'),

	'ADMIN_AUTH_SALT' => md5('abrakadabra123'),

	'MAIL_KEY_SALT' => md5('12312321321'),

	//администратор
	//почта администратора
	'ADMIN_MAIL' => DEF_MAIL,

	'SMTP_CONFIG' => array(
		'port' => '465',
		'timeout' => '30',
		'auth' => true,
		'host' => 'ssl://smtp.gmail.com',
		'username' => '{GMAIL_USER}@gmail.com',
		'password' => '{GMAIL_PASS}',
	),

	'VALID_DEV_IPS' => array(
		'127.0.0.1',
	),

	//административная панель
	'ADMIN_PANEL' => 'Административная панель',
    'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'upload',
	//'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'temp_image',

	//Директория загрузки изображений дизайна
	'DESIGN_IMAGE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'img' . DS . 'design',
	'DESIGN_IMAGE_DIR_RELATIVE' => '/img' . "/" . 'design' . "/",

	// Загрузка фотки пользователя
    'USER_AVATAR_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'images' . DS . 'users',
    'USER_AVATAR_UPLOAD_DIR_RELATIVE' => 'images' . DS . 'users',

	//Директория загрузки пользовательских изображений
	'USER_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'product_images',
	'USER_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'product_images',

    //Директория загрузки пользовательских изображений
    'PRODUCT_IMAGE_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'product_images',
    'PRODUCT_IMAGE_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'product_images',

	//Директория загрузки  изображений администратором и редактором
	'ADMIN_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'admin_files',

	//WEBROOT
	'WEBROOT' => dirname(__DIR__) . DS . 'webroot',

    //Директория загрузки импорта товаров
    'PRODUCT_IMPORT_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'imports',
    'PRODUCT_IMPORT_UPLOAD_DIR_RELATIVE' => 'imports',

	//пользовательский конфиг загрузки изображений
	'USER_IMAGE_UPLOAD_CONFIG' => array(
		'ext' => array("jpg", "jpeg", "png", "gif"),
		'max_file_size' => 5 * 1000 * 1000,
		'max_x' => 2000,
		'max_y' => 2000,
		'image' => 'true',
	),

    'API_LOG' => dirname(__DIR__) . DS . '..' . DS . 'logs' . DS . 'api.log',
);