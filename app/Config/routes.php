<?php

$api_prefix = "api";
Router::connect('/', array('controller' => 'Index', 'action' => 'pass_list'));
// status system
Router::connect('/status', array('controller' => 'Index', 'action' => 'status'));
Router::connect('/settings', array('controller' => 'Index', 'action' => 'settings'));
Router::connect('/image/:id', array('controller' => 'Index', 'action' => 'image'));
Router::connect('/image_pass/:id', array('controller' => 'Index', 'action' => 'image_pass'));
Router::connect('/image_event/:id', array('controller' => 'Index', 'action' => 'image_event'));
Router::connect('/group', array('controller' => 'Index', 'action' => 'group'));
Router::connect('/group_status/:id', array('controller' => 'Index', 'action' => 'group_status'));
Router::connect('/operator/set_pass', array('controller' => 'Operator', 'action' => 'set_pass'));
Router::connect('/person/add_face_verify/:person_id', array('controller' => 'Person', 'action' => 'add_face_verification_document'));
Router::connect('/person/set_status_view/', array('controller' => 'Person', 'action' => 'set_status_view'));


Router::connect('/auth', array('controller' => 'Operator', 'action' => 'login'));
Router::connect('/logout', array('controller' => 'Operator', 'action' => 'logout'));

Router::connect('/control_list', array('controller' => 'Index', 'action' => 'control_list'));

Router::connect('/autogate/set_off_and_open/:gate_id', array('controller' => 'Index', 'action' => 'set_off_and_open'));
Router::connect('/autogate/set_off_and_close/:gate_id', array('controller' => 'Index', 'action' => 'set_off_and_close'));
Router::connect('/autogate/set_on/:gate_id', array('controller' => 'Index', 'action' => 'set_on'));

Router::connect('/pass_list/:gate_id', array('controller' => 'Index', 'action' => 'pass_list'));
Router::connect('/pass_list/', array('controller' => 'Index', 'action' => 'pass_list'));
Router::connect('/pass_list', array('controller' => 'Index', 'action' => 'pass_list'));
Router::connect('/pass_data/:pass_id', array('controller' => 'Index', 'action' => 'pass_data'));


Router::connect('/operators', array('controller' => 'Operator', 'action' => 'operator_list'));
Router::connect('/operator/view/:id', array('controller' => 'Operator', 'action' => 'view'));
Router::connect('/operator/add', array('controller' => 'Operator', 'action' => 'add'));
Router::connect('/operator/edit/:id', array('controller' => 'Operator', 'action' => 'edit'));
Router::connect('/operator/reset_password/:id', array('controller' => 'Operator', 'action' => 'reset_password'));
Router::connect('/operator/set_status/:id', array('controller' => 'Operator', 'action' => 'set_status'));
Router::connect('/person/delete_document/:id', array('controller' => 'Person', 'action' => 'delete_document'));
Router::connect('/person/reset_document/:id', array('controller' => 'Person', 'action' => 'reset_document'));




//список групп автогейстов для управления
Router::connect('/dashboard', array('controller' => 'Index', 'action' => 'group_list'));
Router::connect('/dashboard/status_ajax/', array('controller' => 'Index', 'action' => 'dashboard_status'));
Router::connect('/dashboard/get_pass_data/', array('controller' => 'Index', 'action' => 'get_pass_data'));

Router::connect('/dashboard/force_access/', array('controller' => 'Index', 'action' => 'forceAccess'));

Router::connect('/dashboard/:group_id', array('controller' => 'Index', 'action' => 'dashboard'));

//CONTROL_LIST
Router::connect('/control_list/view/:list_id', array('controller' => 'ControlList', 'action' => 'list_view'));

Router::connect('/control_list/create_list/', array('controller' => 'ControlList', 'action' => 'create_list'));
Router::connect('/control_list/create_rule/:list_id', array('controller' => 'ControlList', 'action' => 'create_rule'));

Router::connect('/control_lists', array('controller' => 'ControlList', 'action' => 'control_lists'));

Router::connect('/control_list/delete/list/:list_id', array('controller' => 'ControlList', 'action' => 'delete_list'));
Router::connect('/control_list/delete/rule/:rule_id', array('controller' => 'ControlList', 'action' => 'delete_rule'));

Router::connect('/control_list/set_status/rule/:rule_id', array('controller' => 'ControlList', 'action' => 'set_rule_status'));
Router::connect('/control_list/set_status/list/:list_id', array('controller' => 'ControlList', 'action' => 'set_list_status'));

Router::connect('/control_list/update/list/:list_id', array('controller' => 'ControlList', 'action' => 'update_list'));
Router::connect('/control_list/update/rule/:rule_id', array('controller' => 'ControlList', 'action' => 'update_rule'));

Router::connect('/control_list/matches/:list_id', array('controller' => 'ControlList', 'action' => 'control_list_matches'));
Router::connect('/control_list/test/', array('controller' => 'ControlList', 'action' => 'controlListTest'));




Router::connect('/test_ag', array('controller' => 'Index', 'action' => 'initTestPass'));
Router::connect('/test_pass_done', array('controller' => 'Index', 'action' => 'testPassDone'));

Router::connect('/reset_test_ag', array('controller' => 'Index', 'action' => 'resetTestPass'));
Router::connect('/test_gd', array('controller' => 'Index', 'action' => 'testGDImage'));


Router::connect('/person/edit/:person_id', array('controller' => 'Person', 'action' => 'edit_person'));
Router::connect('/person/:person_id/add_document/', array('controller' => 'Person', 'action' => 'add_document'));
Router::connect('/person/view/:person_id', array('controller' => 'Person', 'action' => 'person_view'));
Router::connect('/persons', array('controller' => 'Person', 'action' => 'persons'));
Router::connect('/add_person', array('controller' => 'Person', 'action' => 'add_person'));
Router::connect('/person/:person_id/set_status/', array('controller' => 'Person', 'action' => 'update_person_status'));
Router::connect('/person/add_photo', array('controller' => 'Person', 'action' => 'addPersonPhoto'));
Router::connect('/person/delete/:person_id', array('controller' => 'Person', 'action' => 'delete_person'));
// подтвердить документ с параметрами
Router::connect('/person/accept_document/:pass_id', array('controller' => 'Person', 'action' => 'accept_document'));
Router::connect('/person/load_doubtful_document/:pass_id', array('controller' => 'Person', 'action' => 'load_doubtful_document'));
Router::connect('/person_inside', array('controller' => 'Person', 'action' => 'personInside'));

Router::connect('/set_document_off/:id', array('controller' => 'Person', 'action' => 'set_document_off'));
Router::connect('/set_document_on/:id', array('controller' => 'Person', 'action' => 'set_document_on'));
Router::connect('/person/:person_id/add_document', array('controller' => 'Person', 'action' => 'add_document'));
Router::connect('/person/:person_id/edit_document/:document_id', array('controller' => 'Person', 'action' => 'edit_document'));
Router::connect('/show_pass', array('controller' => 'Operator', 'action' => 'show_pass'));



Router::connect('/person/get_ajax_list/', array('controller' => 'Person', 'action' => 'get_ajax_list'));





Router::connect('/test', array('controller' => 'Index', 'action' => 'test'));







Router::connect('/' . $api_prefix . '/:api_version/', array('controller' => 'Index', 'action' => 'index'));

CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';