<?php

App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

class TestapiShell extends Shell
{
	public $uses = array();

	public $components = array();

	public function main()
	{
        $url = 'testurl';

        $result_data_raw = getUrl($url);
        $this->out( 'test url ' . print_r($result_data_raw, true));
	}

}