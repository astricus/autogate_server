<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class AdminController extends AppController
{
    public $uses = array(
        'Autogate',
        'Person',
        'Operator',
        'Token',
        'Auth'
    );

    public $components = array(
        'Activity',
        'Cookie',
        'AutogateCom',
        'Flash',
        'OperatorCom',
        'PersonCom',
        'Session',
        'Uploader',
        'Validator',
    );

    public $layout = "default";

    private $MIN_PASS_LENGTH = 8;

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    // РЕГИСТРАЦИЯ нового оператора

    public function register_form()
    {
    }

    public function registerOperator()
    {
        $errors = [];
        if ($this->request->is('get')) {
            $errors[] = " Operator registration requires http-post method";
            response_api($errors, "error");
            exit;
        }
        $data = $this->params['data'];

        if (empty($data['Login'])) {
            $errors[] = "required field is empty: Login";
            exit;
        }

        // валидация фио, login
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $name_regexp = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";

          $data['password'] = trim($data['password']);
        $data['firstname'] = trim($data['firstname']);
        $data['lastname'] = trim($data['lastname']);
        $data['middlename'] = trim($data['middlename']);
        $data['firstname'] = mb_ucfirst(mb_strtolower($data['firstname']));
        $data['lastname'] = mb_ucfirst(mb_strtolower($data['lastname']));
        $data['middlename'] = mb_ucfirst(mb_strtolower($data['middlename']));

//        if (!preg_match($mail_regexp, $data['email'])) {
//            $errors[] = "incorrect field: email";
//        }

        if (!empty($data['Operator']['lastname'])) {
            if (!preg_match($name_regexp, $data['lastname'])) {
                $errors[] = "incorrect field: lastname";
            }
        }

        if (!$this->Validator->valid_firstname($data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!$this->Validator->valid_firstname($data['lastname'])) {
            $errors[] = "incorrect field: lastname";
        }

        if (!$this->Validator->valid_password($data['password'])) {
            $errors[] = "incorrect field: password";
        }

//        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
//            $errors[] = "incorrect field: email";
//        }

        if ($this->OperatorCom->checkLoginExists($data['Login'])) {
            $errors[] = "login exists ";
        }

//        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
//            $errors[] = "incorrect field: email";
//        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }
        //данные пользователя
        $user_data["firstname"] = isset($data["FirstName"]) ? $data["firstname"] : null;
        $user_data["lastname"] = isset($data["LastName"]) ? $data['Register']["lastname"] : null;
        $user_data["Login"] = isset($data["Login"]) ? $data["Login"] : null;

        //генерация пароля с солью
        $real_pwd = $data["password"];
        $user_data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $this->Operator->save($user_data);

        $op_id = $this->Operator->getLastInsertId();


        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */

        //$message_data = $this->getRegistrationMessage($data["firstname"], $real_pwd, $mail_key, $user_data["email"]);
        response_api($user_data, "success");
        /*
        $this->EmailCom->sendEmailNow(
            $user_data["email"],
            "",
            "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $attachment = "",
            $start_sending = ""
        );
        */

        // отправка письма через очередь
        /*
        $this->EmailCom->sendEmailLater(
            $user_data["email"], "", "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $this->EmailCom->critical_priority,
            "",
            date('Y-m-d H:i:s'),
            0
        );*/

        //$this->EmailCom->sendRegisterOrdererEmail($data["firstname"], $user_data["email"], "REGISTER_ON_PROJECT" . " " . site_url(), $message_data);
        exit;
    }


    //== сырой код

    // Смена пароля
    public function changePassword()
    {
        if ($this->UserCom->is_auth()) {
            if (count($this->params->data) > 0) {
                // Параметры пришли POST данными или строкой json
                if (!empty($this->request->data('current_password')) AND !empty($this->request->data('new_password'))) {
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $this->request->data('current_password'));
                    $new_password = trim($this->request->data('new_password'));
                } else {
                    $data = json_decode(array_key_first($this->params->data));
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $data->current_password);
                    $new_password = trim($data->new_password);
                }

                if ($new_password == '' or mb_strlen($new_password) < $this->MIN_PASS_LENGTH) {
                    response_api(['error' => 'Пароль меньше 8 символов'], 'error');
                    exit();
                }

                $user_data = $this->UserCom->user_data();
                $user_id = $user_data['Orderer']['id'];

                if ($hash_current_pass == $user_data['Orderer']['password']) {
                    $res = $this->UserCom->changePassword($user_id, $new_password);
                    if ($res) {
                        response_api(['success' => true], 'success');
                    } else {
                        response_api(['error' => 'Новый пароль не сохранен'], 'error');
                    }
                } else {
                    response_api(['error' => 'Пароли не совпали'], 'error');
                }
            } else {
                response_api(['error' => 'Данные не пришли'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit();
    }

    // Отправить письмо с новым паролем
    public function resetPasswordSendNew()
    {
        if (!empty($this->request->data('login'))) {
            $email = trim($this->request->data('login'));
            $user_data = $this->UserCom->getUserDataByEmail($email);

            if ($user_data) {
                //$hash = get_hash(Configure::read('USER_AUTH_SALT'), $this->UserCom->generateRandomPass(10));
                $new_password = $this->UserCom->generateRandomPass(10);
                $hash_new_password = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);

                $text_message = 'Временный пароль от личного кабинета: ' . $new_password;//.Router::url('/', true).'route?token='.$hash;
                $recipiet_name = $user_data['firstname'] . ' ' . $user_data['lastname'];

                $email_send = $this->EmailCom->sendBaseEmail($recipiet_name, $email, 'Новый пароль - terpikka.ru', $text_message);
                if ($email_send) {
                    $this->Orderer->id = $user_data['id'];
                    $this->Orderer->save(['password' => $hash_new_password]);
                    response_api(['success' => true], 'success');
                } else {
                    response_api(['error' => 'Письмо с новым паролем не ушло'], 'error');
                }
            } else {
                response_api(['error' => 'Пользователь с таким логином не найден'], 'error');
            }
        } else {
            response_api(['error' => 'Логин не передан'], 'error');
        }

        exit();
    }

    // Сменить пароль
    public function resetPassword()
    {

    }

    // Последние просмотренные товары
    public function lastView()
    {
        $count = 10; // Количество последних просмотренных товаров
        if ($this->UserCom->is_auth()) {
            $user_id = $this->UserCom->getUserId();
            $lastView = $this->ProductCom->getLastProductViewForUser($user_id, $count);
            if ($lastView) {
                response_api(['product_id' => $lastView], 'success');
            } else {
                response_api(['success' => []], 'success');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    //УПРАВЛЕНИЕ ПРОФИЛЕМ И ЛИЧНЫМИ ДАННЫМИ

    public function saveProfile()
    {
        /*
         * имя, фамилия, отчество, пол, д.р., телефон, почта, город, адреса (массив), пароль новый, пароль старый, изображение, подписка на россылку Полезные статьи, подписка скидки и спецпредложения
         *
         * */
        $this->UserCom->auth_requires();
        $errors = [];
        $data = $this->params['data'];

        // валидация фио, почты, телефона
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $data['firstname'] = trim($data['firstname']);
        $data['email'] = trim($data['email']);
        $data['password'] = trim($data['password']);

        if (!empty($data['email'])) {
            if ($this->checkEmailExists($data['email']) > 0) {
                $errors[] = "email exists ";
            }
        }

        if (!preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

//		if (!preg_match($phone_regexp, $data['phone'])) {
//            $errors[] = "incorrect field: phone number";
//		}

//        if (!empty($data['Register']['lastname'])) {
//            if (!preg_match($name_regexp, $data['lastname'])) {
//                $errors[] = "incorrect field: lastname";
//            }
//        }

        if (!$this->Validator->valid_firstname($data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!$this->Validator->valid_password($data['password'])) {
            $errors[] = "incorrect field: password";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }
        //данные пользователя
        $user_data["firstname"] = isset($data["firstname"]) ? $data["firstname"] : null;
        //$user_data["lastname"] = isset($data["lastname"]) ? $data['Register']["lastname"] : null;
        $user_data["email"] = isset($data["email"]) ? $data["email"] : null;
        //$user_data["city_id"] = isset($user_data["city_id"]) ? $data["city_id"] : 0;
        //$user_data["country_id"] = isset($user_data["country_id"]) ? $data["country_id"] : 0;
        $user_data["money_amount"] = 0;
        $user_data["main_foto"] = "";
        $user_data["last_activity"] = date("Y-m-d H:i:s");
        $user_data["uptime"] = date("Y-m-d H:i:s");
        //$user_data["User"]["phone"] = isset($data['Register']["phone"]) ? $data['Register']["phone"] : null;
        $user_data["mail_key"] = md5(time() . $user_data["email"]);
        //генерация пароля с солью
        $real_pwd = $data["password"];
        $user_data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $this->Orderer->save($user_data);

        $user_id = $this->Orderer->getLastInsertId();

        //ключ активации
        $mail_key_salt = Configure::read('MAIL_KEY_SALT');
        $mail_key = generate_mail_key($user_id, $mail_key_salt);
        $this->Orderer->id = $user_id;
        $this->Orderer->save(array('mail_key' => $mail_key));

        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */
        exit;
    }

    // Изменение профиля
    public function editProfile()
    {
        if ($this->UserCom->is_auth()) {
            $data = [];
            $error = [];
            $user_id = $this->UserCom->getUserId();
            $validation = $this->Validator;
            $param = $this->params['data'];

            if (!empty($param['firstname'])) {
                if ($validation->valid_firstname($param['firstname'])) {
                    $data['firstname'] = $param['firstname'];
                } else {
                    $error[] = 'Не корректное имя';
                }
            }

            if (!empty($param['lastname'])) {
                if ($validation->valid_lastname($param['lastname'])) {
                    $data['lastname'] = $param['lastname'];
                } else {
                    $error[] = 'Не корректная Фамилия';
                }
            }

            if (!empty($param['middlename'])) {
                if ($validation->valid_middlename($param['middlename'])) {
                    $data['middlename'] = $param['middlename'];
                } else {
                    $error[] = 'Не корректное отчество';
                }
            }

            if (!empty($param['sex'])) {
                if ($validation->valid_sex($param['sex'])) {
                    $data['sex'] = $param['sex'];
                } else {
                    $error[] = 'Не корректный пол';
                }
            }

            if (!empty($param['birthday'])) {
                if ($validation->valid_date($param['birthday'])) {
                    $data['birthday'] = date('Y-m-d', strtotime($param['birthday']));
                } else {
                    $error[] = 'Не корректная дата рождения';
                }
            }

            if (!empty($param['email'])) {
                if ($validation->valid_mail($param['email'])) {
                    $data['email'] = $param['email'];
                } else {
                    $error[] = 'Не корректный email';
                }
            }

            if (!empty($param['phone'])) {
                if ($validation->valid_phone_number($param['phone'])) {
                    $data['phone_number'] = $param['phone'];
                } else {
                    $error[] = 'Не корректный телефон';
                }
            }

            if (count($error) > 0) {
                $str = implode(", ", $error);
                response_api(['error' => $str], 'error');
            } else {
                $this->Orderer->id = $user_id;
                if (isset($data['middlename']) AND $data['middlename'] == "null") {
                    $data['middlename'] = null;
                }
                if (isset($data['lastname']) AND $data['lastname'] == "null") {
                    $data['lastname'] = null;
                }
                $this->Orderer->save($data);
                response_api(['success' => 'Данные пользователя изменены'], 'success');
            }
        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit();
    }

    public function confirmPhone()
    {
        if ($this->UserCom->is_auth()) {
            $user_data = $this->UserCom->user_data()['Orderer'];
            $code = $this->request->data['code'] ?? $this->request->query('code');
            $code = trim($code);

            if ($code == $user_data['last_code_sms']) {
                $this->Orderer->id = $user_data['id'];
                $this->Orderer->save(['phone_confirmed' => 1]);

                response_api(['success' => true], 'success');
            } else {
                response_api(['error' => 'Не верный код'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }




}