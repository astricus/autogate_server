<?php
//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');

//контроллер Пользователь

class AppController extends Controller
{
    public $uses = array();

    public $user_data;

    public $components = array(
        'Cookie',
        'Session',
        //'DebugKit.Toolbar',
        'OperatorCom'
    );

    public $not_auth_valid_methods = array('login', 'login_form', 'passreminder', 'auth', 'show_pass', 'set_pass');

    public function beforeFilter()
    {
        $auth_error = $this->request->query('auth_error') ?? null;
        $auth_error_text = $this->request->query('auth_error_text') ?? null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);

        $op_id = $this->OperatorCom->operator_id();
        if(!empty($op_id)) {
            if ($this->OperatorCom->checkPassIsNull($op_id)) {
                if ($this->request->controller != 'Operator' && $this->request->controller != 'operator' or $this->request->action != 'set_pass') {
                    $this->redirect(
                        [
                            'controller' => 'operator',
                            'action' => 'set_pass',
                        ]
                    );
                    return;
                }
            }
        }

        if(!$this->OperatorCom->is_auth()){
            if($this->request->controller != 'operator' OR
                !in_array($this->request->action, $this->not_auth_valid_methods)
            ){
                if ($this->request->is('post')) {
                    die("need to be auth");
                }
                else {
                    $this->redirect(
                        [
                            'controller' => 'operator',
                            'action' => 'login',
                        ]
                    );
                }
                return;
            }
        }

        $this->_setLanguage();

         $active_page = strtolower($this->request->controller);

        if($active_page == "index") {
            $active_page = "";
        }
        if($active_page == "autogates") {
            $active_page = "autogates";
        }
        if($active_page == "dashboard") {
            $active_page = "dashboard";
        }
        if($active_page == "operators") {
            $active_page = "operators";
        }
        if($this->request->controller == "Activity" AND $this->request->action == "index") {
            $active_page = "notifications";
        }

        $this->set('active_page', $active_page);
        $this->set('is_auth', $this->OperatorCom->is_auth() ? 'true' : 'false');
        $this->set('title', "Личный кабинет");
        $this->set('content_title', "");

        $user_data =  $this->OperatorCom->user_data();
        $operator_name = prepare_fio($user_data['Operator']['lastname'], $user_data['Operator']['firstname'], $user_data['Operator']['middlename']);
        $this->set('operator_name', $operator_name);
        $this->set('operator_id', $user_data['Operator']['id']);
        $operator_role = $user_data['Operator']['role'];
        $this->set('role', $operator_role);

        parent::beforeFilter();
    }

    private function _setLanguage()
    {
        $session_lang = ($this->Session->read('lang')) ? $this->Session->read('lang') : null;
        if ($session_lang == null) {
            $session_lang = Configure::read('DEF_LANG');

            Configure::load('rus_config');

        } else {
            if (!in_array($session_lang, Configure::read('VALID_LANGS'))) {
                $session_lang = Configure::read('DEF_LANG');
                Configure::load('rus_config');
            }
        }
        $this->Session->write('lang', $session_lang);
        $c = 0;
        foreach (Configure::read('VALID_LANGS') AS $lang) {

            if ($lang == $session_lang) {
                $key = $c;
            }
            $c++;
        }
        $locale = Configure::read('VALID_LANG_LOCALES')[$key];
        //запись в конфиг локали
        Configure::write('Config.language', $locale);
        Configure::load($locale . '_config');
        $this->Session->write('lang_locale', $locale);

        $lang_prefix = 'name_' . lang_prefix();
        $this->set('lang_prefix', $lang_prefix);
    }

}