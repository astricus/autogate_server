<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ControlListController extends AppController
{
    public $uses = array();

    public $components = array(
        'Session',
        'ControlRule',
        'PersonCom',
        'Flash'
        //'AutogateCom'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $auth_error = (isset($this->request->query['auth_error'])) ? $this->request->query['auth_error'] : null;
        $auth_error_text = (isset($this->request->query['auth_error_text'])) ? $this->request->query['auth_error_text'] : null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);
        $this->set('show_login_form', $show_login_form);
        parent::beforeFilter();
    }

    public function control_lists()
    {
        $lists = $this->ControlRule->controlListAll();
        $this->set("control_lists", $lists);
        $logic_action_list = $this->ControlRule->logic_action_list;
        $this->set("logic_action_list", $logic_action_list);
        $direction_list = $this->ControlRule->direction_list;
        $this->set("direction_list", $direction_list);
    }

    public function list_view()
    {
        $list_id = $this->request->param("list_id") ?? null;
        if ($list_id == null) {
            die("undefined list!");
        }
        $list = $this->ControlRule->getListById($list_id);
        $list_name = $list['Control_List']['list_name'];

        $check_delete = $this->ControlRule->checkPassesByControlList($list_id);
        $this->set("list_name", $list_name);
        $this->set("control_lists", ["0" => $list]);

        $logic_action_list = $this->ControlRule->logic_action_list;
        $this->set("logic_action_list", $logic_action_list);

        $direction_list = $this->ControlRule->direction_list;
        $this->set("direction_list", $direction_list);

        $rules = $this->ControlRule->getRulesDataByListId($list_id);
        $this->set("rules", $rules);
        $this->set("check_delete", $check_delete);
    }

    public function update_rule()
    {
        $rule_id = $this->request->param("rule_id") ?? null;
        if ($this->request->is('post')) {
            $rule_id = $this->request->data('rule_id') ?? null;
            $lastname_mask = $this->request->data('lastname_mask') ?? null;
            $firstname_mask = $this->request->data('firstname_mask') ?? null;
            $document_mask = $this->request->data('document_mask') ?? null;
            $person_id = $this->request->data('person_id') ?? null;
            $result = $this->ControlRule->updateRule($rule_id, $firstname_mask, $lastname_mask, $document_mask, $person_id);
            if ($result) {
                $result = ["message" => "Правило успешно обновлено!"];
                response_ajax($result, "success");
            } else {
                $result = ["message" => $this->ControlRule->error, "status" => "error"];
                response_ajax($result, "error");
            }
            exit;
        } else {
            if ($rule_id == null) {
                die("undefined rule!");
            }
            $rule = $this->ControlRule->getRuleById($rule_id);
            $list_name = $this->ControlRule->getListById($rule[0]['Control_Rule']['control_list_id']);
            if(($rule[0]['Control_Rule']['person_id']>0)){
                $person_name = $this->PersonCom->getPersonNameById($rule[0]['Control_Rule']['person_id']);
                $this->set("person_name", $person_name);
            } else {
                $this->set("person_name", "");
            }
            $this->set("rule", $rule[0]);
            $this->set("list_name", $list_name['Control_List']['list_name']);
        }
    }

    public function update_list()
    {
        $list_id = $this->request->param("list_id") ?? null;
        if ($this->request->is('post')) {
            $list_id = $this->request->data('list_id') ?? null;
            $list_name = $this->request->data('list_name') ?? null;
            $description = $this->request->data('description') ?? null;
            $list_owner = $this->request->data('list_owner') ?? null;
            $direction = $this->request->data('direction') ?? null;
            $action_logic = $this->request->data('action_logic') ?? null;
            $result = $this->ControlRule->updateList($list_id, $list_name, $description, $list_owner, $direction, $action_logic);
            if ($result) {
                $result = ["message" => "Список успешно обновлен!"];
                response_ajax($result, "success");
            } else {
                $result = ["message" => $this->ControlRule->error, "status" => "error"];
                response_ajax($result, "error");
            }
            exit;
        } else {
            if ($list_id == null) {
                die("undefined ist!");
            }
            $list = $this->ControlRule->getListById($list_id);
            $this->set("list", $list['Control_List']);

            $logic_action_list = $this->ControlRule->logic_action_list;
            $this->set("logic_action_list", $logic_action_list);

            $direction_list = $this->ControlRule->direction_list;
            $this->set("direction_list", $direction_list);
        }
    }

    public function create_list()
    {
        if ($this->request->is('post')) {
            $name = $this->request->data('list_name') ?? null;
            $direction = $this->request->data('direction') ?? null;
            $action_logic = $this->request->data('action_logic') ?? null;
            $description = $this->request->data('description') ?? null;
            $list_owner = $this->request->data('list_owner') ?? null;
            $list_id = $this->ControlRule->createList($name, $description, $list_owner, $direction, $action_logic);

            if ($list_id) {
                $result = ["message" => "Список $name был успешно создан!", "list_id" => $list_id];
                response_ajax($result, "success");
            } else {
                $result = ["message" => $this->ControlRule->error, "status" => "error"];
                response_ajax($result, "error");
            }
            exit;
        }
        $logic_action_list = $this->ControlRule->logic_action_list;
        $this->set("logic_action_list", $logic_action_list);
        $direction_list = $this->ControlRule->direction_list;
        $this->set("direction_list", $direction_list);
    }

    public function create_rule()
    {
        $list_id = $this->request->param("list_id");


        if ($this->request->is('post')) {
            $list_id = $this->request->data('list_id') ?? null;
            $lastname_mask = $this->request->data('lastname_mask') ?? null;
            $firstname_mask = $this->request->data('firstname_mask') ?? null;
            $document_mask = $this->request->data('document_mask') ?? null;
            $person_id = $this->request->data('person_id') ?? null;
            $rule_id = $this->ControlRule->createRule($list_id, $firstname_mask, $lastname_mask, $document_mask, $person_id);

            if ($rule_id > 0) {
                $result = ["message" => "Правило успешно добавлено!", "rule_id" => $list_id];
                response_ajax($result, "success");
            } else {
                $result = ["message" => $this->ControlRule->error, "status" => "error"];
                response_ajax($result, "error");
            }
            exit;
        }
        $list = $this->ControlRule->getListById($list_id);
        $list_name = $list['Control_List']['list_name'];
        $this->set("list_name", $list_name);
        $this->set("list_id", $list_id);
    }

    public function delete_list()
    {
        $list_id = $this->request->param("list_id") ?? $this->request->data;
        if (empty($list_id) or intval($list_id) <= 0) {
            $result = ["message" => "Не указан идентификатор листа для удаления", "status" => "error"];
            response_ajax($result, "error");
            exit;
        }
        $result = $this->ControlRule->deleteList($list_id);
        if ($result) {
            $result = ["message" => "Список был успешно удален вместе с правилами"];
            response_ajax($result, "success");
        } else {
            $result = ["message" => $this->ControlRule->error, "status" => "error"];
            response_ajax($result, "error");
            if(count($this->ControlRule->error)>0){
                $error_list = "";
                foreach ($this->ControlRule->error as $err_item){
                    $error_list.= $err_item;
                }
                $this->Flash->set("Произошли ошибки, $error_list", array("params" => array("class" => "alert alert-danger")));
            }
        }
        exit;
    }

    public function delete_rule()
    {
        $list_id = $this->request->query("list_id") ?? null;
        $redirect_uri = "/control_list/view/" . $list_id;
        $rule_id = $this->request->param("rule_id") ?? null;
        if (empty($rule_id) or intval($rule_id) <= 0) {
            $this->redirect($redirect_uri);
            exit;
        }
        $this->ControlRule->deleteRule($rule_id);

        if(count($this->ControlRule->error)>0){
            $error_list = "";
            foreach ($this->ControlRule->error as $err_item){
                $error_list.= $err_item;
            }
            $this->Flash->set("Произошли ошибки, $error_list", array("params" => array("class" => "alert alert-danger")));
        }
        $this->redirect($redirect_uri);
        exit;
    }

    public function set_rule_status()
    {
        $list_id = $this->request->query("list_id") ?? null;
        $set_status = $this->request->query("status") ?? null;
        $redirect_uri = "/control_list/view/" . $list_id;
        $rule_id = $this->request->param("rule_id") ?? null;
        if (empty($rule_id) or intval($rule_id) <= 0) {
            $this->redirect($redirect_uri);
            exit;
        }
        if ($set_status == "active") {
            $this->ControlRule->setOnRule($rule_id);
        } else {
            $this->ControlRule->setOffRule($rule_id);
        }
        $this->redirect($redirect_uri);
        exit;
    }

    public function set_list_status()
    {
        $list_id = $this->request->param("list_id") ?? null;
        $set_status = $this->request->query("status") ?? null;
        $redirect_uri = "/control_lists";
        if (empty($list_id) or intval($list_id) <= 0) {
            $this->redirect($redirect_uri);
            exit;
        }
        if ($set_status == "active") {
            $this->ControlRule->setOnList($list_id);
        } else {
            $this->ControlRule->setOffList($list_id);
        }
        $this->redirect("/control_lists");
        exit;
    }

    public function controlListTestData()
    {

        $user1 = [
            'firstname' => 'Данила',
            'lastname' => 'Макаров',
            'document_number' => '5566 8800',
            'person_id' => '',
        ];
        $user2 = [
            'firstname' => 'Макар',
            'lastname' => 'Макаренко',
            'document_number' => '4556 6001 ',
            'person_id' => '2',
        ];
        $user3 = [
            'firstname' => 'Петр',
            'lastname' => 'Петров',
            'document_number' => '32005566',
            'person_id' => '',
        ];
        $user4 = [
            'firstname' => 'Mark',
            'lastname' => 'Jonson',
            'document_number' => 'Макаров',
            'person_id' => '',
        ];
        $user5 = [
            'firstname' => 'Andrey',
            'lastname' => 'Popov',
            'document_number' => '540020232',
            'person_id' => '4',
        ];
        return [$user1, $user2, $user3, $user4, $user5];
    }

    public function controlListTest()
    {
        $result_rule_list = [];
        foreach ($this->controlListTestData() as $test_user) {
            $user = $test_user['firstname'] . " " . $test_user['lastname'] . " " . $test_user['person_id'];
            $list = [];
            $result = $this->ControlRule->checkRuleMatch($test_user['firstname'], $test_user['lastname'], $test_user['document_number'], $test_user['person_id']);
            if($result!=null){
                foreach ($result as $result_list_item){
                    if(!in_array($result_list_item, $result_rule_list)){
                        $list[] = $result_list_item;
                    }
                }

            }
            $result_rule_list[$user] = $list;
        }
        pr($result_rule_list);

        pr($this->controlListTestData());
        exit;
    }


}