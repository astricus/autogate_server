<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class IndexController extends AppController
{
    public $uses = array(
        'Biometric'
    );


    /*
     * к.с.: поменять местами поля, сделать описание, добавить проверку
     * контрльные списки - поменять местами поля в выводе, проверить почему не работает на эксплорере
     * ! добавить проверку возможности активации индентификации по лицу только для тех, у кого есть шаблон в биометрии
     * ! добавить классы для изображений в другие блоки
     * добавить маску для паспорта рф
     * ! дизайн галереи улучшить - кнопка, поля
     * */


    /*
     * успешно, создание, добавление, активация - зеленый
     * удаление - красный
     * запретить, сброситб (сведения/пароль) = желтый
     * изменение, Изменить - голубой
     * Добавить, сохранить - темно-синий
     * Отмена - желтый
     *
     * */

    /*
     ! пагинация по первой букве фамилии
     ! сортировка по фамилии
     ! вместо даты доб указать  отдел
     ! чекбокс - показывать всех или только активных (по умолчанию)
     ! фильтр по организации
     ! показывать ошибку при добавлении документа если документ не был создан
     *
     * ! под вопросом --- в добавлении и редактировании список подразделение, организацию и отдел
     ! при редактировании паспорта порядок полей должен быть как и при добавлении,
     ! убрать № документа в заглавии "редактирование документа для"
     ! на странице просмотра сделать две колонки фото и данные
     ! при выводе проходов в персоне вместо номера к.с. указывать его название
     *
     ! разобраться с названиями и цветами кнопок для действий
     ! повесить обработчик на сброс подтверждения действий (сброс сведений, удалений, плюс запреты)
     ! Добавить ссылку на правила списка в редактирование
     ! ри добавлении правила в список сообщение переименовать
     ! у текущего пользователя не сбрасывать пароль
     ! проверка того, что у персоны при добавлении нового документа документ с такими данными не привязан к другой персоне в статусе Активен
     * */

    public  $show_pass_count = 50;

    public $components = array(
        'Session',
        'OperatorCom',
        'AutogateCom',
        'PersonCom'
    );

    public $layout = "default";

    /*
     * jobs:
     * 1. обертка над обоими автогейтами, в меню перенести вниз, три статуса как и сейчас для обертки !
     * 2. мастшабирование изображений - функция resize_image !
     * 3. Показ где-то в интерфейсе pdn_agree !
     *
     * */

    public function beforeFilter()
    {
        $auth_error = (isset($this->request->query['auth_error'])) ? $this->request->query['auth_error'] : null;
        $auth_error_text = (isset($this->request->query['auth_error_text'])) ? $this->request->query['auth_error_text'] : null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);
        $this->set('show_login_form', $show_login_form);
        parent::beforeFilter();
    }

    public function image(){
        $id = $this->request->param('id') ?? null;
        return $this->PersonCom->get_image("Biometric", $id);
    }

    public function image_pass(){
        $id = $this->request->param('id') ?? null;
        return $this->PersonCom->get_image("Pass_List", $id);
    }

    public function image_event(){
        $id = $this->request->param('id') ?? null;
        return $this->PersonCom->get_image("Event", $id);
    }

    public function status()
    {
        response_api(null, "success");
        exit;
    }

    public function settings()
    {
    }

    public function group()
    {
        $this->Autogate = ClassRegistry::init("Autogate");
        $autogates = $this->Autogate->find("all",
            array('conditions' =>
                array(
                    'gate_group' => 1,
                ),
                'limit' => '1',
                'order' => array('Autogate.id DESC'),
            )
        );
        $this->set("autogates", $autogates);
    }

    public function index()
    {
        $autogates = $this->OperatorCom->getActiveAutoGates();
        $this->set("autogates", $autogates);

        $user_data = $this->OperatorCom->user_data();
        $role = $user_data['Operator']['role'];
        if ($role == "security") {
            $this->redirect("/dashboard/1");
        }
    }

    //список групп автогейстов (объединенных автогейстов одной калитки)
    public function group_list()
    {
        $autogates = $this->OperatorCom->getActiveAutoGateGroups();
        $this->set("autogate_group_list", $autogates);
    }

    public function set_off_and_close()
    {
        $gate_id = $this->request->param('gate_id') ?? null;
        $out_of_service = true;
        $doors_status = false;
        $this->AutogateCom->getGatesDoorsStatus($gate_id, $out_of_service, $doors_status);
        return $this->redirect(array('controller' => 'Index', 'action' => 'group'));
    }

    public function set_off_and_open()
    {
        $gate_id = $this->request->param('gate_id') ?? null;
        $out_of_service = true;
        $doors_status = true;
        $this->AutogateCom->getGatesDoorsStatus($gate_id, $out_of_service, $doors_status);
        return $this->redirect(array('controller' => 'Index', 'action' => 'group'));
    }

    public function set_on()
    {
        $gate_id = $this->request->param('gate_id') ?? null;
        $out_of_service = false;
        $doors_status = true;
        $this->AutogateCom->getGatesDoorsStatus($gate_id, $out_of_service, $doors_status);
        return $this->redirect(array('controller' => 'Index', 'action' => 'group'));
    }

    public function dashboard()
    {
        $this->layout = "operator";
        $group_id = $this->request->param('group_id') ?? null;
        if ($group_id == null) {
            die("Autogate group identifier cannot be null!");
        }
        $autogates = $this->AutogateCom->getGatesByGateGroup($group_id);
        $this->set("autogates", $autogates);
    }

    public function group_status()
    {
        $this->layout = false;
        $group_id = $this->request->param('id') ?? null;
        if ($group_id == null) {
            die("Autogate group identifier cannot be null!");
        }
        $status = $this->AutogateCom->getGroupStatus($group_id);
        response_ajax($status, "success");
        exit;
    }

    public function dashboard_status()
    {
        // получение текущего статуса и истории смены статусов
        $gate_id = $this->request->query('id') ?? null;

        if ($gate_id == null) {
            $error = "Autogate identifier cannot be null!";
            $result = [];
            $result["error"] = $error;
            response_ajax($result, "success");
            exit;
        }
        $env = $this->AutogateCom->getAGEnvironment($gate_id);
        $out_of_service_status = $this->AutogateCom->getAGOutOfService($gate_id);
        $gate_status = $this->AutogateCom->getAGStatus($gate_id);
        $gate_substatus = null;
        if (substr_count($gate_status, ":") > 0) {
            $gate_status_arr = explode(":", $gate_status);
            $gate_status = $gate_status_arr[0];
            $gate_substatus = $gate_status_arr[1];
        }

        $amount = 5;
        $gate_group = 1;
        $gates = [];
        foreach ($this->AutogateCom->getGatesByGateGroup($gate_group) as $gate_elem) {
            $gates[] = $gate_elem['Autogate']['id'];
        }
        $last_passes = $this->AutogateCom->getLastPasses($gates, $amount);
        $pass_arr = [];
        foreach ($last_passes as $last_pass) {
            $pass_arr[] = [
                "name" => prepare_fio($last_pass['Person']['lastname'],
                    $last_pass['Person']['firstname'],
                    $last_pass['Person']['middlename']),
                "pass_time" => $last_pass["Pass_List"]["pass_time"],
                "list_id" => $last_pass["Control_List"]["id"],
                "list_name" => $last_pass["Control_List"]["list_name"],
                "pass_done" => $last_pass["Pass_List"]["pass_done"],
                "pass_allowed" => $last_pass["Pass_List"]["pass_allowed"],
                "pass_id" => $last_pass["Pass_List"]["pass_id"],
            ];
        }
        $last_passes = $pass_arr;

        $show_pass_window_statuses = [
            'IdentificationByFace',
            'ReadACMSCard',
            'ReadDocument',
            'GetInfoFromDB',
            'InteractiveForm',
            'VerifyByFingerprint',
            'VerifyByFace',
            'MakeDecision',
            'AccessGranted',
            'AccessDeny',
            'PassDone',
        ];

        $show_pass_window = "false";
        if (in_array($gate_status, $show_pass_window_statuses)) {
            $show_pass_window = "true";
        }


        $current_pass_id = $this->AutogateCom->getCurrentPassID($gate_id);
        $result = [
            "environment" => $env,
            "out_of_service_status" => $out_of_service_status,
            "gate_status" => $gate_status,
            "current_pass" => "$current_pass_id",
            "user_check" => null, //"document_alert"
            "show_pass_window" => $show_pass_window,
            "gate_substatus" => $gate_substatus,
            "last_passes" => $last_passes,
        ];
        response_ajax($result, "success");
        exit;
    }

    /*
     * 1. Пропала вторая полоса с устройствами пока не готово ссм
     * 2. желтый фон и черные буквы окно списка !
     * 3. убрать кнопки с действиями для охранника из списка !
     * 4. Не подгрузилась фотография во время прохода - лаосcкий паспорт андрей и фотография от россиского паспорта
     * 5. сбрасывать фотографию от прошлого прохода
     * 6. Добавить редактирование фОТОГРАФИИ в редактирование персоны !
     * 7. Документ при создании персоны не сохранился !
     * 8. Добавить сохранение нового документа для персоны !
     * 9. По умолчанию добавлять в статусе разрешенной персоны !
     * 10. При создании нового контрольного списка не выпадают направление и действие !
     * 11. в проходах переделать дизайн вместо кнопок на текст !
     * 12. убрать сериализованные данные из вывода прохода !
     * 13. перевести название из ивентов на русский язык в проходе !
     * 14. В контрольном списке не по id добавлять !
     *
     *
     * */

    /*
     * Загранпаспорт РФ если с чипом, то начинается с 7, если нет - то обычный.
     * Если паспорт с чипом то верификация по лицу доступна всегда, по отпечатку - при налиии
     * Если паспорт ЛАОС - верификация по лицу и отпечатку доступна всегда
     * Если внутрений/СКУД верификация доступна если есть соотв шаблон
     *
     *
     * */

    public function get_pass_data()
    {
        $pass_id = $this->request->query('pass_id') ?? null;
        $gate_id = $this->request->query('gate_id') ?? null;
        $last_event_id = $this->request->query('last_event_id') ?? null;
        if ($pass_id == null) {
            $error = "Pass identifier cannot be null!";
            $result = [];
            $result["error"] = $error;
            response_ajax($result, "success");
            exit;
        }
        $last_event_id = 0;
        $pass_data = $this->AutogateCom->getLastEvents($gate_id, $pass_id, $last_event_id);

        $pass_result = [];
        $new_event_id = $last_event_id;


        foreach ($pass_data as $pass_item) {
            if ($pass_item['Event']['object'] != "") {
                $value = base64_encode($pass_item['Event']['object']);
            } else {
                $value = unserialize($pass_item['Event']['event_data']);
            }
            $pass_result[$pass_item['Event']['event_type']] = $value;
            $new_event_id = $pass_item['Event']['id'];
        }

        $document_verify = ["status" => "true", "type" => "success", "text" => "Проверка успешно пройдена!"];

        //$automatic_decision = ["status" => "true", "type" => "success", "text" => "Пропустить!"];
        $automatic_decision = ["status" => "false", "type" => "danger", "text" => " Запретить!"];
        //$gate_init = ["status" => "wait", "type" => "info", "text" => " Не активно"];
        $gate_init = ["status" => "open", "type" => "success", "text" => "Ворота открыты!"];

        $user_pass = ["status" => "wait", "type" => "info", "text" => "Не активно"];
        //$user_pass = ["status" => "failure", "type" => "danger", "text" => "Таймаут прохода!"];
        //$user_pass = ["status" => "success", "type" => "success", "text" => "Пользователь прошел через автогейт"];

        //$database = ["status" => "wait", "type" => "info", "text" => ""];
        //$database = ["status" => "success", "type" => "success", "text" => ""];
        $database = ["status" => "success", "type" => "success", "text" => ""];

        $pass_trigger = ["status" => "camera", "type" => "success", "text" => " Проход по верификации лица через камеру"];
        //$pass_trigger = ["status" => "scud", "type" => "success", "text" => " Проход по приложенной карте скуд"];
        //$pass_trigger = ["status" => "passport", "type" => "success", "text" => " Проход по приложенному документу"];
        $images = ["real_image" => null, "db_image" => null];
        $camera_verify = ["status" => "true", "type" => "info", "text" => "Нет данных"];
        $fp_verify = ["status" => "true", "type" => "info", "text" => "Неt данных"];
        $person_organization = null;
        $person_division = null;
        $person_position = null;
        $person_name = null;
        $person_subdivision = null;
        $alert_doubtful = null;
        $control_list_detected = null;
        $required = null;
        if ($pass_id > 0) {
            $images = $this->PersonCom->getPersonPhotosByPassId($gate_id, $pass_id);
            $alert_doubtful = $this->PersonCom->checkDoubtfulDocument($pass_id);
            $all_pass_data = $this->AutogateCom->getAllPassData($pass_id);
            if ($all_pass_data['fingerprint_verification'] == "Unknown") {
                $fp_verify = ["status" => "true", "type" => "info", "text" => "Нет данных"];
            } else if ($all_pass_data['fingerprint_verification'] == "Required") {
                $fp_verify = ["status" => "true", "type" => "warning", "text" => "Проверка требуется"];
            } else if ($all_pass_data['fingerprint_verification'] == "NotRequired") {
                $fp_verify = ["status" => "true", "type" => "info", "text" => "Проверка не требуется"];
            } else if ($all_pass_data['fingerprint_verification'] == "Failed") {
                $fp_verify = ["status" => "false", "type" => "danger", "text" => "Неудачно"];
            } else if ($all_pass_data['fingerprint_verification'] == "Successfully") {
                $fp_verify = ["status" => "true", "type" => "success", "text" => "Успешно"];
            }

            if ($all_pass_data['face_verification'] == "Unknown") {
                $camera_verify = ["status" => "true", "type" => "info", "text" => "Нет данных"];
            } else if ($all_pass_data['face_verification'] == "Required") {
                $camera_verify = ["status" => "true", "type" => "warning", "text" => "Проверка требуется"];
            } else if ($all_pass_data['face_verification'] == "NotRequired") {
                $camera_verify = ["status" => "true", "type" => "info", "text" => "Проверка не требуется"];
            } else if ($all_pass_data['face_verification'] == "Failed") {
                $camera_verify = ["status" => "false", "type" => "danger", "text" => "Неудачно"];
            } else if ($all_pass_data['face_verification'] == "Successfully") {
                $camera_verify = ["status" => "true", "type" => "success", "text" => "Успешно"];
            }

            $camera_verify = ["status" => "false", "type" => "danger", "text" => "Проверка не пройдена!"];
            $document_verify = ["status" => "true", "type" => "success", "text" => "Проверка успешно пройдена!"];
            // }

            $pass_data = $this->AutogateCom->getPassData($gate_id, $pass_id);
            if ($pass_data['person_id'] > 0) {
                $person_data = $this->PersonCom->getPersonData($pass_data['person_id']);
                if ($person_data != null) {
                    $person_name = prepare_fio($person_data['Person']['lastname'], $person_data['Person']['firstname'], $person_data['Person']['middlename']);
                    $person_organization = $person_data['Person']['organization'];
                    $person_division = $person_data['Person']['division'];
                    $person_position = $person_data['Person']['position'];
                    $person_subdivision = $person_data['Person']['subdivision'];
                }
            }
            $control_list_detected = $this->AutogateCom->getControlListByPass($pass_id);
            $required = $this->PersonCom->getRequiredVerification($pass_id);

            if (is_array($required)) {
                if (in_array("Face", $required)) {
                    $camera_verify = ["status" => "wait", "type" => "warning", "text" => "Требуется верификация"];
                } else {
                    $camera_verify = ["status" => "success", "type" => "info", "text" => "Не требуется верификация"];
                }
                if (in_array("Fingerprint", $required)) {
                    $fp_verify = ["status" => "wait", "type" => "warning", "text" => "Требуется верификация"];
                } else {
                    $fp_verify = ["status" => "success", "type" => "info", "text" => "Не требуется верификация"];
                }
            }

        }

        $result = [
            "data" => $pass_result,
            "last_event_id" => $new_event_id,
            "fp_verify" => $fp_verify,
            "camera_verify" => $camera_verify,
            "document_verify" => $document_verify,
            "automatic_decision" => $automatic_decision,
            "gate_init" => $gate_init,
            "user_pass" => $user_pass,
            "database" => $database,
            "pass_trigger" => $pass_trigger,
            "real_image" => $images['real_image'],
            "db_image" => $images['db_image'],
            "is_pass_done" => (string)$this->AutogateCom->isPassDone($gate_id, $pass_id),
            "control_list" => "Проход по контрольному списку " . $control_list_detected,
            "document_params" => $alert_doubtful,
            "organization" => $person_organization,
            "division" => $person_division,
            "position" => $person_position,
            "user_name" => $person_name,
            "subdivision" => $person_subdivision,
            "required" => $required
            //"status" => $status,
        ];

        response_ajax($result, "success");
        exit;
    }

    public function getPassDataByName()
    {
        $pass_id = $this->request->query('pass_id') ?? null;
        $gate_id = $this->request->query('gate_id') ?? null;
        if ($pass_id == null) {
            $error = "Pass identifier cannot be null!";
            $result = [];
            $result["error"] = $error;
            response_ajax($result, "success");
            exit;
        }
        $data = $this->AutogateCom->getPassData($gate_id, $pass_id);
        $result = [
            "document" => $data['pass_document'],
            "document_type" => $data['document_type'],
            //"status" => $status,
        ];
        response_ajax($result, "success");
        exit;
    }

    public function pass_list()
    {
        $gate_id = $this->request->param('gate_id') ?? null;
        $sort_by = $this->request->query('sort_by') ?? null;
        $sort_dir = $this->request->query('dir') ?? null;

        $filter_organization = $this->request->query('organization') ?? null;
        $filter_person = $this->request->query('person') ?? null;
        $filter_doc_number = $this->request->query('doc_number') ?? null;

        $filters = [];
        if(!empty($this->request->query('person'))){
            $filters['person'] = $this->request->query('person');
        }
        if(!empty($this->request->query('organization'))){
            $filters['organization'] = $this->request->query('organization');
        }
        if(!empty($this->request->query('doc_number'))){
            $filters['doc_number'] = $this->request->query('doc_number');
        }

        $page = $this->request->query('page') ?? 1;

        $selection = $this->request->query('selection') ?? 'today';
        $this->set("selection", $selection);


        $this->set("sort_field", $sort_by);
        $this->set("sort_dir", $sort_dir);
        if($selection=="today"){
            $selection_date = date('Y-m-d');
        }
        else if($selection=="last_3_days"){
            $selection_date = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 3, date('Y')));
        }
        else if($selection=="last_week"){
            $selection_date = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
        }
        else if($selection=="last_month"){
            $selection_date = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 30, date('Y')));
        }
        else if($selection=="all_time"){
            $selection_date = null;
        }
        else {
            $selection_date = null;
        }

        $current_url = "/pass_list";

        $list = $this->AutogateCom->getGatePassListByDate($gate_id, $selection_date, $filters, $sort_by, $sort_dir, $page, false, $this->show_pass_count);

        $pass_count =  $this->AutogateCom->getGatePassListCountByDate($gate_id, $selection_date, $filters);


        foreach ($list as &$item) {

            $item['Pass_List']['fingerprint_verification'] = $item['Pass_List']['fingerprint_verification'] ?? 'Unknown';
            $item['Pass_List']['face_verification'] = $item['Pass_List']['face_verification'] ?? 'Unknown';
            $item['Pass_List']['fp_verification_status_name'] = $this->PersonCom->verification_status[$item['Pass_List']['fingerprint_verification']];
            $item['Pass_List']['face_verification_status_name'] = $this->PersonCom->verification_status[$item['Pass_List']['face_verification']];

            $item['Person']['name'] = prepare_fio($item['Person']['lastname'], $item['Person']['firstname'], $item['Person']['middlename']);
            $item['Person']['organization'] = $item['Person']['organization'] ?? 'Нет данных';
//            if ($item['Pass_List']['photo'] != null) {
//                $item['Pass_List']['photo_image'] = $this->PersonCom->generateImageFromBinary($item['Pass_List']['photo']);
//            } else {
            $item['Pass_List']['photo_image'] = null;
            //}
            if($item['Pass_List']['doc_type']=="Face"){
                $item['Pass_List']['doc_type'] = "Идентификация по лицу";
            }
            else if (!in_array($item['Pass_List']['doc_type'], array_keys($this->PersonCom->doc_type))) {
                $item['Pass_List']['doc_type'] = "UNKNOWN";
            } else {
                $item['Pass_List']['doc_type'] = $this->PersonCom->doc_type[$item['Pass_List']['doc_type']];
            }
            //pr($item);
        }
        $curr_url = server_path();
        $curr_url.= "?";
        if(!empty($selection)){
            $curr_url.= "&selection=$selection";
        }
        if(!empty($page)){
            $curr_url.= "&page=$page";
        }
        $this->set("path", $curr_url);
        $this->set("sort_by", $sort_by);
        $this->set("dir", $sort_dir);
        $this->set("list", $list);
        $this->set("gate_id", $gate_id);
        $this->set("pass_count", $pass_count);
        $pages = ceil($pass_count / $this->show_pass_count);
        $this->set("pages", $pages);
        $this->set("page", $page);
        $this->set("current_url", $current_url);
        $this->set("show_count", $this->show_pass_count);

        $table_header = [];
        $table_header[] = ["type" => "text", "field" => "n", "title" => "#", "class" => ""];
        $table_header[] = ["type" => "link", "field" => "pass_time", "title" => "время прохода", "class" => ($sort_by == "pass_time") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "pass_direction", "title" => "Направление", "class" => ($sort_by == "pass_direction") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "doc_number", "title" => "Документ", "class" => ($sort_by == "doc_number") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "person_id", "title" => "Персона", "class" => ($sort_by == "person_id") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "", "title" => "Фотография", "class" => ""];
        $table_header[] = ["type" => "text", "field" => "organization", "title" => "Организация", "class" => ""];
        $table_header[] = ["type" => "link", "field" => "control_list", "title" => "Наличие в К.С.", "class" => ($sort_by == "control_list") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "fingerprint_verification", "title" => "Верификация (пальцы)", "class" => ($sort_by == "fingerprint_verification") ? "selected_td" : ""];
        $table_header[] = ["type" => "link", "field" => "face_verification", "title" => "Верификация (лицо)", "class" => ($sort_by == "face_verification") ? "selected_td" : ""];
         $table_header[] = ["type" => "link", "field" => "pass_allowed", "title" => "Проход произведен", "class" => ($sort_by == "pass_allowed") ? "selected_td" : ""];

        if($sort_dir == "ASC"){
            $dir_next = "DESC";
        } else {
            $dir_next = "ASC";
        }
        $header_string = "";
        foreach ($table_header as &$table_header_item){
            if($table_header_item['type']=="link"){
                $table_header_item["href"] = $curr_url . "&sort_by=" . $table_header_item['field'] . "&dir=".$dir_next;
                if(isset($filters['person'])){
                    $table_header_item["href"].="&person=".$filters['person'];
                }
                if(isset($filters['organization'])){
                    $table_header_item["href"].="&organization=".$filters['organization'];
                }
                if(isset($filters['doc_number'])){
                    $table_header_item["href"].="&doc_number=".$filters['doc_number'];
                }
                $header_string.="<td class=" . $table_header_item["class"] . ">
                <a class='btn-link text-semibold' href=" . $table_header_item["href"] . ">" . $table_header_item["title"] . "</a></td>";
            } else {
                $header_string.="<td>" . $table_header_item["title"] . "</td>";
            }
        }
        $this->set("header_string", $header_string);
        $this->set("filter_organization", $filter_organization);
        $this->set("filter_person", $filter_person);
        $this->set("filter_doc_number", $filter_doc_number);
    }

    public function pass_data()
    {
        $gate_id = $this->request->query('gate_id') ?? null;
        $pass_id = $this->request->param('pass_id') ?? null;
        $pass = $this->AutogateCom->getPassEvents($pass_id);
        $this->set("pass_id", $pass_id);
        $this->set("gate_id", $gate_id);
        foreach ($pass as &$item) {
            $item['Event']['object_image'] = null;
            if ($item['Event']['object'] != null) {
                $item['Event']['object_image'] = $this->PersonCom->generateImageFromBinary($item['Event']['object']);
            }
        }
        $pass_events = $this->PersonCom->pass_events;
        $this->set('pass_events', $pass_events);
        $this->set("pass", $pass);
    }

    public function initTestPass()
    {
        $test_data = [
            "user_name" => "Иванов Николай Петрович",
            "division" => "Отдел информатики",
            "subdivision" => "Отдел программирования",
            "organization" => "СовТехСнаб",
            "document" => "Паспорт №7373 848494",
            "position" => "Программист",
            "photo_from_db" => file_get_contents(WWW_ROOT . "photo_from_db"),
            "live_photo" => file_get_contents(WWW_ROOT . "photo_from_db"),
        ];
        $gate_id = 6;
        $pass_id = 13;
        $this->AutogateCom->setCurrentPassID($gate_id, $pass_id);


        $this->AutogateCom->clearEventData($gate_id, $pass_id);
        foreach ($test_data as $key => $item) {
            $status = "true";
            $data_type = "text";
            if ($key == "photo_from_db" or $key == "live_photo") {
                $data_type = "object";
            }
            $this->AutogateCom->addEventData($gate_id, $pass_id, $key, $item, $data_type, $status);
        }
        echo "TEST Successfully finished!";
        exit;
    }

    public function testPassDone()
    {
        $gate_id = 6;
        $pass_id = 13;
        $this->AutogateCom->setPassDone($gate_id, $pass_id);
        echo "TEST Pass Done!";
        exit;
    }

    public function resetTestPass()
    {
        $gate_id = 6;
        $pass_id = 12;
        $this->AutogateCom->clearEventData($gate_id, $pass_id);
        $this->AutogateCom->setCurrentPassID($gate_id, 0);
        echo "reset test!";
        exit;
    }

    public function forceAccess()
    {
        $gate_id = $this->request->query('gate_id') ?? null;
        $current_pass_id = $this->AutogateCom->getCurrentPassID($gate_id);
        $action = $this->request->query('action') ?? null;

        if ($action <= 0 && $gate_id == null) {
            $result = [
                "error" => "Undefined gate_id",
            ];
            response_ajax($result, "error");
            exit;
        }
        if ($action !== "access_granted" && $action !== "access_denied") {
            $result = [
                "error" => "Undefined force action",
            ];
            response_ajax($result, "error");
            exit;
        }
        // add force status
        $this->AutogateCom->addEventData($gate_id, $current_pass_id, "force_access", $action, "text", "true");
        $result = [
            "force_access" => $action,
        ];
        response_ajax($result, "success");
        exit;
    }

    public function testGDImage()
    {
        $gate_id = 6;
        $pass_id = "61605098816073186";
        $result = $this->PersonCom->getPersonPhotosByPassId($gate_id, $pass_id);
        pr($result);
        exit;
    }

    public function test()
    {
        echo $this->AutogateCom->getCurrentPassID(6);
    }


}