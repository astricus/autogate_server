<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class OperatorController extends AppController
{
    public $uses = array(
        'Autogate',
        'Person',
        'Operator',
        'Token',
        'Auth'
    );

    public $components = array(
        'Cookie',
        'Flash',
        'OperatorCom',
        'PersonCom',
        'Session',
        'Validator',
    );

    public $layout = "default";

    private $MIN_PASS_LENGTH = 8;

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    // ПОЛЬЗОВАТЕЛЬСКИЕ ДАННЫЕ
    public function user_data()
    {
        $this->UserCom->auth_requires();
        $user_id = $this->UserCom->getUserId();
        if ($user_id == null) {
            response_api(["error" => UserComponent::AUTH_REQUIRES_MESSAGE], "error");
            exit;
        }

        $user_data = $this->UserCom->getUserData($user_id);

        $subscription = $this->UserCom->getUserSubscriptions($user_id);

        $compared_product = $this->UserCom->getCompareProductList($user_id);

        $offer_count = $this->UserCom->getCartOffersCount($user_id);

        $favourite = [];
        if ($this->UserCom->getFavouriteList($user_id)) {
            foreach ($this->UserCom->getFavouriteList($user_id) as $v) {
                $favourite[] = $v['content_id'];
            }
        }


        $user_avatar = null;
        if (!empty($user_data['Orderer']['avatar'])) {
            $user_avatar = $this->UserCom->getUserImagesDir($this->UserCom->getUserId(), "relative") . DS . $user_data['Orderer']['avatar'];
        }

        if ($user_data['Orderer']['middlename'] !== "" or $user_data['Orderer']['middlename'] !== "null") {
            $middlename = $user_data['Orderer']['middlename'];
        } else {
            $middlename = null;
        }
        if ($user_data['Orderer']['lastname'] !== "" or $user_data['Orderer']['lastname'] !== "null") {
            $lastname = $user_data['Orderer']['lastname'];
        } else {
            $lastname = null;
        }
        $arr = [
            'firstname' => $user_data['Orderer']['firstname'],
            'lastname' => $lastname,
            'middlename' => $middlename,
            'sex' => $user_data['Orderer']['sex'],
            'birthday' => $user_data['Orderer']['birthday'],
            'phone_number' => prepare_phone($user_data['Orderer']['phone_number']),
            'email' => $user_data['Orderer']['email'],
            'avatar' => $user_avatar,
            'subscriptions' => $subscription,
            'favourite_products_ids' => $favourite,
            'compare_products_ids' => $compared_product,
            'offer_count' => $offer_count,
        ];

        response_api($arr, "success");
        exit();
    }

    public function show_pass()
    {
        echo get_hash(Configure::read('USER_AUTH_SALT'), "pass_283472847234");
        exit;
    }

    public function set_pass(){
        //$this->layout = "admin";
        if ($this->request->is('post')) {
            $password1 = $this->request->data('password1');
            $password2 = $this->request->data('password2');
            if($password1!=$password2){
                $auth_error_text = "PASS1_NOT_EQ_PASS2";
                $this->redirect(array('controller' => 'operator', 'action' => 'set_pass', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                exit;
            }
            if(!$this->OperatorCom->validatePassword($password1)){
                $auth_error_text = "PASS1_NOT_VALID";
                $this->redirect(array('controller' => 'operator', 'action' => 'set_pass', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                exit;
            }
            $op_id = $this->Session->read('operator_id');
            $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password1);
            $this->Operator->id = $op_id;
            $this->Operator->save(['password' => $hashed_pass]);
            $this->Session->write('needs_pass', null);
            $this->Session->delete('needs_pass');
            $this->redirect(site_url());
        }
        $auth_error_text = $this->request->query('auth_error_text');
        $auth_error = $this->request->query('auth_error');
        if (!empty($auth_error_text)) {
            $this->set("auth_error_text", $auth_error_text);
        }
        if (!empty($auth_error)) {
            $this->set("auth_error", $auth_error);
        }
    }

    public function login()
    {
        $this->layout = "admin";
        //die(get_hash(Configure::read('USER_AUTH_SALT'), $this->request->query('p')));
        if ($this->request->is('post')) {

            $login = $this->request->data('login');
            $password = $this->request->data('password');

            $check_user_exists = $this->Operator->find('first', array('conditions' => array('login' => $login)));

            if ($check_user_exists == null) {
                $auth_error_text = "OPERATOR_NOT_FOUND";
                $this->redirect(array('controller' => 'operator', 'action' => 'login', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                exit;
            }

            if (!empty($login)) {
                $check_user = $this->Operator->find('first', array('conditions' => array('login' => $login)));
                $pass = $check_user_exists['Operator']['password'];
                if ($pass == "") {
                    $this->Session->write('needs_pass', true);
                } else if ($pass != "") {
                    $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password);
                    if ($hashed_pass != $pass) {
                        $auth_error_text = "WRONG_LOGIN_OR_PASSWORD";
                        $this->redirect(array('controller' => 'operator', 'action' => 'login', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                        exit;
                    }
                }

                //удачная авторизация
                // проверка не заблокирован ли оператор
                if ($check_user['Operator']['enabled'] == false) {
                    $auth_error_text = "ADMIN_ACCESS_DENIED";
                    $this->redirect(array('controller' => 'operator', 'action' => 'login', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                    exit;
                }

                $role = $check_user['Operator']['role'];

                $this->Session->write('Operator', $login);
                $op_data = $this->Operator->find('first', array('conditions' => array('login' => $login)));
                $op_id = $op_data['Operator']['id'];

                $this->Session->write('operator_id', $op_id);
                $user_data_arr = [];
                $user_data_arr['id'] = $op_data['Operator']['id'];
                $user_data_arr['firstname'] = $op_data['Operator']['firstname'];
                $user_data_arr['lastname'] = $op_data['Operator']['lastname'];
                $user_data_arr['login'] = $op_data['Operator']['login'];

                //$this->loadModel('Auth');
                $auth_data = array('operator_id' => $op_id, 'remote_ip' => get_ip(), 'result_auth' => 'true', 'login_time' => now_date());
                $this->Auth->create();
                $this->Auth->save($auth_data);
                $op_name = prepare_fio($op_data['Operator']['lastname'], $op_data['Operator']['firstname'], $op_data['Operator']['middlename']);
                $this->Flash->set("Успешная авторизация, $op_name!", array("params" => array("class" => "alert alert-success")));
                $this->redirect(site_url());
                exit;


            } else {
                $auth_error_text = "WRONG_LOGIN_OR_PASSWORD";
                $this->redirect(array('controller' => 'operator', 'action' => 'login', '?' => array('auth_error' => 'true', 'auth_error_text' => $auth_error_text)));
                exit;
            }
        }
        $auth_error_text = $this->request->query('auth_error_text');
        $auth_error = $this->request->query('auth_error');
        if (!empty($auth_error_text)) {
            $this->set("auth_error_text", $auth_error_text);
        }
        if (!empty($auth_error)) {
            $this->set("auth_error", $auth_error);
        }
    }

    public function logout()
    {
        $this->Session->write('operator_id', null);
        $this->redirect(array('controller' => 'Operator', 'action' => 'index'));
        exit;
    }

    public function operator_list()
    {
        $operators = $this->OperatorCom->getOperatorList();
        $this->set("operators", $operators);
    }

    public function view()
    {
        $id = $this->request->param('id') ?? null;
        $operator = $this->OperatorCom->getOperatorById($person_id);
        $this->set("operator", $operator);
    }

    public function add()
    {
        $modelName = "Operator";
        $this->Operator = ClassRegistry::init($modelName);

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $operator_id = $this->OperatorCom->add_operator($data);
            if (count($this->OperatorCom->error) == 0) {
                $result = ["message" => "Оператор успешно создан!</b>"];
                response_ajax($result, "success");
                exit;
            } else {
                $error_text = "";
                foreach ($this->OperatorCom->error as $error) {
                    $error_text .= $error;
                }
                $result = ["message" => $error_text, "status" => "error"];
                response_ajax($result, "error");
                exit;
            }
        }
    }

    public function set_status()
    {
        $id = $this->request->param('id') ?? null;
        $status = $this->request->query('status');
        if (intval($id) > 0) {
            $this->OperatorCom->updateStatus($id, $status);
        }
        $this->redirect('/operators');
        exit;
    }

    public function base64byImage($url)
    {
        //return WWW_ROOT . DS . "images" . DS . "chip_photo.jpg";
        $image = file_get_contents($url);
        if ($image !== false) {
            return 'data:image/jpg;base64,' . base64_encode($image);

        }
    }

    public function addPersonPhoto()
    {
        $person_id = $this->request->query('person_id');
        $file = $this->base64byImage(WWW_ROOT . DS . "images" . DS . "chip_photo.jpg");
        //$photo = $this->request->query('photo');
        if (intval($person_id) > 0) {
            $this->PersonCom->addPersonPhoto($person_id, $file);
        }
        echo "ok";
        //$this->redirect(array('controller' => 'person', 'action' => 'persons'));
        exit;
    }

    public function edit()
    {
        $id = $this->request->param("id") ?? null;
        if ($this->request->is('post')) {
            $id = $this->request->data('operator_id') ?? null;
            $firstname = $this->request->data('firstname') ?? null;
            $lastname = $this->request->data('lastname') ?? null;
            $middlename = $this->request->data('middlename') ?? null;
            $login = $this->request->data('login') ?? null;
            $role = $this->request->data('role') ?? null;

            $result = $this->OperatorCom->editOperator($id, $lastname, $firstname, $middlename, $login, $role);
            if ($result) {
                $result = ["message" => "Оператор успешно изменен!"];
                response_ajax($result, "success");
            } else {
                $result = ["message" => $this->OperatorCom->error, "status" => "error"];
                response_ajax($result, "error");
            }
            exit;
        } else {
            if ($id == null) {
                die("undefined identifier");
            }
            $operator = $this->OperatorCom->getOperatorById($id);
            $this->set("operator", $operator['Operator']);
        }
    }

    // Смена пароля
    public function changePassword()
    {
        if ($this->UserCom->is_auth()) {
            if (count($this->params->data) > 0) {
                // Параметры пришли POST данными или строкой json
                if (!empty($this->request->data('current_password')) and !empty($this->request->data('new_password'))) {
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $this->request->data('current_password'));
                    $new_password = trim($this->request->data('new_password'));
                } else {
                    $data = json_decode(array_key_first($this->params->data));
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $data->current_password);
                    $new_password = trim($data->new_password);
                }

                if ($new_password == '' or mb_strlen($new_password) < $this->MIN_PASS_LENGTH) {
                    response_api(['error' => 'Пароль меньше 8 символов'], 'error');
                    exit();
                }

                $user_data = $this->UserCom->user_data();
                $user_id = $user_data['Orderer']['id'];

                if ($hash_current_pass == $user_data['Orderer']['password']) {
                    $res = $this->UserCom->changePassword($user_id, $new_password);
                    if ($res) {
                        response_api(['success' => true], 'success');
                    } else {
                        response_api(['error' => 'Новый пароль не сохранен'], 'error');
                    }
                } else {
                    response_api(['error' => 'Пароли не совпали'], 'error');
                }
            } else {
                response_api(['error' => 'Данные не пришли'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit();
    }

    // Отправить письмо с новым паролем
    public function resetPasswordSendNew()
    {
        if (!empty($this->request->data('login'))) {
            $email = trim($this->request->data('login'));
            $user_data = $this->UserCom->getUserDataByEmail($email);

            if ($user_data) {
                //$hash = get_hash(Configure::read('USER_AUTH_SALT'), $this->UserCom->generateRandomPass(10));
                $new_password = $this->UserCom->generateRandomPass(10);
                $hash_new_password = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);

                $text_message = 'Временный пароль от личного кабинета: ' . $new_password;//.Router::url('/', true).'route?token='.$hash;
                $recipiet_name = $user_data['firstname'] . ' ' . $user_data['lastname'];

                $email_send = $this->EmailCom->sendBaseEmail($recipiet_name, $email, 'Новый пароль - terpikka.ru', $text_message);
                if ($email_send) {
                    $this->Orderer->id = $user_data['id'];
                    $this->Orderer->save(['password' => $hash_new_password]);
                    response_api(['success' => true], 'success');
                } else {
                    response_api(['error' => 'Письмо с новым паролем не ушло'], 'error');
                }
            } else {
                response_api(['error' => 'Пользователь с таким логином не найден'], 'error');
            }
        } else {
            response_api(['error' => 'Логин не передан'], 'error');
        }

        exit();
    }

    // Сменить пароль
    public function reset_password()
    {
        $operator_id = $this->request->param("id") ?? null;
        if ($this->request->is('post')) {
            $pass = $this->OperatorCom->resetPassword($operator_id);
            if (count($this->OperatorCom->error) == 0) {
                $result = ["message" => "Пароль оператора сброшен"];
                response_ajax($result, "success");
                exit;
            } else {
                $error_text = "";
                foreach ($this->OperatorCom->error as $error) {
                    $error_text .= $error;
                }
                $result = ["message" => "Не удалось сбросить пароль: " . $error_text, "status" => "error"];
                response_ajax($result, "error");
                exit;
            }
        }
    }

}