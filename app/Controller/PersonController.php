<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class PersonController extends AppController
{
    public $uses = array(
        'Biometric'
    );

    public $per_page = 5;

    private $valid_image_types = [
        'image/jpeg',
    ];

    private $min_image_size = 5000;

    private $face_image_width = 800;

    private $face_image_height = 1200;

    public $error;

    public $errors = [
        "undefined_image_type" => "Неопределенный или некорректный формат изображения, поддерживается только jpg",
        "too_small_image" => "Слишком малый вес изображения",
        "person_id_cannot_be_null" => "Идентификатор персоны не может быт пустым",
        "person_not_found" => "Персона не найдена",
    ];

    public $components = array(
        'Session',
        'OperatorCom',
        'AutogateCom',
        'PersonCom',
        'Flash',
        'ControlRule'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function persons()
    {
        $filter = $this->request->query('filter') ?? null;
        $organization = $this->request->query('organization') ?? null;
        $person_status = $this->request->query('person_status') ?? null;
        if ($this->Session->read("set_status_view") == "on") {
            $person_status = "on";
        }
        if (!empty($organization)) {
            $organization = urldecode($organization);
            if ($organization == "Все организации") {
                $organization = null;
            }
        }
        $page = $this->request->query['page'] ?? 1;
        $persons = $this->PersonCom->getPersonList($page, $this->per_page, $filter, $organization, $person_status);
        $persons_count = $this->PersonCom->getPersonsCount($filter, $organization, $person_status);
        $pages = ceil($persons_count / $this->per_page);
        $this->set("doc_type", $this->PersonCom->doc_type);
        $this->set("persons", $persons);
        $this->set("person_count", $persons_count);
        $this->set("per_page", $this->per_page);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $this->set("filter", $filter);
        $this->set("lastname_indexes", $this->PersonCom->getPersonLastnameIndexes($organization, $person_status));
        $this->set("organizations", $this->PersonCom->getPersonOrganizations());
        $this->set("organization", $organization);
        $this->set("person_status", $person_status);
    }

    public function person_view()
    {
        $person_id = $this->request->param('person_id') ?? null;
        if ($person_id == 0) {
            die($this->errors["person_id_cannot_be_null"]);
        }
        $person = $this->PersonCom->getPersonDataById($person_id);
        if ($person == null) {
            header("HTTP/1.0 404 Not Found");
            die($this->errors["person_not_found"]);
        }

        $this->set("verification_type", $this->PersonCom->verification_type);
        $this->set("doc_type", $this->PersonCom->doc_type);
        $this->set("person_content", $person);
        $this->set("person_image_link", $person['person_image_link']);
        $this->set("list_names", $this->ControlRule->getListNames());

        $accept_face_verification = Configure::read('ACCEPT_FACE_VERIFICATION');

        $check_face_verify_document = $this->PersonCom->checkAcceptActivateFaceVerifyDocument($person_id);
        $can_add_verify_doc = false;
        if ($accept_face_verification && $check_face_verify_document) {
            $can_add_verify_doc = true;
        }
        $this->set("can_add_verify_doc", $can_add_verify_doc);
        $person_image_params = $this->PersonCom->getImageParamFromBiometric($person_id);
        $this->set("person_image_params", $person_image_params);

    }

    public function set_status_view()
    {

        if ($this->request->is('post')) {
            $status = $this->request->query('status') ?? null;
            $this->Session->write("set_status_view", $status);
            response_ajax($status, "success");
            exit;
        }
    }

    public function add_face_verification_document()
    {
        $person_id = $this->request->param('person_id') ?? null;
        $result = $this->PersonCom->addFaceVerificationDocument($person_id);
        if ($result) {
            $this->Flash->set("Добавлена идентификация по фото", array("params" => array("class" => "alert alert-success")));
        } else {
            if (is_array($this->PersonCom->error)) {
                $err_str = "";
                foreach ($this->PersonCom->error as $err) {
                    $err_str .= " " . $err;
                }
            }
            $this->Flash->set($err_str, array("params" => array("class" => "alert alert-danger")));
        }
        $this->redirect('/person/view/' . $person_id);
        exit;

    }

    public function add_person()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $this->Session->write("add_person_form", $data);
            $person_id = $this->PersonCom->addPerson($data);
            if (isset($_FILES) && count($_FILES['image']) > 0) {
                if (!empty($_FILES['image']['tmp_name'])) {
                    if ($this->validateImage($_FILES['image']['size'], $_FILES['image']['type'])) {
                        //$this->redirect(array('controller' => 'person', 'action' => 'add_person'));
                        resize_image($_FILES['image']['tmp_name'], $this->face_image_width, $this->face_image_height, false, $_FILES['image']['tmp_name']);
                        $format_face = $this->getBinaryImage($_FILES['image']['tmp_name']);
                        $this->saveFaceToDb($person_id, $format_face);
                    } else {
                        $errors = "";
                        foreach ($this->error as $err) {
                            $errors .= " " . $err;
                        }
                        $this->Flash->set($errors, array("params" => array("class" => "alert alert-danger")));

                        $this->redirect($this->referer());
                        return;

                    }
                }
            }
            if (isset($this->request->data['doc_number']) && isset($this->request->data['doc_type'])) {
                $doc_number = $this->request->data['doc_number'];
                $doc_number = $this->filterDocNumber($doc_number);
                $doc_type = $this->request->data['doc_type'];
                $verify_check = $this->PersonCom->validateVerificationType($this->request->data['verification_type']);
                if ($person_id > 0 && $this->PersonCom->validateDocument($doc_number, $doc_type) && $verify_check) {
                    $this->PersonCom->addPersonDocument($doc_number, $doc_type, $person_id, $this->request->data['verification_type']);
                } else {
                    $this->PersonCom->validateDocument($doc_number, $doc_type);
                }
            }
            $this->Session->delete("add_person_form");
            //$this->redirect('/person/view/' . $person_id);
            $this->redirect('/persons');
            exit;

        } else {
            $data_form = $this->Session->read("add_person_form");
            $form_firstname = $this->request->data("firstname") ?? $data_form["firstname"];
            $form_lastname = $this->request->data("lastname") ?? $data_form["lastname"];
            $form_middlename = $this->request->data("middlename") ?? $data_form["middlename"];
            $this->set("form_firstname", $form_firstname);
            $this->set("form_lastname", $form_lastname);
            $this->set("form_middlename", $form_middlename);
            $form_doc_number = $this->request->data("doc_number") ?? $data_form["doc_number"];
            $this->set("form_doc_number", $form_doc_number);
            $form_organization = $this->request->data("organization") ?? $data_form["organization"];
            $this->set("form_organization", $form_organization);
            $form_division = $this->request->data("division") ?? $data_form["division"];
            $this->set("form_division", $form_division);
            $form_subdivision = $this->request->data("subdivision") ?? $data_form["subdivision"];
            $this->set("form_subdivision", $form_subdivision);
            $form_position = $this->request->data("position") ?? $data_form["position"];
            $this->set("form_position", $form_position);
            $this->set("doc_types", $this->PersonCom->doc_type);
        }
    }

    public function edit_person()
    {
        $person_id = $this->request->param("person_id") ?? null;
        if ($this->request->is('post')) {
            $person_id = $this->request->data('person_id') ?? null;
            $firstname = $this->request->data('firstname') ?? null;
            $lastname = $this->request->data('lastname') ?? null;
            $middlename = $this->request->data('middlename') ?? null;
            $organization = $this->request->data('organization') ?? null;
            $division = $this->request->data('division') ?? null;
            $subdivision = $this->request->data('subdivision') ?? null;
            $position = $this->request->data('position') ?? null;

            if (isset($_FILES) && count($_FILES['image']) > 0) {
                if (!empty($_FILES['image']['tmp_name'])) {
                    if ($this->validateImage($_FILES['image']['size'], $_FILES['image']['type'])) {
                        resize_image($_FILES['image']['tmp_name'], $this->face_image_width, $this->face_image_height, false, $_FILES['image']['tmp_name']);
                        $format_face = $this->getBinaryImage($_FILES['image']['tmp_name']);
                        $this->saveFaceToDb($person_id, $format_face);
                    } else {
                        $errors = "";
                        foreach ($this->error as $err) {
                            $errors .= " " . $err;
                        }
                        $this->Flash->set($errors, array("params" => array("class" => "alert alert-danger")));
                        $this->redirect('/person/edit/' . $person_id);
                        return;
                    }
                }
            }

            $result = $this->PersonCom->editPerson($person_id, $lastname, $firstname, $middlename, $organization, $division, $subdivision, $position);
            if ($result) {
                $this->Flash->set("Персона успешно изменена!", array("params" => array("class" => "alert alert-success")));
            } else {
                if (is_array($this->PersonCom->error)) {
                    $err_str = "";
                    foreach ($this->PersonCom->error as $err) {
                        $err_str .= " " . $err;
                    }
                }
                $this->Flash->set($err_str, array("params" => array("class" => "alert alert-danger")));
            }
            $this->redirect('/person/edit/' . $person_id);
        } else {
            if ($person_id == null) {
                die("undefined person!");
            }

            $person = $this->PersonCom->getPersonData($person_id);
            $this->set("person", $person['Person']);
            $image_id = $this->PersonCom->getFaceIdFromBiometric($person_id);
            $this->set("person_image", "/image/" . $image_id);
        }
    }

    /**
     * @param $string
     * @return string|string[]
     */
    private function filterDocNumber($string)
    {
        return str_replace(" ", "", $string);
    }

    public function delete_document()
    {
        $doc_id = $this->request->param("id") ?? null;
        $person_id = $this->request->query("person_id") ?? null;
        $delete_check = $this->PersonCom->deleteDocument($doc_id);
        if ($delete_check) {
            $this->Flash->set("Документ успешно удален!", array("params" => array("class" => "alert alert-success")));
        } else {
            $this->Flash->set("Документ не удалось удалить, вероятно, по причине наличия проходов", array("params" => array("class" => "alert alert-danger")));
        }
        $this->redirect('/person/view/' . $person_id);
        exit;
    }

    public function add_document()
    {
        $person_id = $this->request->param("person_id") ?? null;
        if ($this->request->is('post')) {
            if (isset($this->request->data['doc_number']) && isset($this->request->data['doc_type'])) {
                $doc_number = $this->request->data['doc_number'];
                $doc_type = $this->request->data['doc_type'];
                $doc_number = $this->filterDocNumber($doc_number);
                $verify_check = $this->PersonCom->validateVerificationType($this->request->data['verification_type']);
                if ($person_id > 0 && $this->PersonCom->validateDocument($doc_number, $doc_type) && $verify_check) {

                    if (!$this->PersonCom->checkDocumentIsFree($doc_number, $doc_type)) {
                        $error_text = "Возникла ошибка в процессе создания документа: " . $this->PersonCom->errors['document_is_busy'] . " " . $this->PersonCom->person_has_document;
                        $this->Flash->set($error_text, array("params" => array("class" => "alert alert-danger"))
                        );
                        $this->redirect('/person/view/' . $person_id);
                        exit;
                    }
                    $result = $this->PersonCom->addPersonDocument($doc_number, $doc_type, $person_id, $this->request->data['verification_type']);

                    if ($result) {
                        $this->Flash->set("Документ успешно создан!", array("params" => array("class" => "alert alert-success")));
                    } else {
                        $this->Flash->set("Возникла ошибка в процессе создания документа", array("params" => array("class" => "alert alert-danger")));
                    }

                } else {
                    $this->Flash->set("Возникла ошибка в процессе создания документа", array("params" => array("class" => "alert alert-danger")));
                }
            } else {
                $this->Flash->set("Возникла ошибка в процессе создания документа", array("params" => array("class" => "alert alert-danger")));
            }

            $this->redirect('/person/view/' . $person_id);
            exit;

        } else {
            if ($person_id == null) {
                die("undefined person!");
            }
            $person = $this->PersonCom->getPersonData($person_id);
            $this->set("person_name", prepare_fio($person['Person']['lastname'], $person['Person']['firstname'], $person['Person']['middlename']));
            $this->set("doc_types", $this->PersonCom->doc_type);
            $this->set("person_id", $person_id);
        }

    }

    /**
     * @param $person_id
     * @param $file_data
     * @return mixed|null
     */
    public function saveFaceToDb($person_id, $file_data)
    {
        $check_face = $this->PersonCom->findPersonFace($person_id);
        if ($check_face != null) {
            $this->PersonCom->deletePersonFace($check_face);
        }

        $face_data = [
            'person_id' => $person_id,
            'modality' => 'Face',
            'fingerprint_index' => null,
            'enrollment_id' => 0,
            'object' => $file_data,
            'deleted' => false
        ];
        $this->Biometric->create();
        $this->Biometric->save($face_data);
        return $this->Biometric->id;
    }

    /**
     * @param $size
     * @param $type
     * @return bool
     */
    private function validateImage($size, $type)
    {
        if (!in_array($type, $this->valid_image_types)) {
            $this->error[] = $this->errors["undefined_image_type"];
            return false;
        }
        if ($size <= $this->min_image_size) {
            $this->error[] = $this->errors["too_small_image"];
            return false;
        }
        return true;
    }

    /**
     * @param $file_image
     * @return false|string
     */
    public function getBinaryImage($file_image)
    {
        $info = getimagesize($file_image);
        $type = $info[2];
        switch ($type) {
            case 1:
                $img = imageCreateFromGif($file_image);
                imageSaveAlpha($img, true);
                break;
            case 2:
                $img = imageCreateFromJpeg($file_image);
                break;
            case 3:
                $img = imageCreateFromPng($file_image);
                imageSaveAlpha($img, true);
                break;
        }
        $exif = exif_read_data($file_image);
        if (isset($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 3:
                    $img = imagerotate($img, 180, 0);
                    break;
                case 6:
                    $img = imagerotate($img, -90, 0);
                    break;
                case 8:
                    $img = imagerotate($img, 90, 0);
                    break;
            }
        }
        ob_start();
        switch ($type) {
            case 1:
                imagegif($img);
                break;
            case 2:
                imagejpeg($img);
                break;
            case 3:
                imagepng($img);
                break;
        }

        $stringdata = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
        return $stringdata;
    }

    /**
     * @param $file_image
     * @return false|resource
     */
    private function convertImage($file_image)
    {
        $info = getimagesize($file_image);
        $width = $info[0];
        $height = $info[1];
        $type = $info[2];
        switch ($type) {
            case 1:
                $img = imageCreateFromGif($file_image);
                imageSaveAlpha($img, true);
                break;
            case 2:
                $img = imageCreateFromJpeg($file_image);
                break;
            case 3:
                $img = imageCreateFromPng($file_image);
                imageSaveAlpha($img, true);
                break;
        }
        $w = $this->face_image_width;
        $h = 0;
        if (empty($w)) {
            $w = ceil($h / ($height / $width));
        }
        if (empty($h)) {
            $h = ceil($w / ($width / $height));
        }
        $tmp = imageCreateTrueColor($w, $h);
        if ($type == 1 || $type == 3) {
            imagealphablending($tmp, true);
            imageSaveAlpha($tmp, true);
            $transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127);
            imagefill($tmp, 0, 0, $transparent);
            imagecolortransparent($tmp, $transparent);
        }
        $tw = ceil($h / ($height / $width));
        $th = ceil($w / ($width / $height));
        if ($tw < $w) {
            imageCopyResampled($tmp, $img, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $width, $height);
        } else {
            imageCopyResampled($tmp, $img, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $width, $height);
        }
        $img = $tmp;
        ob_start();
        switch ($type) {
            case 1:
                imagegif($img);
                break;
            case 2:
                imagejpeg($img);
                break;
            case 3:
                imagepng($img);
                break;
        }
        $stringdata = ob_get_contents(); // read from buffer
        ob_end_clean(); // delete buffer
        return $stringdata;
    }

    public function update_person_status()
    {

        $person_id = $this->request->param('person_id') ?? null;
        $person_status = $this->request->query('status');
        if (intval($person_id) > 0) {
            $this->PersonCom->updatePersonStatus($person_id, $person_status);
        }
        $this->redirect('/persons');
        exit;
    }

    public function base64byImage($url)
    {
        //return WWW_ROOT . DS . "images" . DS . "chip_photo.jpg";
        $image = file_get_contents($url);
        if ($image !== false) {
            return 'data:image/jpg;base64,' . base64_encode($image);

        }
    }

    public function addPersonPhoto()
    {
        $person_id = $this->request->query('person_id');
        $file = $this->base64byImage(WWW_ROOT . DS . "images" . DS . "chip_photo.jpg");
        //$photo = $this->request->query('photo');
        if (intval($person_id) > 0) {
            $this->PersonCom->addPersonPhoto($person_id, $file);
        }
        echo "ok";
        //$this->redirect(array('controller' => 'person', 'action' => 'persons'));
        exit;
    }

    private function validateLastname($string)
    {
        if (mb_strlen($string) <= 1) {
            return false;
        }
    }

    public function edit_document()
    {
        $id = $this->request->param("document_id") ?? null;
        $person_id = $this->request->param("person_id") ?? null;
        if ($this->request->is('post')) {
            $doc_type = $this->request->data('doc_type') ?? null;
            $doc_number = $this->request->data('doc_number') ?? null;
            $doc_number = $this->filterDocNumber($doc_number);
            $verification = $this->request->data('verification') ?? null;
            $result = $this->PersonCom->editDocument($id, $doc_type, $doc_number, $verification);
            if ($result) {
                $this->Flash->set("Документ успешно изменен!", array("params" => array("class" => "alert alert-success")));
            } else {
                $errors = explode(" ", $this->PersonCom->error);
                $this->Flash->set($errors, array("params" => array("class" => "alert alert-danger")));
            }
            $this->redirect('/person/' . $person_id . "/edit_document/" . $id);
        } else {
            if ($id == null) {
                die("undefined document!");
            }
            $this->set("doc_types", $this->PersonCom->doc_type);
            $doc = $this->PersonCom->getDocumentById($id);
            $this->set("person_id", $person_id);
            $this->set("document", $doc['Document']);
            $this->set("person_name", $this->PersonCom->getPersonNameById($person_id));
        }
    }

    public function reset_document()
    {
        $document_id = $this->request->param("id") ?? null;
        $reset_result = $this->PersonCom->resetDocument($document_id);
        $this->Flash->set("Информация о документ сброшена", array("params" => array("class" => "alert alert-success")));
        $this->redirect($this->referer());
        exit;
    }

    public function delete_person()
    {
        $person_id = $this->request->param("person_id") ?? null;
        $delete_result = $this->PersonCom->deletePerson($person_id);
        if (!$delete_result) {
            $this->Flash->set("Персону не удалось удалить, вероятно, по причине наличия проходов и добавленных документов", array("params" => array("class" => "alert alert-danger")));
        } else {
            $this->Flash->set("Персона успешно удалена!", array("params" => array("class" => "alert alert-success")));
        }
        $this->redirect("/persons");
        exit;
    }

    public function accept_document()
    {
        $pass_id = $this->request->param("pass_id") ?? null;
        $accept_result = $this->PersonCom->acceptDocument($pass_id);
        if ($accept_result) {
            response_ajax("success", "success");
        } else {
            response_ajax("error", "error");
        }
        exit;
    }

    // включить или отключить документ персоны
    public function set_document_off()
    {
        $doc_id = $this->request->param("id") ?? null;
        if ($doc_id == null) {
            die("undefined document");
        }
        $this->PersonCom->set_document($doc_id, false);
        return $this->redirect($this->referer());
    }

    public function set_document_on()
    {
        $doc_id = $this->request->param("id") ?? null;
        if ($doc_id == null) {
            die("undefined document");
        }
        $this->PersonCom->set_document($doc_id, true);
        return $this->redirect($this->referer());
    }

    public function load_doubtful_document()
    {
        $pass_id = $this->request->param("pass_id") ?? null;
        $images = $this->PersonCom->getDocumentImagesFromEvents($pass_id);
        if ($images) {
            $data = ["documents" => $images];
        } else {
            $data = [];
        }
        response_ajax($data, "success");
        exit;
    }

    public function personInside()
    {
        $person_inside = $this->PersonCom->getPersonInside();
        $this->set("person_inside", $person_inside);
    }

    public function get_ajax_list()
    {
        $person_string = $this->request->query("string") ?? null;
        if (mb_strlen($person_string) <= 3) {
            response_ajax([], "success");
            exit;
        }
        $count = 20;
        $persons = $this->PersonCom->getPersonByString($person_string, $count);
        $person_arr = [];
        foreach ($persons as $person) {
            $person = $person['Person'];
            $person_arr[] = ['id' => $person['id'],
                'name' => prepare_fio($person['lastname'], $person['firstname'], $person['middlename']),
                'organization' => $person['organization']
            ];
        }
        response_ajax($person_arr, "success");
        exit;
    }
}