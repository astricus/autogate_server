<?php
/**
 * системные функции
 */

/**
 * Обработка строк
 */
function get_mime_type($file)
{
    $mtype = false;
    if (function_exists('finfo_open')) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mtype = finfo_file($finfo, $file);
        finfo_close($finfo);
    } elseif (function_exists('mime_content_type')) {
        $mtype = mime_content_type($file);
    }
    return $mtype;
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $enc = 'utf-8')
    {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
}

function translit($string)
{
    $replace = array(
        "'" => "",
        "`" => "",
        "а" => "a", "А" => "A",
        "б" => "b", "Б" => "B",
        "в" => "v", "В" => "V",
        "г" => "g", "Г" => "G",
        "д" => "d", "Д" => "D",
        "е" => "e", "Е" => "E",
        "ж" => "zh", "Ж" => "Zh",
        "з" => "z", "З" => "Z",
        "и" => "i", "И" => "I",
        "й" => "y", "Й" => "Y",
        "к" => "k", "К" => "K",
        "л" => "l", "Л" => "L",
        "м" => "m", "М" => "M",
        "н" => "n", "Н" => "N",
        "о" => "o", "О" => "O",
        "п" => "p", "П" => "P",
        "р" => "r", "Р" => "R",
        "с" => "s", "С" => "S",
        "т" => "t", "Т" => "T",
        "у" => "u", "У" => "U",
        "ф" => "f", "Ф" => "F",
        "х" => "h", "Х" => "H",
        "ц" => "c", "Ц" => "C",
        "ч" => "ch", "Ч" => "Ch",
        "ш" => "sh", "Ш" => "Sh",
        "щ" => "sch", "Щ" => "Sch",
        "ъ" => "", "Ъ" => "",
        "ы" => "y", "Ы" => "y",
        "ь" => "", "Ь" => "",
        "э" => "e", "Э" => "E",
        "ю" => "yu", "Ю" => "Yu",
        "я" => "ya", "Я" => "Ya",
        "і" => "i", "І" => "I",
        "ї" => "yi", "Ї" => "Yi",
        "є" => "e", "Є" => "E"
    );
    return $str = iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
}


/**
 * транспортные функции
 */

function response_ajax($array, $status = "success")
{
    $data = array('data' => $array, 'status' => $status);
    echo json_encode($data);
}

/**
 * API response функции
 */
/**
 * @param $array_data
 * @param string $status
 */
function response_api($array_data, $status = "success")
{
    $server_time = date("Y-m-d H:i:s");
    $data = array(
        'data' => $array_data,
        "server_time" => $server_time,
        'status' => $status
    );
    echo json_encode($data);
}

/**
 * функции-анализаторы
 */

function is_mobile()
{
    $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
    $accept = strtolower(getenv('HTTP_ACCEPT'));

    if ((strpos($accept, 'text/vnd.wap.wml') !== false) ||
        (strpos($accept, 'application/vnd.wap.xhtml+xml') !== false)
    ) {
        return 1; // Мобильный браузер обнаружен по HTTP-заголовкам
    }

    if (isset($_SERVER['HTTP_X_WAP_PROFILE']) ||
        isset($_SERVER['HTTP_PROFILE'])
    ) {
        return 2; // Мобильный браузер обнаружен по установкам сервера
    }

    if (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|' .
        'wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|' .
        'lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|' .
        'mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|' .
        'm881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|' .
        'r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|' .
        'i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|' .
        'htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|' .
        'sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|' .
        'p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|' .
        '_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|' .
        's800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|' .
        'd736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |' .
        'sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|' .
        'up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|' .
        'pocket|kindle|mobile|psp|treo)/i', $user_agent)) {
        return 3; // Мобильный браузер обнаружен по сигнатуре User Agent
    }

    if (in_array(substr($user_agent, 0, 4),
        Array("1207", "3gso", "4thp", "501i", "502i", "503i", "504i", "505i", "506i",
            "6310", "6590", "770s", "802s", "a wa", "abac", "acer", "acoo", "acs-",
            "aiko", "airn", "alav", "alca", "alco", "amoi", "anex", "anny", "anyw",
            "aptu", "arch", "argo", "aste", "asus", "attw", "au-m", "audi", "aur ",
            "aus ", "avan", "beck", "bell", "benq", "bilb", "bird", "blac", "blaz",
            "brew", "brvw", "bumb", "bw-n", "bw-u", "c55/", "capi", "ccwa", "cdm-",
            "cell", "chtm", "cldc", "cmd-", "cond", "craw", "dait", "dall", "dang",
            "dbte", "dc-s", "devi", "dica", "dmob", "doco", "dopo", "ds-d", "ds12",
            "el49", "elai", "eml2", "emul", "eric", "erk0", "esl8", "ez40", "ez60",
            "ez70", "ezos", "ezwa", "ezze", "fake", "fetc", "fly-", "fly_", "g-mo",
            "g1 u", "g560", "gene", "gf-5", "go.w", "good", "grad", "grun", "haie",
            "hcit", "hd-m", "hd-p", "hd-t", "hei-", "hiba", "hipt", "hita", "hp i",
            "hpip", "hs-c", "htc ", "htc-", "htc_", "htca", "htcg", "htcp", "htcs",
            "htct", "http", "huaw", "hutc", "i-20", "i-go", "i-ma", "i230", "iac",
            "iac-", "iac/", "ibro", "idea", "ig01", "ikom", "im1k", "inno", "ipaq",
            "iris", "jata", "java", "jbro", "jemu", "jigs", "kddi", "keji", "kgt",
            "kgt/", "klon", "kpt ", "kwc-", "kyoc", "kyok", "leno", "lexi", "lg g",
            "lg-a", "lg-b", "lg-c", "lg-d", "lg-f", "lg-g", "lg-k", "lg-l", "lg-m",
            "lg-o", "lg-p", "lg-s", "lg-t", "lg-u", "lg-w", "lg/k", "lg/l", "lg/u",
            "lg50", "lg54", "lge-", "lge/", "libw", "lynx", "m-cr", "m1-w", "m3ga",
            "m50/", "mate", "maui", "maxo", "mc01", "mc21", "mcca", "medi", "merc",
            "meri", "midp", "mio8", "mioa", "mits", "mmef", "mo01", "mo02", "mobi",
            "mode", "modo", "mot ", "mot-", "moto", "motv", "mozz", "mt50", "mtp1",
            "mtv ", "mwbp", "mywa", "n100", "n101", "n102", "n202", "n203", "n300",
            "n302", "n500", "n502", "n505", "n700", "n701", "n710", "nec-", "nem-",
            "neon", "netf", "newg", "newt", "nok6", "noki", "nzph", "o2 x", "o2-x",
            "o2im", "opti", "opwv", "oran", "owg1", "p800", "palm", "pana", "pand",
            "pant", "pdxg", "pg-1", "pg-2", "pg-3", "pg-6", "pg-8", "pg-c", "pg13",
            "phil", "pire", "play", "pluc", "pn-2", "pock", "port", "pose", "prox",
            "psio", "pt-g", "qa-a", "qc-2", "qc-3", "qc-5", "qc-7", "qc07", "qc12",
            "qc21", "qc32", "qc60", "qci-", "qtek", "qwap", "r380", "r600", "raks",
            "rim9", "rove", "rozo", "s55/", "sage", "sama", "samm", "sams", "sany",
            "sava", "sc01", "sch-", "scoo", "scp-", "sdk/", "se47", "sec-", "sec0",
            "sec1", "semc", "send", "seri", "sgh-", "shar", "sie-", "siem", "sk-0",
            "sl45", "slid", "smal", "smar", "smb3", "smit", "smt5", "soft", "sony",
            "sp01", "sph-", "spv ", "spv-", "sy01", "symb", "t-mo", "t218", "t250",
            "t600", "t610", "t618", "tagt", "talk", "tcl-", "tdg-", "teli", "telm",
            "tim-", "topl", "tosh", "treo", "ts70", "tsm-", "tsm3", "tsm5", "tx-9",
            "up.b", "upg1", "upsi", "utst", "v400", "v750", "veri", "virg", "vite",
            "vk-v", "vk40", "vk50", "vk52", "vk53", "vm40", "voda", "vulc", "vx52",
            "vx53", "vx60", "vx61", "vx70", "vx80", "vx81", "vx83", "vx85", "vx98",
            "w3c ", "w3c-", "wap-", "wapa", "wapi", "wapj", "wapm", "wapp", "wapr",
            "waps", "wapt", "wapu", "wapv", "wapy", "webc", "whit", "wig ", "winc",
            "winw", "wmlb", "wonu", "x700", "xda-", "xda2", "xdag", "yas-", "your",
            "zeto", "zte-"))) {
        return 4; // Мобильный браузер обнаружен по сигнатуре User Agent
    }

    return 0; // Мобильный браузер не обнаружен
}

function get_os()
{

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform = "Unknown OS Platform";

    $os_array = array(
        '/windows nt 10/i' => 'Windows 10',
        '/windows nt 6.3/i' => 'Windows 8.1',
        '/windows nt 6.2/i' => 'Windows 8',
        '/windows nt 6.1/i' => 'Windows 7',
        '/windows nt 6.0/i' => 'Windows Vista',
        '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
        '/windows nt 5.1/i' => 'Windows XP',
        '/windows xp/i' => 'Windows XP',
        '/windows nt 5.0/i' => 'Windows 2000',
        '/windows me/i' => 'Windows ME',
        '/win98/i' => 'Windows 98',
        '/win95/i' => 'Windows 95',
        '/win16/i' => 'Windows 3.11',
        '/macintosh|mac os x/i' => 'Mac OS X',
        '/mac_powerpc/i' => 'Mac OS 9',
        '/linux/i' => 'Linux',
        '/ubuntu/i' => 'Ubuntu',
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile'
    );

    foreach ($os_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $os_platform = $value;
        }

    }

    return $os_platform;

}

function phone_format($number){
    $number = str_replace("+", "", $number);
    $number = str_replace("-", "", $number);
    $number = str_replace("(", "", $number);
    $number = str_replace(")", "", $number);
    $number = str_replace(" ", "", $number);
    $number = str_replace("  ", "", $number);

    if(!preg_match('/^[0-9]{10,11}$/',$number)){
        return $number;
    }
    $number_ar[] = substr($number, 1, 3);
    $number_ar[] = substr($number, 4, 3);
    $number_ar[] = substr($number, 7, 2);
    $number_ar[] = substr($number, 9, 2);
    return "+7 (" . $number_ar[0] . ") " . $number_ar[1] . "-" . $number_ar[2] . "-" . $number_ar[3];
}

function prepare_phone($number){
    $number = str_replace("+", "", $number);
    $number = str_replace("-", "", $number);
    $number = str_replace("(", "", $number);
    $number = str_replace(")", "", $number);
    $number = str_replace(" ", "", $number);
    $number = str_replace("  ", "", $number);
    return $number;
}


function complete_date($date){
    $day = substr($date, 9, 2);
    $month = substr($date, 6, 2);
    $year = substr($date, 0, 4);
    $time = explode("", $date);
    $time = $time[1];
    $complete_date = $day . "-" . $month . "-" . $year . " " . $time;
    return $complete_date;
}

function get_ua()
{

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser = "Unknown Browser";

    $browser_array = array(
        '/msie/i' => 'Internet Explorer',
        '/firefox/i' => 'Firefox',
        '/safari/i' => 'Safari',
        '/chrome/i' => 'Chrome',
        '/opera/i' => 'Opera',
        '/netscape/i' => 'Netscape',
        '/maxthon/i' => 'Maxthon',
        '/konqueror/i' => 'Konqueror',
        '/mobile/i' => 'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $browser = $value;
        }

    }

    return $browser;

}

function get_ip()
{
    return $_SERVER['REMOTE_ADDR'];
}

function ext($filename)
{
    //$filename = strtolower($filename);
    return substr($filename, strrpos($filename, '.') + 1);
}

function get_hash($salt, $pass)
{
    return md5($salt . $pass . $salt);
}

function valid_hash($hash)
{
    if (strlen(trim($hash)) != 32) {
        return false;
    } else return true;
}

function site_url()
{
    $scheme = "http://";
    if(substr_count($_SERVER['SERVER_NAME'], "terpikka.ru")>0){
        $scheme = "https://";
    }
    return $scheme . $_SERVER['HTTP_HOST'];
}

function http_link($link, $https = false)
{
    if (substr($link, 0, 6) == "http://" OR
        substr($link, 0, 7) == "https://"
    ) {
        return $link;
    }
    if ($https) {
        return "https://" . $link;
    }
    return "http://" . $link;
}

function set_title($add_sitename, $delimiter, $p1, $p2=null){
    $delimiter = " " . $delimiter . " ";
    $title = "";
    if($add_sitename) {
        $title .= L('SITENAME');

    }
    if(isset($p1)){
        $title .= $delimiter.$p1;
    }
    if(isset($p2)){
        $title .= $delimiter.$p2;
    }
    return  $title;
}

function is_active_page($url_param, $page, $add_class){
    if($url_param == $page){
        return 'class="' . $add_class . '"';
    }
}

/* обработка времени*/
/**
 * дата из mysql_f to rus
 * 1 - день месяц год
 */

function MysqlDaySetTime($days){
    if($days == null) $day = 0;
    $day_text = "days";
    $start_date = date("Y-m-d H:i:s");
    $PREFIX = "+" . $days . " " . $day_text;
    return date('Y-m-d H:i:s', strtotime($PREFIX, strtotime($start_date)));
}

function only_date($date)
{
    return $date = substr($date, 0, 10);
}

function only_time($date)
{
    return $date = substr($date, 11, 5);
}

function pass_date($date)
{
    if ($date == "0000-00-00 00:00:00") {
        $date = Configure::read('SITE_START_DATE');
    }

    $month = substr($date, 5, 2);
    $year = substr($date, 0, 4);
    $day = substr($date, 8, 2);
    $lang_month = "of_m_" . $month;
    $lang_month = L($lang_month);


    if (strlen($date) == 19) {
        $hour = substr($date, 11, 2);
        $minute = substr($date, 14, 2);

        $time = $hour . ":" . $minute;

        $formatted_date = $time . " " . $day . " " . $lang_month . " " . $year;
    } else {
        $formatted_date = $day . " " . $lang_month . " " . $year;
    }
    return $formatted_date;
}

function lang_calendar($date, $format = 1, $time_view = true)
{
    if ($date == "0000-00-00 00:00:00") {
        $date = Configure::read('SITE_START_DATE');
    }

    $month = substr($date, 5, 2);
    $year = substr($date, 0, 4);
    $day = substr($date, 8, 2);
    $lang_month = "of_m_" . $month;
    $lang_month = L($lang_month);

    $h_prefix = L('h_prefix');
    $m_prefix = L('m_prefix');
    $y_prefix = L('y_prefix');

    if (strlen($date) == 19) {
        $hour = substr($date, 11, 2);
        $minute = substr($date, 14, 2);

        $time = $hour . $h_prefix . " " . $minute . " " . $m_prefix;

        $formatted_date = $day . " " . $lang_month . " " . $year . " " . $y_prefix . " ";
        if ($time_view) {
            $formatted_date = $formatted_date . $time;
        }
    } else {
        $formatted_date = $day . " " . $lang_month . " " . $year . " " . $y_prefix . " ";
    }
    return $formatted_date;

}

function dateJsToMysql($date)
{
    $day = substr($date, 0, 2);
    $year = substr($date, 6, 4);
    $month = substr($date, 3, 2);

    return $year . "-" . $month . "-" . $day;
}

function datepickerJsToMysql($date)
{
    $month = substr($date, 0, 2);
    $year = substr($date, 6, 4);
    $day = substr($date, 3, 2);

    return $year . "-" . $month . "-" . $day;
}

function datepickerMysqlToJs($date)
{
    $month = substr($date, 5, 2);
    $year = substr($date, 0, 4);
    $day = substr($date, 8, 2);

    return $month . "/" . $day . "/" . $year;
}


function prepare_fio($firstname, $lastname, $fathername)
{
    return mb_ucfirst($firstname) . " " . mb_ucfirst($lastname) . " " . mb_ucfirst($fathername);
}


function prepare_fio_short($lastname, $firstname, $fathername)
{
    return mb_ucfirst($lastname) . " " . mb_strtoupper(mb_substr($firstname, 0, 1)) . ". " . mb_strtoupper(mb_substr($fathername, 0, 1)) . ".";
}

/**
 * @param1 - usd, rub, kzt
 * @param2 - full text or short text
 * @param2 - rus, eng or kaz
 */
function set_money_format($type, $length = "full", $lang = "rus")
{
    //валюта - рубль

    if ($type == 'rub') {
        if ($lang == "rus") {
            if ($length == "full") {
                $prefix = "рублей";
            } elseif ($length = "short") {
                $prefix = "руб";
            }

        } else if ($lang == "eng") {
            if ($length == "full") {
                $prefix = "rub";
            } elseif ($length = "short") {
                $prefix = "rubles";
            }

        } else if ($lang == "kaz") {
            if ($length == "full") {
                $prefix = "rub";
            } elseif ($length = "short") {
                $prefix = "rubles";
            }
        }
    } else if ($type == 'usd') {
        //валюта - доллар

        if ($lang == "rus") {
            if ($length == "full") {
                $prefix = "долларов";
            } elseif ($length = "short") {
                $prefix = "дол.";
            }

        } else if ($lang == "eng") {
            if ($length == "full") {
                $prefix = "usd";
            } elseif ($length = "short") {
                $prefix = "dollars";
            }

        } else if ($lang == "kaz") {
            if ($length == "full") {
                $prefix = "usd";
            } elseif ($length = "short") {
                $prefix = "dollars";
            }
        }
    } else if ($type == 'kzt') {
        //валюта - тенге

        if ($lang == "rus") {
            if ($length == "full") {
                $prefix = "тенге";
            } elseif ($length = "short") {
                $prefix = "тенге";
            }

        } else if ($lang == "eng") {
            if ($length == "full") {
                $prefix = "tenge";
            } elseif ($length = "short") {
                $prefix = "kzt";
            }

        } else if ($lang == "kaz") {
            if ($length == "full") {
                $prefix = "тенге";
            } elseif ($length = "short") {
                $prefix = "тенге";
            }
        }
    } else if ($type == 'credits') {
        //валюта - тенге

        if ($lang == "rus") {
            if ($length == "full") {
                $prefix = "монет";
            } elseif ($length = "short") {
                $prefix = "кр";
            }

        } else if ($lang == "eng") {
            if ($length == "full") {
                $prefix = "credits";
            } elseif ($length = "short") {
                $prefix = "crd";
            }

        } else if ($lang == "kaz") {
            if ($length == "full") {
                $prefix = "credits";
            } elseif ($length = "short") {
                $prefix = "crd";
            }
        }
    }

    return $prefix;
}

$money_type['rub'] = "рублей";


/**
 * @param1 - количество
 * @array (Лошадь, Лошади, Лошадей)
 */
function word_count_format($count, $arr)
{

    $c = substr($count, -2);
    $x = substr($count, -1);
    //echo "<" . $c . ">";
    if ($c >= 10 AND $c <= 20) return $arr[2];
    else {
        if ($x == 1) return $arr[0];
        elseif ($x > 1 AND $x < 5) return $arr[1];
        else return $arr[2];
    }
}

function bold($text)
{

    return " <b>" . $text . "</b> ";

}

// ПЕРЕНЕСЕНО В КОМПОНЕНТ
function day_separator($day_in_seconds, $days_only = false)
{
    $viewed_string = "";
    $days = floor($day_in_seconds / 86400);

    if ($days > 0) {
        $viewed_string .= $days . " " . word_count_format($days, ['день', 'дня','дней']);
        $left = $day_in_seconds - $days * 86400;
    } else {
        $left = $day_in_seconds;
    }

    if ($days_only) {
        if (empty($viewed_string)) $viewed_string = L('1ST_DAY');
        return $viewed_string;
    }

    if($days>1){
        return $viewed_string;
    }

    $viewed_string .= " ";
    $hours = floor($left / 3600);
    if ($hours > 0) {

        $viewed_string .= $hours . " " . word_count_format($hours, ['час', 'часа','часов']);
        $left = $left - $hours * 3600;
    }

    $viewed_string .= " ";
    $minutes = floor($left / 60);
    if ($minutes > 0) {
        $viewed_string .= $minutes . " " . word_count_format($minutes, ['минута', 'минуты','минут']);
        $left = $left - $minutes * 60;
    }

    $seconds = $left;
    $viewed_string .= " ";
    if ($seconds > 0) {
        $viewed_string .= $seconds . " " . word_count_format($seconds, ['секунда', 'секунд','секунд']);
    }

    return $viewed_string;

}
//ПЕРЕНЕСЕНО В КОМПОНЕНТ
function days_later($day_in_seconds, $real_time = '')
{
    $days = floor($day_in_seconds / 86400);
    if ($days == 0) {
        if($day_in_seconds<=60){
            return L('JUST_NOW');
        }
        else if($day_in_seconds>60 AND $day_in_seconds<=3600){
            $min = floor($day_in_seconds / 60);
            //return L('MINUTES_PLURAL');
            return $min . " " . word_count_format($min, L('MINUTES_PLURAL')) . " " . L('LATER');
        }

        //

        if($day_in_seconds>3600 AND $day_in_seconds<=86400){
            $hour = floor($day_in_seconds / 3600);
            return $hour . " " . word_count_format($hour, L('HOURS_PLURAL')) . " " . L('LATER');
        }
        //return L('TODAY');
    } elseif ($days == 1) {
        return L('YESTERDAY') . " " . L('AT') . " " . only_time($real_time);
    } elseif ($days > 1 AND $days < 30) {
        return $days . " " . word_count_format($days, L('DAYS_PLURAL')) . " " . L('LATER');
    } elseif ($days >= 30 AND $days < 365) {
        $month = floor($days / 30);
        return $month . " " . word_count_format($month, L('MONTH_PLURAL')) . " " . L('LATER');
    } elseif ($days >= 365) {
        $years = floor($days / 365);
        return $years . " " . word_count_format($years, L('YEAR_PLURAL')) . " " . L('LATER');
    }
}


function set_error($text, $error_header)
{
    echo "<div style='margin: auto; text-align: center;  background: #f8f8f8; border-radius: 10px; padding: 4px'>";
    echo "<h3 style='margin: auto; font-size: 24px; font-family: tahoma; '>" . $error_header . "</h3><br>";
    echo "<h3 style='margin: auto; font-size: 18px; font-family: tahoma; color: red'>" . $text . "</h3><br>";
    echo "<script>
		function goBack() {
			window.history.back();
		}
		</script>";
    echo "<a href='#' style='margin: auto; font-size: 26px; color: #888; font-family: tahoma' onclick=\"goBack()\"'>Вернуться к форме</a>";
    die();
}



function pgn_link($url, $page, $current = false, $ctrl)
{
    $data = array('page' => $page);
    $query_string = http_build_query($data);
    if ($current) {
        $class_selected = "selected_pgn_link";
    } else {
        $class_selected = "pgn_link";
    }

    if (!empty($url)) {
        $url = $url . "&";
    } else {
        $url = "";
    }

    return "<a class='" . $class_selected . "' href='" . site_url() . $ctrl . "?" . $url . $query_string . "'>" . $page . "</a>";
}

function paginator($page = 0, $pages = 0, $classes = "", $params = array(), $ctrl = "")
{
    if (is_array($params)) {
        $query_string = http_build_query($params);
    } else {
        $query_string = 0;
    }
    $PGN = "Страницы: ";
    if ($pages > 5) {
        $PGN .= pgn_link($query_string, $page - 1, $ctrl);
        $PGN .= pgn_link($query_string, $page, $ctrl);
        if ($page < $pages) {
            $PGN .= pgn_link($query_string, $page + 1, $ctrl);
        }
    } else {
        for ($c = 1; $c <= $pages; $c++) {
            if ($c == $page) {
                $current = true;
            } else {
                $current = false;
            }
            $PGN .= pgn_link($query_string, $c, $current, $ctrl);
        }
    }

    return $PGN;
}

function new_pgn_link($url, $page, $current = false, $ctrl, $dir_type = "")
{
    $page_text = "";
    $data = array('page' => $page);
    $query_string = http_build_query($data);
    if ($current) {
        $class_selected = "page-itempage-item active active";
        $page_text = $page;
    } else if ($dir_type == "prev") {
        $class_selected = "page-item page-pre";
        $page_text = "&lt;";
    } else if ($dir_type == "next") {
        $class_selected = "page-item page-next";
        $page_text = "&gt;";
    } else {
        $class_selected = "page-item pgn_link";
        $page_text = $page;
    }

    if (!empty($url)) {
        $url = $url . "&";
    } else {
        $url = "";
    }

    return "<li  class='" . $class_selected . "'><a href='" . site_url() . $ctrl . "?" . $url . $query_string . "' class='page-link'>" . $page_text . "</a></li>";
}

function new_paginator($page = 0, $pages = 0, $classes = "", $params = array(), $ctrl = "")
{
    if (is_array($params)) {
        $query_string = http_build_query($params);
    } else {
        $query_string = 0;
    }
    $PGN = "";
    if ($pages > 20) {
        // предыдущая
        if($page>1) {
            $PGN .= new_pgn_link($query_string, $page - 1, "", $ctrl, "");
        }
        // текущая
        $PGN .= new_pgn_link($query_string, $page, true, $ctrl, "");
        if ($page < $pages) {
            $PGN .= new_pgn_link($query_string, $page + 1, "", $ctrl, "");
            //$PGN .= " ... ";
            if($page>1 AND $page<$pages){
                //$PGN .= new_pgn_link($query_string, $page, true, $ctrl, "");
            }
            $PGN .= new_pgn_link($query_string, $pages, "", $ctrl, "");
        }
    } else {
        for ($c = 1; $c <= $pages; $c++) {
            if ($c == $page) {
                $current = true;
            } else {
                $current = false;
            }
            $PGN .= new_pgn_link($query_string, $c, $current, $ctrl, "");
        }
    }

    if ($page > 1) {
        $PGN = new_pgn_link($query_string, $page - 1, false, $ctrl, "prev") . $PGN;
    }
    if ($page <= $pages - 1) {
        $PGN = $PGN . new_pgn_link($query_string, $page + 1, false, $ctrl, "next");
    }
    return $PGN;
}

function text_preview($str, $i)
{
    if(mb_strlen($str)>$i) {
        // Здесь приводим в кодировку win
        $str = iconv('UTF-8', 'windows-1251', $str);
        // Указываем также с какого по какой символ
        $str = substr($str, 0, $i);
        // Здесь возвращаем обратно в utf
        $str = iconv('windows-1251', 'UTF-8', $str);
        return $str . "...";
    } else {
        return $str;
    }
}

function lang_prefix()
{
    $def_lang = 'ru';
    $lang_session = (isset($_SESSION['lang'])) ? $_SESSION['lang'] : null;
    if ($lang_session == null) return $def_lang;
    $langs = array('ru', 'en');
    $lang = strtolower($lang_session);
    if (in_array($lang, $langs)) {
        return $lang;
    } else return $def_lang;
}

function is_valid_name($string, $min, $max)
{
    if ((!empty($string)) AND (mb_strlen($string) <= $max) AND (mb_strlen($string) >= $min)
        AND (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ ]+$/u', $string))
    ) {

        return true;
    } else {
        return false;
    }
}

function generate_mail_key($id, $salt)
{

    return md5(md5(time()) . md5($id) . md5($salt));

}

function valid_password($pass)
{
    if (preg_match("/^[A-Z0-9_-]{8,32}$/i", $pass)) {
        return true;
    } else return false;
}

function valid_mail($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else return false;
}

function valid_phone_number($number)
{
    if(preg_match("/^[0-9]{11}$/", $number)) {
        return true;
    }
    return false;
}

function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}

function valid_minute($int)
{
    if ($int >= 0 AND $int <= 59) {
        return true;
    } else return false;
}

function valid_hour($int)
{
    if ($int >= 0 AND $int <= 23) {
        return true;
    } else return false;
}

function valid_date($date)
{
    return (bool)strtotime($date);
}

function get_wday($date)
{
    $day_of_week = date('w', strtotime($date));
    if ($day_of_week == 0) $day_of_week = 6;
    else $day_of_week = $day_of_week - 1;
    return $day_of_week;
}

function check_link($url, $domain)
{
    if (mb_strpos($url, $domain) != 0) {
        return false;
    } else return true;
}

function ts_from_date($date)
{

    return strtotime($date);
}

function get_year_from_mysql_date($date)
{
    $year = substr($date, 0, 4);
    if ((int)$year > 0 AND (int)$year < 9999) {
        return $year;
    } else return null;
}

function is_active_link($action, $link)
{
    if ($action == $link) {
        echo 'class="active"';
    }
}

function L($text)
{
    return Configure::read($text);

}

function new_mail_hash($mail)
{
    return md5($mail . "MAIL" . time());
}

function bolder_text($string)
{
    if ((int)$string > 0) {
        return "<b>" . $string . "</b>";
    } else return $string;

}

//Ссылка на внешний ресурс
function outer_link($link)
{
    if (mb_strlen($link) > 0) {
        if (mb_strpos($link, "http") >= 0) {
            return "http://" . $link;
        } else {
            return $link;
        }
    } else {
        return "#";
    }
}

//функция текущей даты php
function now_date()
{
    return date("Y-m-d H:i:s");
}

// random float value
function random_float ($min,$max) {
    return ($min+lcg_value()*(abs($max-$min)));
}

function floatFontSize($string){
    if (strlen($string) >= 7){
        return 'fontXLarge';
    } else if(strlen($string) <= 6 AND (strlen($string) > 5)){
        return 'fontLarge';
    }
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function format_time($t,$f=':') // t = seconds, f = separator
{
    return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
}

function domain_url($url) {
    $result = parse_url($url);
    return $result['host'];
}

function resize_image($file, $w, $h, $crop=FALSE, $dist = null) {
    $exif = exif_read_data($file);
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    $img = imagecreatefromjpeg($file);
    if (isset($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 3:
                $img = imagerotate($img, 180, 0);
                break;
            case 6:
                $img = imagerotate($img, -90, 0);
                break;
            case 8:
                $img = imagerotate($img, 90, 0);
                break;
        }
    }
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    imagejpeg($dst, $dist);
    return true;
}

function getUrl($url)
{
    $ch = curl_init();
    $timeout = 5; // set to zero for no timeout
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXY, "http://proxy.symmetron.ru"); //your proxy url
    curl_setopt($ch, CURLOPT_PROXYPORT, "8080"); // your proxy port number
    curl_setopt($ch, CURLOPT_PROXYUSERPWD, "symmetron\Arthur.Mataryan:Cosmos1234"); //username:pass
    //echo mb_detect_encoding(curl_exec($ch));
    $file_contents = curl_exec($ch);
    //$file_contents = iconv("Windows-1251", "UTF-8", $file_contents);
    curl_close($ch);
    return $file_contents;
}

function prices_not_empty($prices){
    if(key_exists('USD', $prices)){
        $currency = 'USD';
    } elseif(key_exists('EUR', $prices)){
        $currency = 'EUR';
    } elseif(key_exists('SGD', $prices)){
        $currency = 'SGD';
    } elseif(key_exists('CNY', $prices)){
        $currency = 'CNY';
    } else $currency = null;
    if($currency!=null){
        return $currency;
    }
    return false;
}
function generateTreeMenu($datas, $parent = 0, $limit=0){
    if($limit > 20) return '';
    $tree = '';
    $tree = '<ul>';
    for($i=0, $ni=count($datas); $i < $ni; $i++){
        if($datas[$i]['parent_id'] == $parent){
            $tree .= '<li><a>';
            $tree .= $datas[$i]['title'].'</a>';
            $tree .= generateTreeMenu($datas, $datas[$i]['id'], $limit++);
            $tree .= '</li>';
        }
    }
    $tree .= '</ul>';
    return $tree;
}

function olLiTree($tree)
{
    echo '<ul class="tree">';
    foreach($tree as $key => $item) {
        if (is_array($item)) {
            if(count($item)>0) {
            //if ( isset( $item['children'] ) ) {
                echo "<li><a href='#" . $key . "' class='set_cat' data-id='" . $key . "'>" . $key;
                olLiTree($item);
                echo '</a></li>';
            }
        } else {

                echo '<li>', $item, '</li>';

        }
    }
    echo '</ul>';
}

function select_tree($tree, $t = 0, $last_id = 0, $category_id = 0)
{
    if(is_array($tree)) {
        $t++;
        foreach ($tree as $key => $item) {
            if (!is_array($item)) {
                $t--;
            }
            $space = str_repeat("__ ", $t);
            if(!is_array($item)) {
                if($category_id == $last_id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                echo "<option value='" . $last_id . "' " . $selected . ">" . $space . $item . "</option>";
            }
            $last_id = $key;
            select_tree($item, $t, $last_id, $category_id);
        }
    }
}

function tooltip($title, $content){
    return "<a href='#' class='btn btn-warning btn-xs add-popover' data-original-title='" .
        $title . "' data-content='" . $content ."' data-placement='bottom' data-trigger='hover' data-toggle='popover'>?</a>";
}

function price_rub($price){
    return $price . " " . "руб.";
}

function moderation_message($field_name, $moderations_data){
    if(!isset($moderations_data[$field_name])){
        return '<div class="main moderation_message"><span class="text-muted">Модерация не производилась </span></div>';
    }
    $moderation_data = $moderations_data[$field_name];
    $created = lang_calendar($moderation_data['created']);
    $status = $moderation_data['status'];
    if($status == "warning"){
        return '<div class="main moderation_message"><i class="fa-exclamation-triangle fa text-warning"></i> &nbsp;<span class="text-muted"><strong>Предупреждение!</strong> Поле не заполнено или заполнено 
недостаточно корректно, проверьте данные. Проверка от ' . $created . ' </span></div>';
    } else if($status == "error"){
        return '<div class="main moderation_message"><i class="text-danger fa fa-minus-circle"></i> &nbsp;<span class="text-muted"><strong>Внимание!</strong> Поле не заполнено или заполнено недостаточно корректно, проверьте данные. 
Обязательно корректное заполнение поля. Проверка от ' . $created . ' </span></div>';
    } else if($status == "success"){
        return '<div class="main moderation_message"><i class="fa fa-check-square text-success"></i> &nbsp;<span class="text-muted">Поле заполнено корректно, проверено ' . $created . ' </span></div>';
    }
}

function prepare_import_field($field){
    return mb_ucfirst(mb_strtolower(trim($field)));
}

function array_average($array) {
    return array_sum($array) / count($array);
}

$content_server_host = "http://content.terpikka.ru";

function contentServerFile($file_name, $type, $content_server_host, $product_id = null)
{
    if ($type == "product_images") {
        return $content_server_host . "/files/" . $type . "/p" . $product_id . "/" .  $file_name;
    }
}

function fast_request($url)
{
    $parts=parse_url($url);
    $fp = fsockopen($parts['host'],isset($parts['port'])?$parts['port']:80,$errno, $errstr, 30);
    $out = "GET ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Length: 0"."\r\n";
    $out.= "Connection: Close\r\n\r\n";

    fwrite($fp, $out);
    fclose($fp);
}

function server_path(){
    if(substr_count($_SERVER["REQUEST_URI"], "?")>0){
        return explode("?", $_SERVER["REQUEST_URI"])[0];
    } else {
        return $_SERVER["REQUEST_URI"];
    }
}

function ContentServerKey($content_server, $file){
    return md5($content_server . $file . date("Y-m-d"));
}

function str_split_utf8($str) {
    $split = 1;
    $array = array();
    for ($i=0; $i < strlen($str); ){
        $value = ord($str[$i]);
        if($value > 127){
            if ($value >= 192 && $value <= 223)      $split = 2;
            elseif ($value >= 224 && $value <= 239)  $split = 3;
            elseif ($value >= 240 && $value <= 247)  $split = 4;
        } else $split = 1;
        $key = NULL;
        for ( $j = 0; $j < $split; $j++, $i++ ) $key .= $str[$i];
        array_push( $array, $key );
    }
    return $array;
}
