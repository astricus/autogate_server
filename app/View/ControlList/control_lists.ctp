<div id="page-content ">
    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel control_list_block">
                    <div class="panel-heading">
                        <h3 class="panel-title ">Контрольные листы</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Название</th>
                                    <th>Описание</th>
                                    <th>Направление</th>
                                    <th>Логика действия</th>
                                    <th>Дата добавления</th>
                                    <th>Ответственное лицо/владелец</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($control_lists as $list) {
                                    $enabled = "";
                                    $enabled_status = $list['Control_List']['enabled'];
                                    if($enabled_status=="true"){
                                        $enabled = "Разрешен";
                                    } else {
                                        $enabled = "Запрещен";
                                    }
                                    $n++;
                                    $list_id = $list['Control_List']['id'];
                                    ?>
                                    <tr class="list_id_item" data-list_id="<?=$list_id ?>">
                                        <td><?= $n ?></td>
                                        <td><?= $list['Control_List']['list_name'] ?></td>
                                        <td><?= $list['Control_List']['description'] ?></td>
                                        <td><? echo $direction_list[$list['Control_List']['direction']];?>
                                        <td><? echo $logic_action_list[$list['Control_List']['action_logic']];?></td>
                                        <td width="200px"><span class="text-info"><?=lang_calendar($list['Control_List']['updated']) ?></span>
                                        </td>
                                        <td><?= $list['Control_List']['list_owner'] ?></td>
                                        <td width="400px">
                                            <?if($enabled_status=="active"){?>
                                                <a class="btn btn-warning" href="/control_list/set_status/list/<?=$list_id?>?status=passed">Запретить</a>
                                            <?} else {?>
                                                <a class="btn btn-success" href="/control_list/set_status/list/<?=$list_id?>?status=active">Разрешить</a>
                                            <?}?>
                                            <a class="btn btn-mint" href="/control_list/update/list/<?=$list_id?>">Изменить</a>
                                            <a class="btn btn-primary" href="/control_list/view/<?=$list_id?>">Правила списка</a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                        <a href="/control_list/create_list/" class="btn btn-success btn-bock">Добавить контрольный список</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>