<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Добавление контрольного листа</h1>
        </div>

        <form action="" method="post" class="form-horizontal create_control_list">
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Название списка</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Название списка" name="list_name" autofocus required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Описание списка</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Описание списка" name="description" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Ответственный/Владелец</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Ответственный/Владелец" name="list_owner" required>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Действие по списку</label>
                        <select class="form-control col-sm-6" name="action_logic">
                            <?
                            foreach ($logic_action_list as $key => $val){?>
                                <option value="<?=$key?>"><?=$val?></option>
                            <?}?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Направление</label>
                        <select class="form-control col-sm-6" name="direction">
                            <option value="" selected>Не указано</option>
                            <?
                            foreach ($direction_list as $key => $val){?>
                                <option value="<?=$key?>"><?=$val?></option>
                            <?}?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4">
                        <button class="btn btn-primary btn-block create_control_list_button" type="submit">Добавить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/control_lists">Отмена</a>
                    </div>
                </div>
            </div>

        </form>


    </div>
</div>


