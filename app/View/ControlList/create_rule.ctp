<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Добавление правила в список "<?=$list_name?>"</h1>
        </div>

        <form action="" method="post" class="form-horizontal create_control_rule" data-list_id = "<?=$list_id?>">
            <input type="hidden" name="list_id" value="<?=$list_id?>">
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Маска по имени</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Маска по имени" name="firstname_mask" autofocus>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Маска по фамилии</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Маска по фамилии" name="lastname_mask" autofocus>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Маска по номеру документа</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Маска по номеру документа" name="document_mask">
                    </div>

                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Персона</label>
                        <input type="text" class="person_find col-sm-1" placeholder="Поиск персоны по имени/организации" name="person_string">
                        <select name="person_id" class="col-sm-5 person_block form-control"></select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4">
                        <button class="btn btn-primary btn-block create_control_rule_button" type="submit">Добавить</button>
                    </div>

                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/control_list/view/<?= $list_id ?>">Отмена</a>
                    </div>

                </div>
            </div>

        </form>

    </div>
</div>