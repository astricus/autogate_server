<div id="page-content">
    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title control_list_block">Контрольный список "<?=$list_name?>"</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Описание</th>
                                    <th>Направление</th>
                                    <th>Логика действия</th>
                                    <th>Дата добавления</th>
                                    <th>Ответственное лицо/владелец</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                foreach ($control_lists as $list) {
                                    $enabled = "";
                                    $enabled_status = $list['Control_List']['enabled'];
                                    if($enabled_status=="true"){
                                        $enabled = "Разрешен";
                                    } else {
                                        $enabled = "Запрещен";
                                    }
                                    $list_id = $list['Control_List']['id'];
                                    ?>
                                    <tr class="list_id_item" data-list_id="<?=$list_id ?>">
                                        <td><?= $list['Control_List']['description'] ?></td>
                                        <td><?= $direction_list[$list['Control_List']['direction']] ?></td>
                                        <td><?= $logic_action_list[$list['Control_List']['action_logic']] ?></td>
                                        <td><span class="text-info">
                                                <i class="pli-clock"></i><?=lang_calendar($list['Control_List']['updated']) ?></span>
                                        </td>
                                        <td><?= $list['Control_List']['list_owner'] ?></td>
                                        <th>
                                            <?if($enabled_status=="active"){?>
                                                <a class="btn btn-warning" href="/control_list/set_status/list/<?=$list_id?>?status=passed">Запретить</a>
                                            <?} else {?>
                                                <a class="btn btn-success" href="/control_list/set_status/list/<?=$list_id?>?status=active">Разрешить</a>
                                            <?}?>
                                            <a class="btn btn-mint" href="/control_list/update/list/<?=$list_id?>">Изменить</a>
                                            <?if($check_delete==0){?>
                                            <a class="btn btn-danger delete_list" data-id="<?=$list_id?>" title="Удалить список с правилами,удалить можно только в том случае, если по данному списку нет проходов" href="/control_list/delete/list/<?=$list_id?>"><span class="fa fa-trash"></a>
                                    <?}?></tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-heading">
                        <h3 class="panel-title control_list_block">Правила</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Маска документа</th>
                                    <th>Маска по имени</th>
                                    <th>Маска по фамилии</th>
                                    <th>Персона (идентификатор)</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($rules as $rule) {
                                    $status = $rule['Control_Rule']['enabled'];
                                    $n++;
                                    $rule_id = $rule['Control_Rule']['id'];
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><?= $rule['Control_Rule']['document_mask'] ?></td>
                                        <td><?= $rule['Control_Rule']['firstname_mask'] ?></td>
                                        <td><?= $rule['Control_Rule']['lastname_mask'] ?></td>
                                        <td><?= $rule['person_name'] ?></td>

                                        <th>
                                            <?if($status=="active"){?>
                                                <a class="btn btn-warning" href="/control_list/set_status/rule/<?=$rule_id?>?status=passed&list_id=<?=$list_id ?>">Запретить</a>
                                            <?} else {?>
                                                <a class="btn btn-success" href="/control_list/set_status/rule/<?=$rule_id?>?status=active&list_id=<?=$list_id ?>">Разрешить</a>
                                            <?}?>
                                            <a class="btn btn-mint" href="/control_list/update/rule/<?=$rule_id?>">Изменить правило</a>
                                            <a class="btn btn-danger modal_action" title="Удалить правило, удалить можно только в том случае, если по данному списку нет проходов" href="/control_list/delete/rule/<?=$rule_id?>?list_id=<?=$list_id ?>"><span class="fa fa-trash"></a>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                        <div><?if(count($rules) == 0) echo "Правила списка еще не созданы.";?></div>
                        <a href="/control_list/create_rule/<?=$list_id?>" class="btn btn-success btn-bock">Добавить правило</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>