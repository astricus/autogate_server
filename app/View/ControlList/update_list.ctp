<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Редактирование контрольного листа "<?=$list['list_name']?>"</h1>
        </div>

        <form action="" method="post" class="form-horizontal update_control_list" data-rule_id="<?= $list['id'] ?>">
                <input type="hidden" name="list_id" value="<?= $list['id'] ?>">
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Название списка</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Название списка" name="list_name" autofocus value="<?=$list['list_name']?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Описание списка</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Описание списка" name="description" autofocus value="<?=$list['description']?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Ответственный/Владелец</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Ответственный/Владелец" name="list_owner" value="<?=$list['list_owner']?>">
                    </div>

                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Действие по списку</label>
                        <select class="form-control col-sm-6" name="action_logic">
                            <?
                            foreach ($logic_action_list as $key => $val){?>
                                <option value="<?=$key?>" <?if($list['action_logic']==$key) echo "selected";?>><?=$val?></option>
                            <?}?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Направление</label>
                        <select class="form-control col-sm-6" name="direction">
                            <?
                            foreach ($direction_list as $key => $val){?>
                                <option value="<?=$key?>" <?if($list['direction']==$key) echo "selected";?>><?=$val?></option>
                            <?}?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4">
                        <button class="btn btn-primary btn-block update_control_list_button" type="submit">Сохранить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/control_list/view/<?=$list['id']?>">Отмена</a>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-mint" href="/control_list/view/<?=$list['id']?>">Правила списка</a>
                    </div>

                </div>
            </div>

        </form>


    </div>
</div>


