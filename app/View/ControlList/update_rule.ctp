<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Редактирования правила для списка "<?= $list_name ?>"</h1>

            <div class="well">Правила контрольного списка могут работать в двух режимах. Первый режим - основан на
                масках для имени, фамилии персоны либо номеру документа.
                В этом случае от правила автоматически отвязывается персона. Во втором режиме может быть к правилу
                привязана только персона, в этом случае маски автоматически отключаются
            </div>
        </div>
        <? $rule_id = $rule['Control_Rule']['id'] ?>

        <script>
            $(document).ready(function () {

                $(document).on("change", ".person_find", function () {
                    var value = $(this).val();
                    console.log("change");
                    if (value != "") {
                        $(".blocker_field").attr("disabled", true);
                    } else {
                        $(".blocker_field").removeAttr("disabled");
                    }
                });

                $(document).on("click", ".reset_form", function () {
                    $(".person_find").val('');
                    $(".blocker_field").val('');
                });

                $(document).on("change", ".blocker_field", function () {
                    var value = $(this).val();
                    console.log("change");
                    if (value != "") {
                        $(".person_find").attr("disabled", true);
                        $(".person_block").attr("disabled", true);
                        $(".person_find").val('');
                        $(".person_block").val('');
                    } else {
                        $(".person_find").removeAttr("disabled");
                        $(".person_block").removeAttr("disabled");
                    }
                });
            });

        </script>


        <div class="col-lg-6">

            <form action="" method="post" class="form-horizontal update_control_rule" data-rule_id="<?= $rule_id ?>">
                <input type="hidden" name="rule_id" value="<?= $rule_id ?>">

                <div class="form-group">
                    <label class="control-label col-sm-3">Персона</label>
                    <div class="col-sm-3">
                        <input type="text" class="person_find form-control"
                               placeholder="Поиск персоны по имени/организации" name="person_string">
                    </div>
                    <div class="col-sm-6">
                        <select name="person_id" class="person_block form-control">
                            <option value="<?= $rule['Control_Rule']['person_id'] ?>"><?= $person_name ?></option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Маска по имени</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control blocker_field" placeholder="Маска по имени"
                               name="firstname_mask"
                               value="<?= $rule['Control_Rule']['firstname_mask'] ?>">
                    </div>
                </div>


                <div class="form-group">

                    <label class="control-label col-sm-3">Маска по фамилии</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control blocker_field" placeholder="Маска по фамилии"
                               name="lastname_mask" value="<?= $rule['Control_Rule']['lastname_mask'] ?>">
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-3">Маска по номеру документа</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control blocker_field"
                               placeholder="Маска по номеру документа"
                               name="document_mask" value="<?= $rule['Control_Rule']['document_mask'] ?>">
                    </div>
                </div>


                <div class="form-group">

                    <div class="col-sm-4">
                        <button class="btn btn-primary btn-block update_control_rule_button" type="submit">Сохранить
                        </button>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-default btn-block reset_form">Сбросить форму</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="btn btn-default btn-block"
                           href="/control_list/view/<?= $rule['Control_Rule']['control_list_id'] ?>">Отмена</a>
                    </div>
                </div>


            </form>
        </div>

    </div>
</div>