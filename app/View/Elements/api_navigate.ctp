<div class="row">
    <div class="col-sm-12">
        <a href="/manage/sandbox/script/base_script" class="btn btn-success"><span class="fa fa-code"></span> Create Web Script</a>
        <a href="/manage/sandbox/script/list" class="btn btn-success"><span class="fa fa-code"></span> Examples </a>
        <a href="/manage/sandbox/http/get" class="btn btn-success"><span class="fa fa-code"></span> HTTP Get Response</a>
        <a href="/manage/sandbox/http/post" class="btn btn-success"><span class="fa fa-code"></span> HTTP Post Response</a>
        <a href="/manage/sandbox/http/curl" class="btn btn-success"><span class="fa fa-code"></span> Curl Api </a>
        <a href="/manage/sandbox/email/simple_sending" class="btn btn-success"><span class="fa fa-code"></span> Email Api</a>
        <a href="/manage/sandbox/file/simple_upload" class="btn btn-success"><span class="fa fa-code"></span> File Api </a>
        <a href="/manage/sandbox/database/list" class="btn btn-success"><span class="fa fa-code"></span> Database Api </a>
        <a href="/manage/sandbox/data_object/simple_data_object" class="btn btn-success"><span class="fa fa-code"></span> Data Object Api </a>
        <a href="/manage/sandbox/user/simple_user" class="btn btn-success"><span class="fa fa-code"></span> User Api </a>
        <a href="/manage/sandbox/websocket/simple_socket" class="btn btn-success"><span class="fa fa-code"></span> Web Socket Api </a>
    </div>
</div>
<hr>