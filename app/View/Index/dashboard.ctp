<div id="page-content">

    <?
    $autogate_1 = $autogates[0]['Autogate'];
    $autogate_1_dir = $autogate_1['direction'];
    $autogate_1_id = $autogate_1['id'];
    $autogate_id_2 = $autogates[1]['Autogate'];
    $autogate_2_dir = $autogate_id_2['direction'];
    $autogate_2_id = $autogate_id_2['id'];

    ?>
    <script>
        var autogate_1_id = '<?=$autogate_1_id?>';
        var autogate_2_id = '<?=$autogate_2_id?>';
        var autogate_1_cur_status = null;
        var autogate_2_cur_status = null;
    </script>

    <div class="row">
        <div class="eq-height">

            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Автогейт "<? if ($autogate_1_dir == "Enter") {
                                echo "Вход";
                            } else {
                                echo "Выход";
                            }
                            ?>"</h3>
                    </div>
                    <div class="panel-body ag_1_class_status">

                        <div class="environment_<?=$autogate_1_id?> environment_content_left">
                        </div>

                        <div class="autogate_status_bg" data-autogate_id="<?= $autogate_1_id ?>">
                            <div class="gate_direction_arrow <?
                            if ($autogate_1_dir == "Enter") {
                                echo "gate_direction_up";
                            } else {
                                echo "gate_direction_down";
                            }
                            ?>"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Автогейт "<? if ($autogate_2_dir == "Enter") {
                                echo "Вход";
                            } else {
                                echo "Выход";
                            }
                            ?>"</h3>
                    </div>
                    <div class="panel-body ag_2_class_status">

                        <div class="autogate_status_bg" data-autogate_id="<?= $autogate_2_id ?>">
                            <div class="gate_direction_arrow <?
                            if ($autogate_2_dir == "Enter") {
                                echo "gate_direction_up";
                            } else {
                                echo "gate_direction_down";
                            }
                            ?>"></div>
                        </div>

                        <div class="environment_<?=$autogate_2_id?> environment_content_right">
                        </div>
                    </div>
                </div>
            </div>

    </div>

</div>