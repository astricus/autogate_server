<div id="page-content">
    <div class="panel-heading">
        <h3 class="panel-title">Управление автоматической проходной</h3>
    </div>
    <script>
        $(document).ready(function() {
            var group = 1;
            var checkAutoGateStatus = function() {
                $.ajax({
                    url: "/group_status/" + group,
                    type: "post",
                    dataType: 'json',
                    data: null,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        var gate_status = data.data || undefined;
                        if (gate_status == "Waiting4Environment") {
                            $(".gate_status").text("Ожидание окружения");
                        } else if (gate_status == "WaitingForApproval") {
                            $(".gate_status").text("Ожидание готовности системы");
                        } else if (gate_status == "WaitingForPass") {
                            $(".gate_status").text("Ожидание прохода");
                        } else if (gate_status == "FatalError") {
                            $(".gate_status").text("Критическая ошибка!");
                        } else {
                            $(".gate_status").text("Выполняется проход");
                        }
                    }

                });
                return true;
            };
            setInterval(checkAutoGateStatus, 5000);
        });
    </script>

    <div class="panel">
        <div class="row">
            <? foreach ($autogates as $autogate) {
                $status = $autogate['Autogate']['out_of_service'];
                if ($status == "true") {
                    $stat_class = "panel-danger";
                } else {
                    $stat_class = "panel-info";
                }
                ?>

                <div class="well">Управление <b class="gate_status">
                        <? if ($autogate['Autogate']['out_of_service'] == "true") { ?>
                            Остановлено, двери <? if($autogate['Autogate']['doors_open'] == "true"){ ?>
                                открыты
                            <?} else {?>
                                закрыты
                            <?}?>
                        <? } else { ?>
                            Работает в автоматическом режиме
                        <? } ?></b>
                </div>
            <? } ?>

            <? if ($autogate['Autogate']['out_of_service'] == "true") { ?>

                <a href="/autogate/set_on/<?= $autogate['Autogate']['gate_group'] ?>" class="btn btn-success btn-bock">Включить
                    управление</a>
            <? } else { ?>
                <a href="/autogate/set_off_and_open/<?= $autogate['Autogate']['gate_group'] ?>"
                   class="btn btn-warning modal_action btn-bock">Отключить и оставить двери открыты</a>
                <a href="/autogate/set_off_and_close/<?= $autogate['Autogate']['gate_group'] ?>"
                   class="btn btn-danger modal_action btn-bock">Отключить и закрыть двери</a>
            <? } ?>
            <br>
            Cтатус применяется в течение 30 секунд и после окончания текущего прохода
        </div>
    </div>
</div>