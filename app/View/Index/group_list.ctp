<div id="page-content">

    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Список автогейтов</h3>
                    </div>
                    <div class="panel-body">

                        <div class="list-group">
                            <?
                            $ids = [];
                            foreach ($autogate_group_list as $group){
                                $group = $group['Autogate'];
                                if(in_array($group['gate_group'], $ids)){
                                   continue;
                                }
                                $ids[] = $group['gate_group'];
                            ?>
                            <a href="dashboard/<?=$group['gate_group']?>" class="list-group-item">
                                <h4 class="list-group-item-heading"><?=$group['location']?></h4>
                                <p class="list-group-item-text">Добавить описание автогейта</p>
                            </a>
                            <?}?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>