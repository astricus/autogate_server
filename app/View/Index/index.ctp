<div id="page-content">
    <div class="panel-heading">
        <h3 class="panel-title">Список автогейтов</h3>
    </div>
    <div class="panel">

        <div class="row">
            <table class="table table-striped">
                <tr>
                    <td class="">автогейт</td>
                    <td class="">группа</td>
                    <td class="">направление</td>
                    <td class="">создан</td>
                    <td class="">расположен</td>
                    <td class="">Двери</td>
                    <td class="">Управление</td>
                    <td class="">Статистика проходов</td>
                    <td class=""></td>
                </tr>
            <? foreach ($autogates as $autogate) {
                $status = $autogate['Autogate']['out_of_service'];
                if ($status == "true") {
                    $stat_class = "panel-danger";
                } else {
                    $stat_class = "panel-info";
                }
                ?>



                        <tr>
                            <td class=""><?= $autogate['Autogate']['name'] ?></td>
                            <td class=""><?= $autogate['Autogate']['gate_group'] ?></td>
                            <td class=""><? if($autogate['Autogate']['direction'] == "Exit") {
                                echo "Выход";
                                }  else echo "Вход";?>
                            </td>
                            <td class=""><?= $autogate['Autogate']['mtime'] ?></td>
                            <td class=""><?= $autogate['Autogate']['location'];?></td>
                            <td class="">
                                <? if($autogate['Autogate']['doors_open'] == "true"){ ?>
                                    <div class="btn btn-success btn-bock">открыты</div>
                                <?} else {?>
                                    <div class="btn btn-warning btn-bock">закрыты</div>
                                <?}?>
                            </td>
                            <td class="">
                                <? if($autogate['Autogate']['out_of_service'] == "true"){?>
                                <div class="btn btn-danger btn-bock">недоступен</div>
                                <?} else {?>
                                    <div class="btn btn-success btn-bock">работает</div>
                                <?}?>
                            <? if($autogate['Autogate']['out_of_service'] == "true"){?>
                                    <a href="/autogate/set_on/<?=$autogate['Autogate']['id']?>" class="btn btn-success btn-bock">Включить</a>
                            <?} else {?>
                                    <a href="/autogate/set_off_and_open/<?=$autogate['Autogate']['id']?>" class="btn btn-warning btn-bock">Отключить и оставить двери открыты</a>
                                    <a href="/autogate/set_off_and_close/<?=$autogate['Autogate']['id']?>" class="btn btn-danger btn-bock">Отключить и закрыть двери</a>
                                <?} ?>
                            </td>
                            <td class=""><a href="/pass_list/<?=$autogate['Autogate']['id']?>" class="btn btn-info btn-bock">Cтатистика проходов</a></td>
                        </tr>
            <? } ?>
            </table>
        </div>

    </div>

    <div class="panel">
        <a href="/dashboard" class="btn btn-success btn-bock">Перейти к АРМ охраника</a>
    </div>
</div>