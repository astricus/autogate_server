<div id="page-content">

    <div class="alert alert-success">
        <strong>Проход №<?= $pass_id ?></strong>
    </div>

    <div class="panel">

        <div class="row">
            <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                <tr>
                    <td class=""></td>
                    <td class="">Время</td>
                    <td class="">Событие</td>
                    <td class="">Данные / Фото</td>
                    <td class="">Результат</td>

                </tr>
                <? $t = 0;
                foreach ($pass as $item) {

                    $t++ ?>
                    <tr>
                        <td class=""><?= $t ?></td>
                        <td class="">
                            <?= explode(" ", $item['Event']['event_time'])[1]; ?>
                        </td>
                        <td class=""><?
                        if(key_exists($item['Event']['event_type'], $pass_events)){
                            echo $pass_events[$item['Event']['event_type']];
                        }    else {
                            echo $item['Event']['event_type'];
                        }
                             ?></td>
                        <td class="">
                            <? if ($item['Event']['object_image'] != null){
                                ?>

                            <a target="_blank" class="image_gallery" href="/image_event/<?= $item['Event']['id']; ?>">
                                <img src="/image_event/<?= $item['Event']['id']; ?>" title="person image" style="max-width: 50px;" />
                            </a>
                        </td>
                        <? } ?>
                        <td class=""><?= $item['Event']['event_result']; ?></td>
                    </tr>
                <? } ?>
            </table>
        </div>

    </div>

    <div class="panel">
        <a href="/pass_list" class="btn btn-success btn-bock"> Вернуться к списку проходов</a>
    </div>
</div>