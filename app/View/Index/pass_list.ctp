<div id="page-content">

    <h3>Статистика проходов</h3>
    <script>

        $(document).ready(function () {
            $(document).on("click", ".reset_form", function () {
                $('input[name="organization"]').val("");
                $('input[name="person"]').val("");
                $('input[name="doc_number"]').val("");
                $('form[name="filter_pass_form"]').submit();
            });
        });

    </script>

    <a class="btn <? if ($selection == "today") echo 'btn-success'; else echo 'btn-primary' ?>"
       href="/pass_list?selection=today">Проходы за сегодня</a>
    <a class="btn <? if ($selection == "last_3_days") echo 'btn-success'; else echo 'btn-primary' ?>"
       href="/pass_list?selection=last_3_days">Проходы за последние 3 дня</a>
    <a class="btn <? if ($selection == "last_week") echo 'btn-success'; else echo 'btn-primary' ?>"
       href="/pass_list?selection=last_week">Проходы за последнюю неделю</a>
    <a class="btn <? if ($selection == "last_month") echo 'btn-success'; else echo 'btn-primary' ?>"
       href="/pass_list?selection=last_month">Проходы за последний месяц</a>
    <a class="btn <? if ($selection == "all_time") echo 'btn-success'; else echo 'btn-primary' ?>"
       href="/pass_list?selection=all_time">Проходы за все время</a>
    <br>

    <hr>
    <a class="btn btn-primary" href="/person_inside">Кто находится внутри предприятия</a>
    <hr>

    <? /*
    <script>
        $(document).ready(function (){
            var setUrl = '<?=$path?>';
            String.prototype.count=function(c) {
                var result = 0, i = 0;
                for(i;i<this.length;i++)if(this[i]==c)result++;
                return result;
            };
            $("#datepicker").change(function (){
                console.log($(this).val());
                if(setUrl.count("?")>0){
                    setUrl = setUrl + "&date=" + $(this).val();
                } else {
                    setUrl = setUrl + "?date=" + $(this).val();
                }
                window.location.href = setUrl;
            });
        });
    </script>

    <div class=""><input type="text" id="datepicker" value="<?
        if(empty($datep)){
            echo date("Y-m-d");
        } else {
            echo $datep;
        }

            ?>" /> </div>

    */ ?>

    <div class="panel">

        <span class="pagination-info">Всего проходов: <?= $pass_count ?></span>
        <hr>

        <form name="filter_pass_form" action="" method="get" class="form-inline">

            <?
            $url_params = array(
                'selection' => $selection,
                'sort_by' => $sort_by,
                'dir' => $dir,
                'page' => 1
            );
            foreach ($url_params as $key => $url_param) {
                ?>
                <input name=<?= $key ?> type="hidden" value="<?= $url_param ?>">
            <? } ?>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label">Организация</label>
                        <input type="text" class="form-control" name="organization"
                               value="<?= $filter_organization ?>"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label">Персона</label>
                        <input type="text" class="form-control" name="person" value="<?= $filter_person ?>"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label">Номер документа</label>
                        <input type="text" class="form-control" name="doc_number" value="<?= $filter_doc_number ?>"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input class="form-control btn btn-warning reset_form" value="Сбросить фильтр"/>
                        <input type="submit" class="form-control btn btn-primary" value="Применить фильтр"/>
                    </div>
                </div>
            </div>
        </form>

        <hr>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                    <tr>
                        <?= $header_string ?>
                        <? /*
                    <td class=""></td>
                    <td class="<?if($sort_by=="pass_time") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=pass_time&dir=<?=$dir?>">Время прохода</a></td>
                    <td class=" <?if($sort_by=="pass_direction") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=pass_direction&dir=<?=$dir?>">Направление</a></td>
                    <td class="<?if($sort_by=="doc_number") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=doc_number&dir=<?=$dir?>">Документ</a></td>
                    <td class="<?if($sort_by=="person_id") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=person_id&dir=<?=$dir?>">Персона</a></td>
                    <td class="">Организация</td>
                    <td class="<?if($sort_by=="control_list") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=control_list&dir=<?=$dir?>">Наличие в К.С.</a></td>
                    <!--td class="">Фото</td>-->
                    <td class="<?if($sort_by=="face_verification") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=face_verification&dir=<?=$dir?>">Верификация (фото)</a></td>
                    <td class="<?if($sort_by=="fingerprint_verification") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=fingerprint_verification&dir=<?=$dir?>">Верификация (пальцы)</a></td>
                    <!--<td class="<?if($sort_by=="pass_done") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>?sort_by=pass_done">Проход разрешен</a></td> -->
                    <td class="<?if($sort_by=="pass_allowed") echo "selected_td";?>"><a class="btn-link text-semibold" href="<?=$path?>&sort_by=pass_allowed&dir=<?=$dir?>">Проход произведен</a></td>
                */ ?></tr>


                    <? $t = $show_count * ($page - 1);
                    foreach ($list as $item) {
                        //pr($item);
                        $t++ ?>
                        <tr>
                            <td class=""><?= $t ?></td>
                            <td width="184px">
                                <? if ($item["has_event"] > 0) { ?>
                                    <a class="btn-link text-semibold"
                                       href="/pass_data/<?= $item['Pass_List']['pass_id'] ?>?gate_id=<?= $gate_id ?>"><?= pass_date($item['Pass_List']['pass_time']); ?></a>
                                <? } else { ?>
                                    <?= pass_date($item['Pass_List']['pass_time']); ?>
                                <? } ?>
                            </td>
                            <td class=""><?
                                if ($item['Pass_List']['pass_direction'] == "Enter") echo 'Вход'; else echo "Выход"; ?></td>
                            <td class=""><?
                                $face_identify = "Идентификация по лицу";
                                if($item['Pass_List']['doc_type']!=$face_identify) {
                                    echo $item['Pass_List']['doc_type'] . " " . $item['Pass_List']['doc_number'] ;
                                } else {
                                    echo $face_identify;
                                }
                                ?></td>
                            <td class=""><a class="btn-link"
                                            href="person/view/<?= $item['Person']['id'] ?>"><?= $item['Person']['name'] ?></a>
                            </td>
                            <td class=""><a class="image_gallery" href="/image_pass/<?= $item['Pass_List']['id']; ?>">Фотография
                            </td>
                            <td class=""><?= $item['Person']['organization']; ?></td>
                            <td class="">
                                <? if (!empty($item['Control_List']['list_name'])) { ?><a class="btn-link"
                                                                                          href="/control_list/view/<?= $item['Control_List']['id'] ?>"><?= $item['Control_List']['list_name'] ?></a>
                                <? } else {
                                    echo "---";
                                } ?>
                            </td>

                            <? /*
                        <td class="">
                            <? if ($item['Pass_List']['photo_image'] != null){ ?>
                            <a target="_blank"
                               href="data:image/jpeg;base64,<?= $item['Pass_List']['photo_image']; ?>"><img
                                        src="data:image/jpeg;base64,<?= $item['Pass_List']['photo_image']; ?>"
                                        title="person image" style="max-width: 50px;"></a>
                        </td>
                        <?  */ ?>
                            <td class=""><?= $item['Pass_List']['fp_verification_status_name']; ?></td>
                            <td class=""><?= $item['Pass_List']['face_verification_status_name']; ?></td>

                            <td class="">
                                <? if ($item['Pass_List']['pass_allowed'] == "true") { ?>
                                    Да
                                <? } else { ?>
                                    Нет
                                <? } ?>
                            </td>
                            <? /*
                        <td class="">
                            <? if ($item['Pass_List']['pass_done'] == "true") { ?>
                                Да
                            <? } else { ?>
                                Нет
                            <? } ?>
                        </td>
                        */ ?>
                        </tr>
                    <? } ?>
                </table>
            </div>
        </div>

        <span class="pagination-info">Всего проходов: <?= $pass_count ?></span>

        <ul class="pagination">
            <?php $ctrl_pgn = $current_url;
            $params = array(
                'selection' => $selection,
                'sort_by' => $sort_by,
                'dir' => $dir,
                'organization' => $filter_organization,
                'person' => $filter_person,
                'doc_number' => $filter_doc_number,

            );
            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
        </ul>

    </div>

</div>