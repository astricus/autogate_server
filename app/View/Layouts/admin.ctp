<!DOCTYPE html>
<html lang="en">
<head>

    <?= $this->Html->charset() ?>
    <title>
        <?php echo $title; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?=site_url();?>">

    <?= $this->Html->meta('icon') ?>

    <!--Open Sans Font [ OPTIONAL ]-->

    <? //$this->Html->css('base.css') ?>
    <? //$this->Html->css('style.css') ?>
    <?= $this->Html->css('fonts.css') ?>
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <?= $this->Html->css('premium-line-icons.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.min.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?//= $this->Html->css('nifty.css') ?>

    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>

    <?= $this->Html->script('../plugins/pace/pace.min.js') ?>

    <!--Demo [ DEMONSTRATION ]-->

    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-select/bootstrap-select.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css') ?>

    <?= $this->Html->css('../plugins/chosen/chosen.min.css') ?>

    <?= $this->Html->css('../plugins/noUiSlider/nouislider.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>

    <?= $this->Html->css('../plugins/ionicons/css/ionicons.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.css') ?>
    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/morris-js/morris.min.css') ?>

    <?= $this->Html->css('admin.css') ?>

    <!--jQuery [ REQUIRED ]-->
    <?= $this->Html->script('jquery.min.js') ?>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <?= $this->Html->script('bootstrap.min.js') ?>

    <!--NiftyJS [ RECOMMENDED ]-->
    <?= $this->Html->script('nifty.min.js') ?>


    <!--Flot Chart [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.resize.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.tooltip.min.js') ?>

    <?= $this->Html->script('../plugins/select2/js/select2.js') ?>

    <?= $this->Html->script('../plugins/masked-input/jquery.maskedinput.js') ?>

    <!--Sparkline [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/sparkline/jquery.sparkline.min.js') ?>

    <?= $this->Html->script('../plugins/bootstrap-datepicker/bootstrap-datepicker.js') ?>


    <?= $this->Html->script('index.js') ?>

    <? //$this->Html->script('calls.js') ?>

    <? $this->Html->script('products.js') ?>

    <? //$this->Html->script('users.js') ?>

    <? //$this->Html->script('objects.js') ?>

    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/morris-js/morris.min.js') ?>
    <?= $this->Html->script('../plugins/morris-js/raphael-js/raphael.min.js') ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <!-- DX CHART -->
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.6/css/dx.common.css" />
    <?= $this->Html->css('dx_light.css') ?>
     <?= $this->Html->script('dx.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>

<body>
<div id="container" class="cls-container">

    <div id="bg-overlay"></div>

    <div class="cls-content">
        <div class="cls-content-sm panel">
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <h1 class="h3">Вход в систему управления</h1>
                    <p>Требуется авторизация</p>
                </div>

                <form action="/operator/login" method="post">
                    <div class="form-group <?if(isset($auth_error_text)) echo "has-error";?>">
                        <input type="text" class="form-control" placeholder="login" name="login" autofocus>
                    </div>
                    <div class="form-group <?if(isset($auth_error)) echo "has-error";?>">
                        <input type="password" class="form-control" placeholder="пароль" name="password">
                    </div>

                    <?if(isset($auth_error_text)) echo L($auth_error_text) . "<br><br>";?>

                    <button class="btn btn-primary btn-lg btn-block" type="submit">Вход</button>
                </form>
            </div>

        </div>
    </div>

</div>

<!--jQuery [ REQUIRED ]-->
<script src="js/jquery.min.js"></script>


<!--BootstrapJS [ RECOMMENDED ]-->
<script src="js/bootstrap.min.js"></script>


<!--NiftyJS [ RECOMMENDED ]-->
<script src="js/nifty.min.js"></script>

</body>
</html>