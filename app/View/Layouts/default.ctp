<?php
$active_page = "start";
$panel_name = "Автоматическая проходная: Панель управления";
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <?= $this->Html->charset() ?>
    <title>
        Автоматическая проходная
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?= site_url(); ?>">

    <?= $this->Html->meta('icon') ?>

    <!--Open Sans Font [ OPTIONAL ]-->
    <?= $this->Html->css('fonts.css') ?>

    <? //$this->Html->css('base.css') ?>
    <?= $this->Html->css('fa.all.min') ?>

    <!--CSS Loaders [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/css-loaders/css/css-loaders') ?>

    <!--Animate.css [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/animate-css/animate.min.css') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <?= $this->Html->css('premium-line-icons.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.min.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <? //= $this->Html->css('nifty.css') ?>

    <!--Unite Gallery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/unitegallery/css/unitegallery.min.css') ?>

    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>

    <?= $this->Html->script('../plugins/pace/pace.min.js') ?>

    <!--Demo [ DEMONSTRATION ]-->

    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-select/bootstrap-select.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css') ?>

    <?= $this->Html->css('../plugins/chosen/chosen.min.css') ?>

    <?= $this->Html->css('../plugins/noUiSlider/nouislider.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>

    <?= $this->Html->css('../plugins/ionicons/css/ionicons.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.css') ?>
    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/morris-js/morris.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-validator/bootstrapValidator.min.css') ?>
    <?= $this->Html->css('admin.css') ?>

    <?= $this->Html->css('interface.css') ?>
    <?= $this->Html->css('chat.css') ?>

    <!--jQuery [ REQUIRED ]-->
    <?= $this->Html->script('jquery.min.js') ?>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <?= $this->Html->script('bootstrap.min.js') ?>

    <!--NiftyJS [ RECOMMENDED ]-->
    <?= $this->Html->script('nifty.min.js') ?>

    <!--Flot Chart [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.resize.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.tooltip.min.js') ?>

    <?= $this->Html->script('../plugins/select2/js/select2.min.js') ?>

    <?= $this->Html->script('../plugins/masked-input/jquery.maskedinput.js') ?>

    <!--Sparkline [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/sparkline/jquery.sparkline.min.js') ?>

    <?= $this->Html->script('../plugins/bootstrap-datepicker/bootstrap-datepicker.js') ?>

    <?= $this->Html->script('../plugins/chosen/chosen.jquery.js') ?>

    <?= $this->Html->script('../plugins/unitegallery/js/unitegallery.min') ?>
    <?= $this->Html->script('../plugins/unitegallery/themes/tilesgrid/ug-theme-tilesgrid') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/switchery/switchery.min.js') ?>

    <!--Bootbox Modals [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/bootbox/bootbox.min.js') ?>
    <!--Modals [ SAMPLE ]-->
    <? /*
    <script src="js/demo/ui-modals.js"></script>
 */ ?>
    <?= $this->Html->script('notify.js') ?>
    <?= $this->Html->script('index.js') ?>

    <?= $this->Html->script('alert_messages.js') ?>
    <?= $this->Html->script('operators.js') ?>
    <?= $this->Html->script('control_list.js') ?>
    <?= $this->Html->script('persons.js') ?>

    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/morris-js/morris.min.js') ?>
    <?= $this->Html->script('../plugins/morris-js/raphael-js/raphael.min.js') ?>
    <?= $this->Html->css('fa_new.css') ?>

    <!-- DX CHART -->
    <?= $this->Html->css('css_dx.css') ?>
    <?= $this->Html->css('dx_light.css') ?>
    <?= $this->Html->script('dx.js') ?>

    <?= $this->Html->css('base_jquery_ui') ?>
    <?= $this->Html->script('base_jquery.js') ?>
    <?= $this->Html->script('gallery.js') ?>
    <?= $this->Html->script('app.js') ?>
    <script>

        /* Локализация datepicker */

        $.datepicker.regional['ru'] = {

            closeText: 'Закрыть',

            prevText: 'Предыдущий',

            nextText: 'Следующий',

            currentText: 'Сегодня',

            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],

            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],

            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],

            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],

            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],

            weekHeader: 'Не',

            dateFormat: 'dd.mm.yy',

            firstDay: 1,

            isRTL: false,

            showMonthAfterYear: false,

            yearSuffix: ''

        };

        $.datepicker.setDefaults($.datepicker.regional['ru']);


        $(document).ready(function () {
            $(function () {
                $("#datepicker").datepicker({
                    showAnim: "slideDown",
                    dateFormat: "yy-mm-dd"
                });

            });
        });

    </script>

    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/styles/default.min.css">
    <?= $this->Html->script('highlight.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>

<body>
<div id="container"
     class="effect aside-float aside-bright mainnav-lg">


    <header id="navbar">
        <div id="navbar-container" class="boxed">

            <div class="navbar-header">
                <a href="/" class="navbar-brand">
                    <div class="brand-title">
                        <span class="brand-text">AUTOGATE&nbsp; Dev Panel</span>
                    </div>
                </a>
            </div>

            <div class="navbar-content">

                <ul class="nav navbar-top-links">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="pli-list-view"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-top-links">

                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                            <span class="ic-user pull-right">
                                <i class="pli-male"></i>
                            </span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right panel-default">
                            <ul class="head-list">
                                <li>
                                    <a href="/logout"><i class="pli-unlock icon-lg icon-fw"></i>Выход</a>
                                </li>
                            </ul>
                        </div>
                    </li>


                </ul>
            </div>

        </div>
    </header>

    <div class="boxed">

        <div id="content-container">
            <div id="page-head">

                <div class="pad-all text-center">
                    <h3><?= $panel_name ?></h3>
                </div>
            </div>

            <div id="page-content">

                <div class="row">
                    <!-- CAKE CONTENT -->
                    <?= $this->Flash->render() ?>
                    <div class="clearfix">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">АРМ Бюро пропусков</h3>
                            </div>
                            <div class="panel-body">
                                <?= $this->fetch('content') ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        <nav id="mainnav-container">
            <div id="mainnav">

                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">

                            <ul id="mainnav-menu" class="list-group">

                                <li class="list-header">Разделы</li>






                                <? if ($role != "security") { ?>

                                    <li <?= is_active_page($active_page, "persons", "active-link") ?>>

                                        <a href="/persons" data-original-title="" title="">
                                            <i class="fas fa-user"></i>
                                            <span class="menu-title">Персоны</span>
                                        </a>
                                    </li>

                                    <li <?= is_active_page($active_page, "orders", "active-link") ?>>

                                        <a href="/control_lists" data-original-title="" title="">
                                            <i class="fas fa-list-alt"></i>
                                            <span class="menu-title">Контрольные списки</span>
                                        </a>
                                    </li>

                                    <li <?= is_active_page($active_page, "companies", "active-link") ?>>

                                        <a href="/pass_list" data-original-title="" title="">
                                            <i class="fas fa-address-book"></i>
                                            <span class="menu-title">Проходы</span>
                                        </a>
                                    </li>

                                    <li <?= is_active_page($active_page, "operators", "active-link") ?>>
                                        <a href="/operators" data-original-title="" title="">
                                            <i class="fa fa-eye"></i>
                                            <span class="menu-title">Администраторы</span>
                                        </a>
                                    </li>

                                    <li <?= is_active_page($active_page, "group", "active-link") ?>>

                                        <a href="/group" data-original-title="" title="">
                                            <i class="fa fa-door-closed"></i>
                                            <span class="menu-title">Автогейты</span>
                                        </a>
                                    </li>


                                    <?/*

                                    <li <?= is_active_page($active_page, "settings", "active-link") ?>>
                                        <a href="/settings" data-original-title="" title="">
                                            <i class="fa fa-cogs"></i>
                                            <span class="menu-title">Настройки системы</span>
                                        </a>
                                    </li>

                                */?>

                                <? } ?>

                            </ul>

                            <div class="mainnav-widget">

                                <div class="show-small">
                                    <a href="#" data-toggle="menu-widget" data-target="#wg-server">
                                        <i class="pli-monitor-2"></i>
                                    </a>
                                </div>

                                <div id="wg-server" class="hide-small mainnav-widget-content">
                                    <ul class="list-group">
                                        <!-- раздел статистики в меню админки, добавить пункты по необходимости -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <footer id="footer">


        <div class="show-fixed pad-rgt pull-right">
            You have <a href="#" class="text-main"><span class="badge badge-danger">3</span> pending action.</a>
        </div>
        <p class="pad-lft">Система управления Autogate</p>

    </footer>

    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
</div>

<div class="modal fade in" id="call_order_modal" role="dialog" tabindex="-1" aria-labelledby="call_order_modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Modal Heading</h4>
            </div>

            <!--Modal body-->
            <div class="modal-body"></div>

            <!--Modal footer-->
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="__gallery">
    <div class="__gallery_image"></div>
    <a class="__gallery_close btn btn-primary"><i class="pci-cross pci-circle"></i></a>
</div>

</body>
</html>