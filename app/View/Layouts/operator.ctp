<!DOCTYPE html>
<html lang="en">
<head>

    <?= $this->Html->charset() ?>
    <title>
        Добро пожаловать!
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?= site_url(); ?>">

    <?= $this->Html->meta('icon') ?>

    <!--Open Sans Font [ OPTIONAL ]-->
    <?= $this->Html->css('fonts.css') ?>

    <?= $this->Html->css('fa.all.min') ?>

    <!--CSS Loaders [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/css-loaders/css/css-loaders') ?>

    <!--Animate.css [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/animate-css/animate.min.css') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <?= $this->Html->css('premium-line-icons.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.min.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <? //= $this->Html->css('nifty.css') ?>

    <!--Unite Gallery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/unitegallery/css/unitegallery.min.css') ?>

    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>

    <?= $this->Html->script('../plugins/pace/pace.min.js') ?>

    <!--Demo [ DEMONSTRATION ]-->

    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-select/bootstrap-select.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css') ?>

    <?= $this->Html->css('../plugins/chosen/chosen.min.css') ?>

    <?= $this->Html->css('../plugins/noUiSlider/nouislider.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>

    <?= $this->Html->css('../plugins/ionicons/css/ionicons.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.css') ?>
    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/morris-js/morris.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-validator/bootstrapValidator.min.css') ?>
    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->css('interface.css?x=' . rand(1, 10000000)) ?>

    <!--jQuery [ REQUIRED ]-->
    <?= $this->Html->script('jquery.min.js') ?>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <?= $this->Html->script('bootstrap.min.js') ?>

    <!--NiftyJS [ RECOMMENDED ]-->
    <?= $this->Html->script('nifty.min.js') ?>

    <!--Flot Chart [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.resize.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.tooltip.min.js') ?>

    <?= $this->Html->script('../plugins/select2/js/select2.min.js') ?>

    <?= $this->Html->script('../plugins/masked-input/jquery.maskedinput.js') ?>

    <!--Sparkline [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/sparkline/jquery.sparkline.min.js') ?>

    <?= $this->Html->script('../plugins/bootstrap-datepicker/bootstrap-datepicker.js') ?>

    <?= $this->Html->script('../plugins/chosen/chosen.jquery.js') ?>

    <?= $this->Html->script('../plugins/unitegallery/js/unitegallery.min') ?>
    <?= $this->Html->script('../plugins/unitegallery/themes/tilesgrid/ug-theme-tilesgrid') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/switchery/switchery.min.js') ?>

    <!--Bootbox Modals [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/bootbox/bootbox.min.js') ?>
    <!--Modals [ SAMPLE ]-->
    <? /*
    <script src="js/demo/ui-modals.js"></script>
 */ ?>
    <?= $this->Html->script('notify') ?>
    <?= $this->Html->script('user_autogate') ?>
    <? //$this->Html->script('camera') ?>


    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/morris-js/morris.min.js') ?>
    <?= $this->Html->script('../plugins/morris-js/raphael-js/raphael.min.js') ?>
    <?= $this->Html->css('fa_new.css') ?>
    <!-- DX CHART -->
    <?= $this->Html->css('css_dx.css') ?>
    <?= $this->Html->css('dx_light.css') ?>
    <?= $this->Html->script('dx.js') ?>
    <?= $this->Html->css('base_jquery_ui.css') ?>
    <?= $this->Html->script('base_jquery.js') ?>
    <script>

        /* Локализация datepicker */

        $.datepicker.regional['ru'] = {

            closeText: 'Закрыть',

            prevText: 'Предыдущий',

            nextText: 'Следующий',

            currentText: 'Сегодня',

            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],

            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],

            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],

            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],

            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],

            weekHeader: 'Не',

            dateFormat: 'dd.mm.yy',

            firstDay: 1,

            isRTL: false,

            showMonthAfterYear: false,

            yearSuffix: ''

        };

        $.datepicker.setDefaults($.datepicker.regional['ru']);


        $(document).ready(function () {
            $(function () {
                $("#datepicker").datepicker({
                    showAnim: "slideDown",
                    dateFormat: "yy-mm-dd"
                });

            });
        });

    </script>

    <?= $this->Html->css('highlight.css') ?>
    <?= $this->Html->script('highlight.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>

<body>
<a href="/" class="btn btn-lg btn-success comeback_to_admin">Вернуться к административной панели</a>
<div id="container" class="cls-container">
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</div>
</body>

<a class="document_image_preview">
    <div class="alert-danger alert">Использован сомнительный документ!</div>
</a>

<div class="doubtful_document">
    <div class="document_image_container"></div>
</div>

<div class="last_pass_block"></div>

<div class="control_list_message"></div>


<div class="modal_box">

    <div class="content_modal">

        <div class="user_check_success user_check_popup">Проверка данных пользователя успешно пройдена!</div>

        <div class="user_check_failure user_check_popup">Верификация пользователя не пройдена!</div>

        <div class="user_check_document_alert user_check_popup">Документ не действителен или подделка!
        </div>

        <div class="user_access_denied_message user_access_message">Проход запрещен!</div>

        <div class="user_access_granted_message user_access_message">Проход разрешен!</div>

        <div class="row">
            <div class="col-md-6 text-center">
                <img alt="Profile Picture" class="img_modal_ mar-ver user_photo_from_db_field" src="/images/user.svg">
            </div>
            <div class="col-md-6 text-center">
                <img alt="Profile Picture" class="img_modal_ mar-ver user_live_photo_field" src="/images/user.svg">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-left text-bold big_text">
                <table class="table_info">
                    <tr>
                        <td class="user_name_field"></td>
                        <td class="user_organization_field"></td>
                    </tr>
                    <tr>
                        <td class="user_division_field"></td>
                        <td class="user_subdivision_field"></td>
                    </tr>
                    <tr>
                        <td class="user_position_field"></td>
                        <td class="user_document_field"></td>
                    </tr>
                </table>
                <!-- Документы в системе: Паспорт 5005 909595, карта скуд №8374 090394 -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 text-center">
                <a class="btn btn-success  btn-lg force_access_granted">Разрешить проход</a>
            </div>
            <div class="col-md-6 text-center">
                <button class="btn btn-danger  btn-lg force_access_denied">Запретить проход</button>
            </div>
        </div>
        <div class="row verify_messages">

        </div>

    </div>
</div>

</html>