<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Добавление оператора / администратора</h1>
        </div>

        <form action="/operator/add" method="post" class="form-horizontal add_operator">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Имя*" name="firstname" required>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Фамилия*" name="lastname" required value="">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Отчество" name="middlename" value="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Логин*" name="login" required value="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-1">Роль в системе</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="role">
                                <option value="admin">
                                    Администратор
                                </option>
                                <option value="security">
                                    Охранник
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-8"></div>
                </div>

                <hr>
                Поля, обозначенные * обязательны для заполнения

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-2">
                        <button class="btn btn-primary btn-block add_operator_button">Добавить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/operators">Отмена</a>
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>


