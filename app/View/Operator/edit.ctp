<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Редактирование оператора <i><?= prepare_fio($operator['lastname'], $operator['firstname'], $operator['middlename'])?></i></h1>
        </div>

        <form action="" method="post" class="form-horizontal edit_operator" data-operator_id="<?= $operator['id'] ?>">
            <input type="hidden" name="operator_id" value="<?= $operator['id'] ?>">
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Имя *</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Имя" name="firstname"
                               autofocus value="<?= $operator['firstname'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Фамилия *</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Фамилия"
                               name="lastname" autofocus value="<?= $operator['lastname'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Отчество</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Отчество"
                               name="middlename" value="<?= $operator['middlename'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Лoгин *</label>
                        <input type="text" class="form-control col-sm-6" placeholder="Лoгин"
                               name="login" value="<?= $operator['login'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Роль в системе</label>
                        <select class="form-control col-sm-6" name="role">
                            <option value="admin" <? if ($operator['role'] == 'admin') echo "selected"; ?>>
                                Администратор
                            </option>
                            <option value="security" <? if ($operator['role'] == 'security') echo "selected"; ?>>
                                Охранник
                            </option>
                        </select>
                    </div>
                </div>

                <hr>
                Поля, обозначенные * обязательны для заполнения

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4">
                        <button class="btn btn-primary btn-block edit_operator_button" type="submit">Сохранить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/operators">Отмена</a>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>


