<div id="page-content">

    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Список операторов/администраторов</h3>
                    </div>
                    <div class="panel-body reset_result">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>ФИО</th>
                                    <th>Логин</th>
                                    <th>Роль</th>
                                    <th>Статус</th>
                                    <th>Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $cur_operator_id = $operator_id;

                                $n = 0;
                                foreach ($operators as $operator) {
                                    $enabled = "";
                                    $n++;
                                    $operator = $operator['Operator'];
                                    $pass_hash = $operator['password'];
                                    $operator_id = $operator['id'];
                                    if($operator['enabled'] == "true"){
                                        $enabled = "Разрешен";
                                    } else {
                                        $enabled = "Запрещен";
                                    }
                                    $operator_name = prepare_fio_short($operator['lastname'], $operator['firstname'], $operator['middlename']);
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><a class="btn-link text-semibold" href="operator/edit/<?=$operator_id?>"> <?= $operator_name ?></a></td>
                                        <td><?= $operator['login'] ?></td>
                                        <td><? if($operator['role'] == "admin"){
                                                echo "Администратор";
                                            } else echo "Охранник";?>
                                        </td>
                                        <td><? if($operator['enabled'] == "true"){
                                                echo "Активен";
                                            } else echo "Заблокирован";?></td>

                                        <th>
                                            <?if($operator['enabled'] ){?>
                                                <a class="btn btn-warning modal_action" href="/operator/set_status/<?=$operator_id?>?status=false&id=">Запретить</a>
                                            <?} else {?>
                                                <a class="btn btn-success modal_action" href="/operator/set_status/<?=$operator_id?>?status=true&id=">Разрешить</a>
                                            <?}?>
                                            <a class="btn btn-mint" href="/operator/edit/<?=$operator_id?>">Изменить</a>
                                            <?if($cur_operator_id!=$operator_id && !empty($pass_hash)){?>
                                            <a class="btn btn-danger reset_password_button " data-operator_name="<?=$operator_name?>" data-operator_id="<?=$operator_id?>">
                                                Сбросить пароль</a>
                                            <?}?>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                        <a href="/operator/add" class="btn btn-success btn-bock">Добавить</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>