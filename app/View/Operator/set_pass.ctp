<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Установить пароль</h1>
        </div>

        <?
        $auth_error_arr["PASS1_NOT_EQ_PASS2"] = "Пароль 1 не совпадает с паролем 2";
        $auth_error_arr["PASS1_NOT_VALID"] = "Пароль не подходит. Требуется пароль от 8 до 20 символов и быть достаточно сложным";
        if(isset($auth_error_text)) echo ($auth_error_arr[$auth_error_text]) . "<br><br>";?>

        <form action="/operator/set_pass" method="post" class="form-horizontal set_pass">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group <?if(isset($auth_error)) echo "has-error";?>">
                            <input type="text" class="form-control" placeholder="Пароль" name="password1" required>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group <?if(isset($auth_error)) echo "has-error";?>">
                            <input type="text" class="form-control" placeholder="Повторите пароль" name="password2" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-2 col-sm-offset-2">
                        <button class="btn btn-primary btn-block">Сохранить</button>
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>


