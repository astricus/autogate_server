<div id="page-content">

    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Список персон</h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Персона</th>
                                    <th>Организация</th>
                                    <th>Дата добавления</th>
                                    <th>Дата добавления</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($person_content['info'] as $person_data) {
                                    $n++;
                                    $doc_number = " no data";
                                    $person = $person_data['Person'];
                                    $PersonID = $person['id'];
                                    $person_name = prepare_fio_short($person['lastname'], $person['firstname'], $person['middlename']);
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><a class="btn-link text-semibold" href="person/<?=$PersonID?>"> <?= $person_name ?></a></td>
                                        <td><?= $doc_number ?></td>
                                        <td><?= $person['organization'] ?></td>
                                        <td><span class="text-info"><i
                                                        class="pli-clock"></i><?= lang_calendar($person['created']) ?></span>
                                        </td>
                                        <th>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                        <a class="btn btn-success" href="/person/<?=$person['id']?>/add_document/">Добавить документ</a>

                        <h3 class="panel-title">Список документов</h3>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Документ</th>
                                    <th>Верификация</th>
                                    <th>Статус</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($person_content['docs'] as $person_data) {
                                    $enabled = "";
                                    $n++;
                                    $doc_number = " no data";

                                    $enabled_status = $person_data['Document']['enabled'];
                                    if($enabled_status=="true"){
                                        $enabled = "Разрешен";
                                    } else {
                                        $enabled = "Запрещен";
                                    }
                                    $document_id = $person_data['Document']['id'];
                                    $doc_number = $person_data['Document']['doc_number'];
                                    $doc_type = $person_data['Document']['doc_type'];
                                    $doc_number = "<b>" . $doc_number . " " .  $doc_type . "</b>";

                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><?= $doc_number ?></td>
                                        <td><?= $person_data['Document']['verification'] ?></td>
                                        <td>
                                        <?if($enabled_status){?>
                                            <a class="btn btn-danger" href="/update_person/set_status?status=false&id=<?=$person_data['Document']['id']?>">Запретить</a>
                                        <?} else {?>
                                            <a class="btn btn-success" href="/update_person/set_status?status=true&id=<?=$person_data['Document']['id']?>">Разрешить</a>
                                        <?}?>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                        <h3 class="panel-title">Список проходов</h3>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Автогейт</th>
                                    <th>Направление</th>
                                    <th>Фото</th>
                                    <th>Верификация по камере</th>
                                    <th>Наличие в контрольных списках</th>
                                    <th>Был ли разрешен проход</th>
                                    <th>Был ли завершен проход</th>
                                    <th>Время прохода (pass time)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($person_content['pass_list'] as $pass) {
                                    $n++;
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><?= $pass['Pass_List']['gate_id'] ?></a></td>
                                        <td><?= $pass['Pass_List']['pass_direction'] ?></a></td>
                                        <td><?= $pass['Pass_List']['photo'] ?></a></td>
                                        <td><?= $pass['Pass_List']['camera_verification'] ?></a></td>
                                        <td><?= $pass['Pass_List']['control_list'] ?></a></td>
                                        <td><?= $pass['Pass_List']['pass_allowed'] ?></a></td>
                                        <td><?= $pass['Pass_List']['pass_done'] ?></a></td>
                                        <td><span class="text-info"><i class="pli-clock"></i><?= lang_calendar($pass['Pass_List']['pass_time']) ?></span></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>