<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Добавление документа  для <?=$person_name?></h1>
        </div>

        <?= $this->Html->script('pass_validate.js') ?>
        <form action="/person/<?=$person_id?>/add_document" method="post" class="form-horizontal">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4 ">
                        Тип документа
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <select  class="form-control" name="doc_type">
                                <? foreach ($doc_types as $key => $doc_type){
                                    if($key=="FACE") continue;
                                    ?>
                                    <option value="<?=$key?>"><?=$doc_type?></option>
                                <?  }?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4 ">
                        Номер документа
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Номер документа" name="doc_number" autofocus required>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4 ">
                        Верификация
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">

                            <select class="form-control" name="verification_type">
                                <option value="No">Нет</option>
                                <option value="Face">Лицо</option>
                                <option value="All">Лицо и отпечатки пальцев</option>
                                <option value="Fingerprint">Отпечатки пальцев</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4 ">
                        Cтатус
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <select name="enabled" class="form-control">
                                <option value="true">Активен</option>
                                <option value="false">Заблокирован</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-2 col-sm-offset-6">
                        <button class="btn btn-primary btn-block" type="submit">Добавить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/person/view/<?= $person_id ?>">Отмена</a>
                    </div>
                </div>
            </div>



        </form>
    </div>
</div>


