<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Добавление персоны</h1>
        </div>
        <?= $this->Html->script('pass_validate.js') ?>
        <form action="/add_person" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="row">
                    <label class="control-label col-sm-3">Имя *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Имя" name="firstname" autofocus required value="<?=$form_firstname?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Фамилия *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Фамилия" name="lastname" required value="<?=$form_lastname?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Отчество</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Отчество" name="middlename" value="<?=$form_middlename?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>


                <div class="row">
                    <label class="control-label col-sm-3">Номер документа *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Номер документа" name="doc_number" required value="<?=$form_doc_number?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Тип документа *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select class="form-control" name="doc_type" required>
                                <? foreach ($doc_types as $key => $doc_type) {
                                    ?>
                                    <option value="<?= $key ?>"><?= $doc_type ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <label class="control-label col-sm-3">Организация *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Организация" name="organization" required value="<?=$form_organization?>">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Отделение</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Отделение" name="division" value="<?=$form_division?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Подразделение</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Подразделение" name="subdivision"  value="<?=$form_subdivision?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3">Должность  *</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Должность" name="position" required value="<?=$form_position?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <label for="verification_type"> Верификация документа *</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="verification_type" id="verification_type" class="form-control">
                                <option value="No">Нет</option>
                                <option value="Face">По лицу</option>
                                <option value="Fingerprint">Отпечатки пальцев</option>
                                <option value="All">Лицо и отпечатки пальцев</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <label class="control-label col-sm-3">Фотография персоны</label>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="file" class="form-control" name="image" style="height: 38px">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-2">
                        <button class="btn btn-primary btn-block" type="submit">Добавить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/persons">Отмена</a>
                    </div>
                </div>

                <hr>
                Поля, обозначенные * обязательны для заполнения

        </form>
    </div>
</div>