<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Редактирование документа для персоны <?= $person_name ?></h1>
        </div>

        <?= $this->Html->script('pass_validate.js') ?>

        <form action="" method="post" class="form-horizontal edit_document">
            <input type="hidden" name="id" value="<?= $document['id'] ?>" class="document_id"
                   data-document_id="<?= $document['id'] ?>">
            <input type="hidden" name="person_id" value="<?= $person_id ?>">
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Тип документа</label>
                        <select class="form-control col-sm-6" name="doc_type">
                            <? if ($document['doc_type'] != "FACE") { ?>
                                <? foreach ($doc_types as $key => $doc_type) {
                                    if($key=="FACE") continue;?>
                                    <option value="<?= $key ?>" <? if ($document['doc_type'] == $key) echo "selected"; ?>>
                                        <?= $doc_type ?>
                                    </option>
                                <? }
                            } else { ?>
                                <option value="FACE"><?= $doc_types['FACE'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <? if ($document['doc_type'] != "FACE") {//Идентификация)?>


                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Номер документа</label>
                            <input type="text" class="form-control col-sm-6" placeholder="Номер документа"
                                   name="doc_number" autofocus value="<?= $document['doc_number'] ?>">
                        </div>
                    </div>

                <? } ?>

                <div class="row">

                    <div class="form-group">
                        <label class="control-label col-sm-3">Верификация</label>
                        <select class="form-control col-sm-6" name="verification">
                            <? if ($document['doc_type'] != "FACE") { ?>
                                <option value="No" <? if ($document['verification'] == "No") echo "selected"; ?>>Нет
                                </option>
                                <option value="Face" <? if ($document['verification'] == "Face") echo "selected"; ?>>
                                    Лицо
                                </option>
                                <option value="All" <? if ($document['verification'] == "All") echo "selected"; ?>>Лицо
                                    и отпечатки пальцев
                                </option>
                                <option value="Fingerprint" <? if ($document['verification'] == "Fingerprint") echo "selected"; ?>>
                                    Отпечатки пальцев
                                </option>
                            <? } else { ?>
                                <option value="Face" <? if ($document['verification'] == "Face") echo "selected"; ?>>
                                    Лицо
                                </option>
                                <option value="All" <? if ($document['verification'] == "All") echo "selected"; ?>>Лицо
                                    и отпечатки пальцев
                                </option>

                            <? } ?>
                        </select>
                    </div>


                </div>

                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4">
                        <button class="btn btn-primary btn-block edit_document_button" type="submit">Сохранить</button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default btn-block" href="/person/view/<?= $person_id ?>">Отмена</a>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>


