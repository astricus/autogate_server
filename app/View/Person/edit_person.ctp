<div class="panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Редактирование персоны <i><?= prepare_fio($person['lastname'], $person['firstname'], $person['middlename']) ?></i></h1>
        </div>

        <div class="row">
            <div class="col-sm-3">

                Фотография персоны <br/>
                <? if (!empty($person_image)) { ?>
                    <a href="<?= $person_image ?>"><img alt="" src="<?= $person_image ?>" title="person image" style="width: 200px"/></a>
                <? } ?>
            </div>
            <div class="col-sm-9">
                <form action="" method="post" class="form-horizontal edit_person" enctype="multipart/form-data">
                    <input type="hidden" name="person_id" value="<?= $person['id'] ?>" class="person_id"
                           data-person_id="<?= $person['id'] ?>">

                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Имя *</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Имя" name="firstname"
                                       autofocus value="<?= $person['firstname'] ?>" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Фамилия *</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Фамилия"
                                       name="lastname" value="<?= $person['lastname'] ?>" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Отчество</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Отчество"
                                       name="middlename" value="<?= $person['middlename'] ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Организация *</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Организация"
                                       name="organization" value="<?= h($person['organization']) ?>" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Подразделение</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Подразделение"
                                       name="division" value="<?= h($person['division']) ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Отдел</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Отдел"
                                       name="subdivision" value="<?= h($person['subdivision']) ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Должность *</label>
                                <input type="text" class="form-control col-sm-6" placeholder="Должность"
                                       name="position" value="<?= h($person['position']) ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Фотография персоны</label>

                                <input type="file" class="form-control col-sm-6" name="image" id="person_image" style="height: 38px">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2 col-sm-offset-4">
                                <button class="btn btn-primary btn-block update_person_button" type="submit">Сохранить </button>
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-default btn-block" onclick="history.back(); return false;">Отмена</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <hr>
        Поля, обозначенные * обязательны для заполнения

    </div>
</div>


