<div id="page-content">

    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Персоны внутри предприятия</h3>
                    </div>
                    <div class="panel-body">

                        <?if(count($person_inside)>0){?>

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Персона</th>
                                    <th>Время прохода</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($person_inside as $person) {
                                    $n++;
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><a class="btn-link text-semibold" href="person/view/<?=$person['id']?>"> <?= $person['name'] ?></a></td>
                                        <td><span class="text-info"><i class="pli-clock"></i><?=lang_calendar($person['time']);?></span>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                        <?} else {?>
                            внутри никого нет
                        <?}?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>