<div id="page-content">


    <style>
        .person_image_icon img {
            max-width: 25px;
            max-height: 25px;

        }
        .person_image_icon {
            position: relative;
            top: 35px;
            left: 0px;
            width: 35px;
            height: 35px;
            display: block;
            background: rgba(255, 255, 255, 0.5);
            padding: 4px;
            text-align: center;
        }
    </style>
    <div class="row">
        <div class="panel">
            <div class="panel-body">


                <div class="row">
                    <div class="col-sm-3">

                        <?
                        if ($person_image_params != null) {
                            $icon = "";
                            if ($person_image_params['processed'] == true && $person_image_params['templated'] == false) {
                                $icon = '<div class="person_image_icon"><IMG src="/images/2_red.png"></div>';
                            } else if ($person_image_params['processed'] == true && $person_image_params['templated'] == true) {
                                $icon = '<div class="person_image_icon"><IMG src="/images/2_green.png"></div>';
                            }
                            echo $icon;
                        }
                        ?>
                        <img src="<?= $person_image_link ?>" title="person_image" alt="" style="max-width: 200px"
                             class="person_image_view">
                    </div>

                </div>
                <div class="col-sm-9">

                    Управление:
                    <?
                    foreach ($person_content['info'] as $person_data) {
                        //pr($person_data);
                        $person_status = $person_data['Person']['blocked'];
                        $doc_number = " нет данных ";
                        $person = $person_data['Person'];
                        $PersonID = $person['id'];
                        $person_name = prepare_fio($person['lastname'], $person['firstname'], $person['middlename']);
                        ?>
                        <a class="btn btn-success" href="/person/<?= $person['id'] ?>/add_document/">Добавить
                            документ</a>
                        <? if (!$person_status) { ?>
                            <a class="btn btn-warning modal_action"
                               href="/person/<?= $PersonID ?>/set_status/?status=true">Запретить доступ</a>
                        <? } else { ?>
                            <a class="btn btn-success modal_action"
                               href="/person/<?= $PersonID ?>/set_status/?status=false">Разрешить доступ</a>
                        <? } ?>

                        <? if ($can_add_verify_doc) { ?>

                            <a class="btn btn-dark" href="/person/add_face_verify/<?= $PersonID ?>">Активировать
                                идентификацию по лицу</a>
                        <? } ?>
                        <a class="btn btn-mint" href="/person/edit/<?= $PersonID ?>">Изменить данные</a>
                        <a class="btn btn-danger modal_action"
                           href="/person/delete/<?= $PersonID ?>">Удалить персону</a>

                    <? } ?>

                    <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Персона</th>
                                <th>Организация</th>
                                <th>Дата добавления</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            foreach ($person_content['info'] as $person_data) {
                                //pr($person_data);
                                $person_status = $person_data['Person']['blocked'];
                                $doc_number = " нет данных ";
                                $person = $person_data['Person'];
                                $PersonID = $person['id'];
                                $person_name = prepare_fio($person['lastname'], $person['firstname'], $person['middlename']);
                                ?>
                                <tr class="">
                                    <td><?= $person_name ?></td>
                                    <td><?= $person['organization'] ?></td>
                                    <td>
                                        <span class="text-info"><?= lang_calendar($person['created']) ?></span>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>


                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Подразделение</th>
                                <th>Отдел</th>
                                <th>Должность</th>
                                <th>Согласие с условиями обработки п.д.</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?

                            foreach ($person_content['info'] as $person_data) {
                                $doc_number = " no data";
                                $person = $person_data['Person'];
                                $PersonID = $person['id'];
                                $person_name = prepare_fio($person['lastname'], $person['firstname'], $person['middlename']);
                                ?>
                                <tr class="">
                                    <td><?= $person['division']; ?></td>
                                    <td><?= $person['subdivision'] ?></td>
                                    <td><?= $person['position'] ?></td>
                                    <td><? if ($person['pdn_agree'] == "true") echo "согласен"; else echo "нe согласен"; ?></td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <hr>


            <h3 class="panel-title">Список документов</h3>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Документ</th>
                        <th>Верификация</th>
                        <th>Статус</th>
                        <th>Управление документом</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $n = 0;
                    foreach ($person_content['docs'] as $person_data) {
                        $enabled = "";
                        $n++;
                        $doc_number = " no data";

                        $enabled_status = $person_data['Document']['enabled'];
                        if ($enabled_status == "true") {
                            $enabled = "Разрешен";
                        } else {
                            $enabled = "Запрещен";
                        }
                        $document_id = $person_data['Document']['id'];
                        $doc_number = $person_data['Document']['doc_number'];
                        $doc_type_name = $doc_type[$person_data['Document']['doc_type']] ?? "тип документа не определен";

                        if ($person_data['Document']['doc_type'] == "PN_RUS") {
                            $doc_number = "<b>" . $doc_type_name . " " . substr($doc_number, 0, 4) . " " . substr($doc_number, 4) . "</b>";
                        } else {
                            $doc_number = "<b>" . $doc_type_name . " " . $doc_number . "</b>";
                        }

                        ?>
                        <tr class="">
                            <td><?= $n ?></td>
                            <td><?= $doc_number ?></td>
                            <td><?= $verification_type[$person_data['Document']['verification']] ?></td>
                            <td>
                                <? if ($enabled_status) { ?>
                                    Разрешен
                                <? } else { ?>
                                    Запрещен
                                <? } ?>
                            </td>

                            <td>
                                <? if ($enabled_status) { ?>
                                    <a class="btn btn-warning modal_action"
                                       href="/set_document_off/<?= $person_data['Document']['id'] ?>">Запретить</a>
                                <? } else { ?>
                                    <a class="btn btn-success modal_action"
                                       href="/set_document_on/<?= $person_data['Document']['id'] ?>">Разрешить</a>
                                <? } ?>

                                <a class="btn btn-mint"
                                   href="/person/<?= $person['id'] ?>/edit_document/<?= $person_data['Document']['id'] ?>">Изменить</a>
                                <? if ($person_data['Document']['known_security_elements'] > 0) { ?>
                                    <a class="btn btn-primary modal_action"
                                       href="/person/reset_document/<?= $person_data['Document']['id'] ?>">Сбросить
                                        сведения о документе</a>
                                <? } ?>
                                <a class="btn btn-danger modal_action"
                                   href="/person/delete_document/<?= $person_data['Document']['id'] ?>/?person_id=<?= $person['id'] ?>">Удалить</a>
                            </td>

                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

            <h3 class="panel-title">Список проходов (последние 100)</h3>
            <div class="table-responsive">
                <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Время прохода</th>
                        <th>Направление</th>
                        <th>Документ</th>
                        <th>Фото</th>
                        <th>Наличие в К.С.</th>
                        <th>Верификация (лицо)</th>
                        <th>Верификация (отпечатки пальцев)</th>

                        <th>Проход произведен</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $n = 0;
                    foreach ($person_content['pass_list'] as $pass) {
                        $n++;
                        ?>
                        <tr class="">
                            <td><?= $n ?></td>
                            <td>
                                <span class="text-info"><?= pass_date($pass['Pass_List']['pass_time']) ?></span>
                            </td>
                            <!-- <td><?= $pass['Pass_List']['gate_id'] ?></a></td> -->
                            <td><? if ($pass['Pass_List']['pass_direction'] == "Enter") {
                                    echo "Вход";
                                } else {
                                    echo "Выход";
                                } ?></a></td>
                            <td class=""><?
                                $face_identify = "Идентификация по лицу";
                                if ($pass['Pass_List']['doc_type'] != $face_identify) {
                                    echo $pass['Pass_List']['doc_type'] . " " . $pass['Pass_List']['doc_number'];
                                } else {
                                    echo $face_identify;
                                } ?></td>
                            <td>
                                <a href="/image_pass/<?= $pass['Pass_List']['id']; ?>" class="image_gallery">
                                    Фотография
                                </a>
                            </td>
                            <td>
                                <?
                                //pr($list_names);
                                if ($pass['Pass_List']['control_list'] > 0) { ?>
                                    <a class="btn-link"
                                       href="control_list/view/<?= $pass['Pass_List']['control_list'] ?>"><?
                                        $pass_list_id = $pass['Pass_List']['control_list'];
                                        if (in_array($pass_list_id, array_keys($list_names))) {
                                            echo $list_names[$pass_list_id];
                                        } else {
                                            echo "список№" . $pass_list_id;
                                        } ?></a>
                                <? } else {
                                    echo "----";
                                } ?>
                            </td>
                            <td><?= $pass['Pass_List']['face_verification_status_name'] ?></a></td>
                            <td><?= $pass['Pass_List']['fp_verification_status_name'] ?></a></td>
                            <td class="">
                                <? if ($pass['Pass_List']['pass_allowed'] == "true") { ?>
                                    Да
                                <? } else { ?>
                                    Нет
                                <? } ?>
                            </td>

                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

</div>