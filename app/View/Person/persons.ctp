<div id="page-content">

    <?

    $params = array(
        "filter" => $filter,
        "person_status" => $person_status,
        "organization" => $organization
    );


    function get_params($array, $elem){
        foreach ($array as $k => &$v){
            if($k==$elem){
                unset($array[$k]);
            }
        }
        return http_build_query($array);
    }
    ?>
    <div class="row">
        <div class="eq-height">
            <div class="col-sm-10 eq-box-lg">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Список персон</h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">
                            <div class="col-sm-12">
                                Фильтр по букве фамилии
                                <? foreach ($lastname_indexes as $index) { ?>
                                    <a href="/persons?filter=<?= $index ?>&<?=get_params($params, "filter")?>"
                                       class="btn btn-small btn-link <? if ($index == $filter) echo 'btn-primary'; ?>"><?= $index ?></a>
                                <? } ?> <a href="/persons?1=1&<?=get_params($params, "filter")?>" class="btn btn-small btn-link <? if (empty($filter)) echo 'btn-primary'; ?>">Все</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">

                                <form name="person_status" action="/persons" method="get">
                                    <div class="checkbox">
                                        <input type="hidden" name="url" value="<?=get_params($params, "person_status")?>">
                                        <input type="checkbox" name="person_status" class="magic-checkbox" id="person_status" <?if($person_status=="on") echo 'checked';?>/>
                                        <label for="person_status">Показывать всех персон (включая заблокированых)</label>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-1">
                                Фильтр по организации
                            </div>
                            <div class="col-sm-3">
                                <script>
                                    $(document).ready(function () {

                                        $(document).on("change", "#person_status", function (){
                                            var status = null;
                                            if ($('#person_status').is(":checked")){
                                                status = "on";
                                            }
                                            console.log(status);
                                            $.ajax({
                                                url: "/person/set_status_view/?status=" + status,
                                                type: "post",
                                                dataType: 'json',
                                                data: null,
                                                beforeSend: function () {
                                                },
                                                success: function (data) {
                                                    console.log(data)
                                                }
                                            });
                                            $('form[name="person_status"]').submit();
                                        });

                                        $('#organization_select').change(function () {
                                            var opt = $("#organization_select option:selected").text();
                                            $("#organization").val(encodeURIComponent(opt));
                                            $('form[name="organization"]').submit();
                                        });

                                    });

                                </script>
                                <form name="organization" action="/persons" method="get">
                                    <input class="form-control" type="hidden" name="organization" id="organization">

                                    <?
                                    foreach ($params as $key => $item){
                                        if($key =="organization") continue;?>
                                        <input type="hidden" name="<?=$key?>" value="<?=$item?>">
                                    <?}
                                    ?>
                                    <select class="form-control" id="organization_select">
                                        <option value="">Все организации</option>
                                        <? foreach ($organizations as $org) {
                                            ?>

                                            <option value="" <? if ($organization == $org) echo 'selected'; ?>><?= $org ?></option>
                                        <? } ?>
                                    </select>
                                </form>
                            </div>
                            <div class="col-sm-2">
                                <a href="/persons?" class="btn btn-small btn-link">Сбросить фильтры</a>
                            </div>
                        </div>


                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Персона</th>
                                    <th>Организация</th>
                                    <th>Должность</th>
                                    <th>Отдел</th>
                                    <th>Статус</th>
                                    <th>Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $n = 0;
                                foreach ($persons as $person_data) {
                                    $enabled = "";
                                    $n++;
                                    $doc_number = " нет данных";
                                    $person = $person_data['Person'];
                                    $person_status_data = $person_data['Person']['blocked'];
                                    $PersonID = $person['id'];
                                    if (count($person_data['docs'])) {
                                        $enabled_status = $person_data['docs'][0]['Document']['enabled'];
                                        if ($enabled_status == "true") {
                                            $enabled = "Разрешен";
                                        } else {
                                            $enabled = "Запрещен";
                                        }/*
                                        $document_id = $person_data['docs'][0]['Document']['id'] ?? null;
                                        $doc_number = $person_data['docs'][0]['Document']['doc_number'] ?? null;
                                        $doc_type_name = $doc_type[$person_data['docs'][0]['Document']['doc_type']] ?? null;
                                        $doc_number = "<b>" .  $doc_type_name . " " .   $doc_number. "</b>";*/
                                    }

                                    $person_name = prepare_fio_short($person['lastname'], $person['firstname'], $person['middlename']);
                                    ?>
                                    <tr class="">
                                        <td><?= $n ?></td>
                                        <td><a class="btn-link text-semibold"
                                               href="person/view/<?= $PersonID ?>"> <?= $person_name ?></a></td>
                                        <td><?= $person['organization'] ?></td>
                                        <td><?= $person['position'] ?></td>
                                        <td><?= $person['subdivision'] ?></td>
                                        <td>
                                            <? if (!$person_status_data) { ?>
                                                Активен
                                            <? } else { ?>
                                                Заблокирован
                                            <? } ?>
                                        </td>

                                        <th>
                                            <? if (!$person_status_data) { ?>
                                                <a class="btn btn-warning modal_action"
                                                   href="/person/<?= $PersonID ?>/set_status/?status=true">Запретить</a>
                                            <? } else { ?>
                                                <a class="btn btn-success modal_action"
                                                   href="/person/<?= $PersonID ?>/set_status/?status=false">Разрешить</a>
                                            <? } ?>
                                            <a class="btn btn-mint" href="/person/edit/<?= $PersonID ?>">Изменить</a>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <a href="/add_person" class="btn btn-success btn-bock">Добавить персону</a>

                    <hr>

                    <span class="pagination-info">Всего персон: <?= $person_count ?></span>

                    <? if ($person_count > $per_page) {

                        ?>
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/persons";
                            $params = array(
                                    "filter" => $filter,
                                    "person_status" => $person_status,
                                    "organization" => $organization
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>

                    <? } ?>

                </div>
            </div>
        </div>
    </div>

</div>