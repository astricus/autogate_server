var showSuccessAlertMessage = function (message, place) {
    return showAlertMessage(message, place, "success");
};

var showErrorAlertMessage = function (message, place) {
    return showAlertMessage(message, place, "error");
};

var showAlertMessage;

$(document).ready(function () {

    showAlertMessage = function (message, place, type) {
        $(".alert").remove();
        if (type == "success") {
            type_alert = "alert-success";
        } else if (type == "error") {
            type_alert = "alert-danger";
        }
        var message_html = '<div class="alert ' + type_alert + ' alert-dismissible"> ' +
            message +
            '</div>';
        $(message_html).insertBefore(place);
    };
});