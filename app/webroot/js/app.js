$(document).ready(function () {
    $(document).on("click", ".modal_action", function (event) {
        var cur_url = $(this).attr('href') || undefined;
        event.preventDefault();
        if (confirm("Вы действительно хотите выполнить это действие?")) {
            if(cur_url!=undefined) {
                window.location.href = cur_url;
            }
        } else {
            return false;
        }
        return false;
    });
});