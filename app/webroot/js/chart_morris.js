$(document).on('nifty.ready', function () {
    var chart = Morris.Area({
        element: 'added_product_stat',
        data: added_product_stat,
        gridEnabled: true,
        gridLineColor: 'rgba(0,0,100,.1)',
        behaveLikeLine: true,
        smooth: false,
        axes:true,
        xkey: 'period',
        ykeys: ['added'],
        labels: ['Добавлено товаров'],
        lineColors: ['#9B59B6'],
        pointSize: 1,
        pointStrokeColors : ['#045d97'],
        lineWidth: 1,
        resize:true,
        hideHover: 'auto',
        fillOpacity: 0.9,
        parseTime:false
    });

    chart.options.labels.forEach(function(label, i){
        var legendItem = $('<div class=\'morris-legend-items\'></div>').text(label);
        $('<span></span>').css('background-color', chart.options.lineColors[i]).prependTo(legendItem);
        $('#added_product_stat').append(legendItem);
    });
});