var redirect_url;
$(document).ready(function () {

    $(document).on("click", ".create_control_list_button", function (event) {
        console.log("send list data");
        event.preventDefault();
        var data = $(".create_control_list").serialize();
        createControlListAction(data);
        return false;
    });

    $(document).on("click", ".create_control_rule_button", function (event) {
        var list_id = $(".create_control_rule").attr('data-list_id') || null;
        console.log("send rule");
        event.preventDefault();
        var data = $(".create_control_rule").serialize();
        createControlRuleAction(data, list_id);
        return false;
    });

    $(document).on("click", ".update_control_list_button", function (event) {
        var list_id = $(".update_control_list").attr('data-list_id') || null;
        console.log("update list");
        event.preventDefault();
        var data = $(".update_control_list").serialize();
        updateControlListAction(data, list_id);
        return false;
    });

    var checkPersonIdExists = function () {
        var value = $("select[name='person_id'] option:selected").val();
        if(value==0) {
            return false;
        }
        return true;
    };

    var checkRuleMask = function () {
        var blocker_field = $(".blocker_field").length;
        var found_mask = false;
        for(var i = 0; i<blocker_field;i++){
            var value = $(".blocker_field").eq(i).val();
            if(value!=""){
                found_mask = true;
            }
        }
        if(found_mask) {
            return  true;
        }
        return false;
    };


    $(document).on("click", ".update_control_rule_button", function (event) {
        var rule_id = $(".update_control_rule").attr('data-rule_id') || null;
        console.log("update rule");

        if (checkPersonIdExists() || checkRuleMask()) {
            event.preventDefault();
            var data = $(".update_control_rule").serialize();
            updateControlRuleAction(data, rule_id);
        }
        return false;
    });

    var createControlListAction = function (send_data) {
        console.log(send_data);
        $.ajax({
            url: "/control_list/create_list/",
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".create_control_list");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".create_control_list");
                }
            }
        });
    };

    var updateControlListAction = function (send_data, list_id) {
        console.log(send_data);
        $.ajax({
            url: "/control_list/update/list/" + list_id,
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".update_control_list");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".update_control_list");
                }
            }
        });
    };

    var updateControlRuleAction = function (send_data, rule_id) {
        console.log(send_data);
        $.ajax({
            url: "/control_list/update/rule/" + rule_id + "/",
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".update_control_rule");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".update_control_rule");
                }
            }
        });
    };

    var redirectToList = function (){
        window.location = redirect_url;
        return true;
    };

    var createControlRuleAction = function (send_data, list_id) {
        redirect_url = "/control_list/view/" + list_id;
        $.ajax({
            url: "/control_list/create_rule/" + list_id,
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".create_control_rule");
                    setTimeout(redirectToList, 3000);

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".create_control_rule");
                }
            }
        });
    };


    $(document).on("click", ".delete_list", function (event) {
        var href = $(this).attr('href');
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: href,
            type: "post",
            dataType: 'json',
            data: data_id,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".control_list_block");
                    $(".list_id_item[data-list_id='" + data_id + "']").hide();

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".control_list_block");
                }
            }
        });
        event.preventDefault();
        return false;
    });

});