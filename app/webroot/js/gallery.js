$(document).ready(function () {



    var gallery = $(".__gallery");
    var gallery_image = $(".__gallery_image");

    var showGallery = function (img) {
        var new_img = $("<img src='" + img + "' alt='" + img + "' />");
        gallery_image.html(new_img);
        gallery.show();
    };

    var closeGallery = function () {
        gallery.hide();
        gallery_image.html('');
    };

    $(document).mouseup(function (e) {
        var container = gallery;
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        }
    });

    $(document).on("click", ".image_gallery", function (ev) {
        var cur_image = $(this).attr("href");
        showGallery(cur_image);
        ev.preventDefault();
        return false;
    });
    $(document).on("click", ".__gallery_close", function () {
        closeGallery();
    });

});