$(document).ready(function () {

    $(document).on("click", ".edit_operator_button", function (event) {
        var operator_id = $(".edit_operator").attr('data-operator_id') || null;
        console.log("update operator");
        event.preventDefault();
        var data = $(".edit_operator").serialize();
        editOperatorAction(data, operator_id);
        return false;
    });

    var editOperatorAction = function (send_data, operator_id) {
        console.log(send_data);
        $.ajax({
            url: "/operator/edit/" + operator_id,
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".edit_operator");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".edit_operator");
                }
            }
        });
    };

    $(document).on("click", ".add_operator_button", function (event) {
        console.log("add op");
        event.preventDefault();
        var data = $(".add_operator").serialize();
        addOperatorAction(data);
        return false;
    });

    var addOperatorAction = function (send_data) {
        console.log(send_data);
        $.ajax({
            url: "/operator/add/",
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.data.message, ".add_operator");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.data.message, ".add_operator");
                }
            }
        });
    };


    $(document).on("click", ".reset_password_button", function (event) {

        var operator_id = $(this).attr('data-operator_id') || null;
        var operator_name = $(this).attr('data-operator_name') || null;
        console.log("reset password");
        event.preventDefault();
        if (confirm("Вы действительно хотите выполнить это действие?")) {
            resetPassOperatorAction(operator_id, operator_name);
        }
        return false;
    });

    var resetPassOperatorAction = function (operator_id, operator_name) {
        $.ajax({
            url: "/operator/reset_password/" + operator_id,
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.status);
                if (data.status == "success") {
                    showSuccessAlertMessage(operator_name + " " + data.data.message, ".reset_result");

                } else if (data.status == "error") {
                    showErrorAlertMessage(operator_name + " " + data.data.message, ".reset_result");
                }
            }
        });
    };
});