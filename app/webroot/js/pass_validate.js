$(document).ready(function () {
    var doc_number = $("input[name='doc_number']");
    var doc_type = $("select[name='doc_type']");

    var validateVerify = function () {
        var value = $("select[name='doc_type'] option:selected").val() || null;
        console.log(value);
        if (value == "PN_RUS") {
            return true;
        } else if (value == "PD_LAO") {
            return true;
        } else if (value == "ID_internal") {
            return true;
        } else if (value == "P_RUS") {
            if(passportHasChip()){

            } else {

            }

        }
        return false;
    };

    var passportHasChip = function () {

        };

    var checkDocType = function () {
        var value = $("select[name='doc_type'] option:selected").val() || null;
        console.log(value);
        if (value == "PN_RUS") {
            return true;
        }
        return false;
    };

    $("form").submit(function (ev) {

        if (checkDocType()) {
            console.log("trying send form - cancel");
            if (!validatePassNumber()) {
                ev.preventDefault();
                return false;
            }
        }
    });

    var validatePassNumber = function () {
        var value = doc_number.val();
        var pattern = /^[0-9]{10}$/;
        if (pattern.test(value)) {
            if (doc_number.parent().hasClass("has-error")) {
                doc_number.parent().removeClass("has-error");
            }
            return true;
        } else {
            if (!doc_number.parent().hasClass("has-error")) {
                doc_number.parent().addClass("has-error");
            }
            return false;
        }

    };

    $(document).on("change", doc_type, function () {
        if (checkDocType()) {
            validatePassNumber();
        } else {
            if (doc_number.parent().hasClass("has-error")) {
                doc_number.parent().removeClass("has-error");
            }
        }

    });

    $(document).on("change", doc_number, function () {
        if (checkDocType()) {
            validatePassNumber();
        }
    });

});