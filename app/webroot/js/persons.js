$(document).ready(function () {

    if ($(".person_block").length>0) {
        $(".person_find").keyup(function (){
            var value = $(this).val();
            if(value.length>=3) {
                $.ajax({
                    url: "/person/get_ajax_list/?string=" + value,
                    type: "post",
                    dataType: 'json',
                    data: null,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        $(".person_block").html("");
                        for (var i = 0; i < data.data.length; i++) {
                            var person = data.data[i];
                            var pid = person.id;
                            var name = person.name;
                            var organization = person.organization;
                            $(".person_block").append("<option value='" + pid + "'>" + name + " " + organization + "</option>");
                        }
                    }
                });
            }
        });
    }
});