$(document).ready(function () {

    var show_ag = 0;

    var doubt_pass_id;

    var show_passport_mode = false;

    var gate_id;

    var messages_title = {
        "PAK_setup": "Идет запуск системы: загрузка ССМ, загрузка оборудования...",
        "wait_for_approval": "Ожидание разрешения начала работы",
        "wait_for_pass": "Ожидание прохода",
        "scud_card_reading": "Идет чтение карты СКУД...",
        "oncoming_pass": "Встречный проход",
        "document_scanning": "Идет сканнирование документа...",
        "read_database": "Получение данных из базы...",
        "fingerprint_verification": "Верификация по отпечатку пальца",
        "access_granted": "Пользователю разрешено пройти",
        "pass_is_denied": "Проход запрещен",
        "gate_barrier": "Внимание! Препятствие в зоне прохода",
        "detach_document": "Пожалуйста, заберите документ со считывателя",
        "verification_error": "Произошла ошибка верификации, проход запрещен",
    };

    var messages_class = {
        "add_document_for_pass": "info",
        "scud_card_reading": "info",
        "PAK_setup": "info",
        "oncoming_pass": "warning",
        "document_scanning": "info",
        "document_is_failure": "danger",
        "pass_is_denied": "danger",
        "add_finger_to_screen": "info",
        "verification_error": "danger",
        "camera_looking_up": "info",
        "camera_looking_down": "info",
        "gate_barrier": "warning",
        "detach_document": "warning",
        "opening_time_is_up": "warning",
        "access_granted": "success",
        "wait_time": "info",
    };

    var setBGByStatus = function (status) {
        if (status == "PAK_off") {
            $(".cls-container").css({'background-color': '#888888 !important'});
        }
        if (status == "PAK_error") {
            $(".cls-container").css({'background-color': '#f00000 !important'});
        }
    };

    var env_titles = {
        "camera": "Камера",
        "docscanner": "Сканнер документов",
        "fpscanner": "Сканнер отпечатков пальцев",
        "database": "База данных",
        "gate": "Автогейт",
        "lighter": "Светодиодный осветитель",
        "externalsystem": "Внешняя система",
    };

    var last_event_id = 0;

    var updateAutogate = function (id, last_pass) {
        $.ajax({
            url: "/dashboard/status_ajax/?id=" + id,
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                var environment = data.data.environment;
                var current_pass_id = data.data.current_pass;
                var user_check = data.data.user_check || undefined;
                var PAK_status = data.data.PAK_status || undefined;
                var gate_status = data.data.gate_status || undefined;
                var gate_substatus = data.data.gate_substatus || undefined;
                var out_of_service_status = data.data.out_of_service_status || undefined;
                var show_pass_window = data.data.show_pass_window || undefined;

                var last_passes = data.data.last_passes || undefined;
                if (gate_substatus=="DoubtfulDocument") {
                    doubt_pass_id = current_pass_id;
                    documentValidate(current_pass_id);
                }



                if(last_passes!=undefined) {
                    $(".last_pass_block").html("");
                    var pass_allowed_class;
                    var pass_done;
                    $(".last_pass_block").append("<h5>Последние проходы через автогейт</h5>");
                    for (var i = 0; i < last_passes.length; i++) {
                        console.log(last_passes[i]);
                        var cur_pass = last_passes[i];
                        if(cur_pass.pass_done==true){
                            pass_done = "Проход был завершен";
                        } else {
                            pass_done = "Проход не состоялся";
                        }
                        if(cur_pass.pass_done==true){
                            pass_allowed_class = "green_class";
                        } else {
                            pass_allowed_class = "red_class";
                        }
                        var pass_id = cur_pass.pass_id;
                        var user_name = cur_pass.name;
                        var list_id = cur_pass.list_id;
                        var list_name = cur_pass.list_name;
                        var list_name_str="";
                        if(list_name!=null){
                            list_name_str = "<a class='btn btn-warning' href='#'>Контрольный список</a>";
                        }
                        var pass_time = cur_pass.pass_time;

                        $(".last_pass_block").append("<div class='last_pass_item " + pass_allowed_class + "'>" +
                            "<img width=30px height=30px title='' alt='user' src='/images/user.svg'/> " +
                            "<a class='btn-link' href='/pass_data/" + pass_id + "'><b>" + user_name + "</b> проход:  <b>" + pass_time + "</b></a> " + pass_done +
                            "&nbsp;" + list_name_str + "</div>");
                    }
                    if($(".last_pass_block").is(":hidden")){
                        $(".last_pass_block").show();
                    }
                }

                if (PAK_status != undefined) {
                    setBGByStatus(PAK_status);
                }
                if (gate_status=="Waiting4Environment" || gate_status=="FatalError") {
                    return true;
                }
                $(".environment_" + id).html("");
                for (var j = 0; j < environment.length; j++) {
                    var env_status = environment[j].status;
                    var env_name = environment[j].name;
                    var env_status_class;
                    if (env_status == "false") {
                        env_status_class = "env_icon_warning";
                    } else {
                        env_status_class = "env_icon_success";
                    }
                    var img_src = '/images/' + env_name + '.svg';
                    var env_title = env_titles[env_name] || "Устройство не определено";
                    $(".environment_" + id).append("<div class='env_icon " + env_status_class + "'>" +
                        "<img width=40px height=40px title='" + env_title + "' alt='" + env_title + "' src='" + img_src + "'/></div>");
                }
                if (show_pass_window=="false") {
                    //if (show_ag == id) {
                        hideModal(id);
                        hideControlListMessage();
                    //}
                }
                else if (show_pass_window=="true") {
                    //if (show_ag != id || current_pass_id != last_pass) {
                    console.log("SHOW_MODAL");
                        showModal(id);
                    //}
                    getPassData(current_pass_id, id);
                    last_pass = current_pass_id;
                    show_ag = id;
                }
                if (user_check != undefined) {
                    showUserCheckMessage(user_check);
                }

            }
        });
    };

    var getPassData = function (pass_id, gate_id) {

        $.ajax({
            url: "/dashboard/get_pass_data/?pass_id=" + pass_id + "&last_event_id=" + last_event_id + "&gate_id=" + gate_id,
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                var event_id = data.data.last_event_id;
                var is_pass_done = data.data.is_pass_done;
                var real_image = data.data.real_image;
                var db_image = data.data.db_image;
                var validate_data = data.data.document_params || null;
                //var validate_pass = "66055315950";

                //console.log("e_id" + event_id);
                //if (event_id != last_event_id) {
                var user_info = data.data;
                var user_name = user_info.user_name;
                var organization = user_info.organization;
                var division = user_info.division;
                var position = user_info.position;
                var photoFromDB = user_info.photo_from_db;
                var live_photo = user_info.live_photo;
                var subdivision = user_info.subdivision;
                var document = user_info.document;
                real_image = user_info.real_image;
                db_image = user_info.db_image;
                var control_list = data.data.control_list || undefined;
                var required = data.data.required || undefined;
                if(control_list!=undefined){
                    console.log("show ct list");
                    showControlListMessage(control_list);
                }

                fillUserInfo(user_name, organization, division, position, db_image, real_image, subdivision, document);

                console.log(data.data);
                last_event_id = event_id;

                var fp_verify = data.data.fp_verify;
                var camera_verify = data.data.camera_verify;
                var document_verify = data.data.document_verify;
                var automatic_decision = data.data.automatic_decision;
                var gate_init = data.data.gate_init;
                var user_pass = data.data.user_pass;
                var database = data.data.database;
                var pass_trigger = data.data.pass_trigger;

                $(".verify_messages").html("");
                addVerifyMessage("pass_trigger", pass_trigger.status, pass_trigger.type, pass_trigger.text);
                addVerifyMessage("database", database.status, database.type, database.text);//7
                addVerifyMessage("fp_verify", fp_verify.status, fp_verify.type, fp_verify.text);
                addVerifyMessage("camera_verify", camera_verify.status, camera_verify.type, camera_verify.text);
                addVerifyMessage("automatic_decision", automatic_decision.status, automatic_decision.type, automatic_decision.text); //5
                addVerifyMessage("gate_init", gate_init.status, gate_init.type, gate_init.text);//6
                addVerifyMessage("user_pass", user_pass.status, user_pass.type, user_pass.text);//7
                if (is_pass_done == "1") {
                    hideModal();
                    hideControlListMessage();

                }
            }
        });
    };

    var documentValidate = function (pass_id) {
        //loadDoubtfulDocument(pass_id, ".doubtful_document .document_image_container");
        showDoubtfulDocPreview(pass_id);
        console.log("showDoubtfulDocPreview");
        //showDoubtfulDoc();
        return true;
    };

    var fillUserInfo = function (user_name, organization, division, position, photoFromDB, live_photo, subdivision, document) {

        if (user_name != null) {
            console.log("user_name: " + user_name);
            $(".user_name_field").text(user_name);
        }
        if (organization != null) {
            $(".user_organization_field").text("Организация: " + organization);
        }
        if (division != null) {
            $(".user_division_field").text("ОТдел: " + division);
        }
        if (position != null) {
            $(".user_position_field").text("Должность: " + position);
        }
        if (photoFromDB != null) {
            var new_src = "data:image/jpeg;base64,";
            new_src += photoFromDB;
            $(".user_photo_from_db_field").attr("src", new_src);
            $(".user_photo_from_db_field").attr("width", "100%");
        }
        if (live_photo != null) {
            var live_src = "data:image/jpeg;base64,";
            live_src += live_photo;
            $(".user_live_photo_field").attr("src", live_src);
            $(".user_live_photo_field").attr("width", "100%");
        }
        if (subdivision != null) {
            $(".user_subdivision_field").text("Подразделение: " + subdivision);
        }
        if (document != null && show_passport_mode) {
            $(".user_document_field").text("Документ: " + document);
        }
    };

    var clearWindowPass = function (){
        var dev_user_image = "/images/user.svg";
        $(".user_name_field").text("");
        $(".user_organization_field").text("");
        $(".user_division_field").text("");
        $(".user_position_field").text("");
        $(".user_photo_from_db_field").attr("src", dev_user_image);
        $(".user_live_photo_field").attr("src", dev_user_image);
        $(".user_subdivision_field").text("");
        $(".user_document_field").text("");
    };

    var updateAGStatus = function () {
        updateAutogate(autogate_1_id);
        updateAutogate(autogate_2_id);
    };
    // обновление статуса окна
    setInterval(updateAGStatus, 1200);

    var clearWindow = function () {
        window.location.reload();
    };

    setInterval(clearWindow, 600000);

    $(document).on("click", ".close_modal", function () {
        hideModal();
    });

    var showModal = function (id) {
        if($(".modal_box").is(":hidden")) {

            $(".modal_box").show();
        }
        gate_id = id;

    };

    var hideModal = function (id) {
        if(id == gate_id) {
            $(".modal_box").hide();
            gate_id = 0;
            clearWindowPass();
        }
    };

    var showUserCheckMessage = function (type) {
        $(".user_check_popup").hide();
        if (type == "failure") {
            $(".user_check_failure").show();
        } else if (type == "success") {
            $(".user_check_success").show();
        } else if (type == "document_alert") {
            $(".user_check_document_alert").show();
        }
    };

    var hideUserCheckMessage = function () {
        $(".user_check_document_alert").hide();
    };

    var showUserForceMessage = function (type) {
        $(".user_access_message").hide();
        if (type == "access_denied") {
            $(".user_access_denied_message").show();
        } else if (type == "access_granted") {
            $(".user_access_granted_message").show();
        }
    };

    var showControlListMessage = function (name){
        $(".control_list_message").html(name);
        if($(".control_list_message").is(":hidden")) {
            $(".control_list_message").show();
        }
    };

    var hideControlListMessage = function (){
        $(".control_list_message").hide();
    };


    var showDoubtfulDoc = function () {
        $(".doubtful_document").show();
    };

    var hideDoubtfulDoc = function () {
        $(".doubtful_document").hide();
    };

    var showDoubtfulDocPreview = function () {
        $(".document_image_preview").show();
    };

    var hideDoubtfulDocPreview = function () {
        $(".document_image_preview").hide();
    };

    var addVerifyMessage = function (name, status, type, text) {
        var mess_type = "verify_message_" + type;
        if (name == "automatic_decision" && status == "true") {
            mess_type = "gate_opened_verify verify_message_success";
        } else if (name == "automatic_decision" && status == "false") {
            mess_type = "gate_closed_verify verify_message_danger";
        }
        if (name == "gate_init" && status == "wait") {
            mess_type = "gate_default_verify verify_message_info";
        } else if (name == "gate_init" && status == "open") {
            mess_type = "gate_default_verify verify_message_success";
        } else if (name == "gate_init" && status == "error") {
            mess_type = "gate_default_verify verify_message_danger";
        }

        if (name == "user_pass" && status == "wait") {
            mess_type = "user_pass_verify verify_message_info";
        } else if (name == "user_pass" && status == "success") {
            mess_type = "user_pass_verify verify_message_success";
        } else if (name == "user_pass" && status == "failure") {
            mess_type = "user_pass_verify verify_message_danger";
        }
        if (name == "database" && status == "wait") {
            mess_type = "database_verify verify_message_info";
        } else if (name == "database" && status == "success") {
            mess_type = "database_verify verify_message_success";
        } else if (name == "database" && status == "failure") {
            mess_type = "database_verify verify_message_danger";
        }

        if (name == "pass_trigger" && status == "document") {
            mess_type = "passport_verify verify_message_success";
        } else if (name == "pass_trigger" && status == "camera") {
            mess_type = "camera_verify verify_message_success";
        } else if (name == "pass_trigger" && status == "scud") {
            mess_type = "scud_verify verify_message_success";
        }

        var mess = "<div title='" + text + "' class='verify_message " + mess_type + " " + name + "' ></div>";
        $(".verify_messages").append(mess);
    };

    // force access
    var forceAccess = function (action, gate_id) {
        $.ajax({
            url: "/dashboard/force_access/?action=" + action + "&gate_id=" + gate_id,
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                var force_access = data.data.force_access;
                console.log("force access " + action + " result: " + force_access);
                showUserForceMessage(force_access);
            }
        });
    };

    $(document).on("click", ".force_access_granted", function () {
        forceAccess("access_granted", gate_id);
    });

    $(document).on("click", ".force_access_denied", function () {
        forceAccess("access_denied", gate_id);
    });

    $(document).on("click", ".create_control_list_button", function (event) {
        event.preventDefault();
        var data = $(".create_control_list").serialize() + '&' + $.param(object);
        createControlListAction(data);
        return false;
    });



    $(document).on("submit", ".create_control_rule", function (event) {
        var data = $(".create_control_rule").serialize() + '&' + $.param(object);
        createControlRuleAction(data);
        event.preventDefault();
        return false;
    });

    var createControlListAction = function (send_data) {
        $.ajax({
            url: "/control_list/create_list/",
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.message, "create_control_list");
                } else if (data.status == "error") {
                    showErrorAlertMessage(data.message, "create_control_list");
                }
            }
        });
    };

    var createControlRuleAction = function (send_data) {
        $.ajax({
            url: "/control_list/create_rule/",
            type: "post",
            dataType: 'json',
            data: send_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    showSuccessAlertMessage(data.message, "create_control_list");

                } else if (data.status == "error") {
                    showErrorAlertMessage(data.message, "create_control_list");
                }
            }
        });
    };

    var showSuccessAlertMessage = function (message, place) {
        return showAlertMessage(message, place, "success");
    };

    var showErrorAlertMessage = function (message, place) {
        return showAlertMessage(message, place, "error");
    };

    var showAlertMessage = function (message, place, type) {
        if (type == "success") {
            type_alert = "alert-success";
        } else if (type == "error") {
            type_alert = "alert-danger";
        }
        var message_html = '<div class="alert ' + type_alert + '">' +
            message +
            '</div>';
        $(place).insertBefore(message_html);
    };

    var old_load_pass;

    var loadDoubtfulDocument = function (container) {
        if (old_load_pass != doubt_pass_id) {
            $.ajax({
                url: "/person/load_doubtful_document/" + doubt_pass_id,
                type: "post",
                dataType: 'json',
                data: null,
                beforeSend: function () {
                },
                success: function (data) {
                    $(container).html("");
                    for (var i = 0; i < data.data.documents.length; i++) {
                        var doc = data.data.documents[i];
                        var new_src = "data:image/jpeg;base64,";
                        new_src += doc;
                        $(container).append("<a target='_blank' href='" + new_src + "'><img src='" + new_src + "' style='width: 300px' alt=''></a>");
                    }
                    $(container).append("<br><div class='accept_document_action btn-lg btn btn-success' data-pass_id='" + doubt_pass_id + "'>Подтвердить документ</div>");
                    $(container).append("&nbsp;&nbsp;<div class='reject_document_action btn-lg btn btn-success' data-pass_id='" + doubt_pass_id + "'>Закрыть</div>");
                }
            });
            old_load_pass = doubt_pass_id;
        }
    };

    $(document).on("click", ".accept_document_action", function () {
        var pass_id = $(this).attr('data-pass_id');
        acceptDocument(pass_id);
        hideDoubtfulDoc();
        hideDoubtfulDocPreview();
    });

    $(document).on("click", ".reject_document_action", function () {
        hideDoubtfulDoc();
    });

    $(document).on("click", ".document_image_preview", function () {
        loadDoubtfulDocument(".doubtful_document .document_image_container");
        showDoubtfulDoc();
    });


    var acceptDocument = function (pass_id) {
        $.ajax({
            url: "/person/accept_document/" + pass_id,
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                hideDoubtfulDoc();
            }
        });
    };

});